<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property int $group_id
 * @property string $tag_no
 * @property string $grade
 * @property float $thickness
 * @property string $dimensions
 * @property float $lbs
 * @property string $heat
 * @property string $slab
 * @property string $country_of_origin
 * @property string $mill_name
 * @property float $price_cwt
 * @property float $total_price
 * @property string $visible
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Group $group
 */
class Product extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'group_id' => true,
        'tag_no' => true,
        'item_ser_no' => true,
        'mtr' =>true,
        'mtr_tag_sql_recid' =>true,
        'notes' =>true,
        'grade' => true,
        'thickness' => true,
        'dimensions' => true,
        'lbs' => true,
        'sf'    => true,
        'heat' => true,
        'slab' => true,
        'country_of_origin' => true,
        'mill_name' => true,
        'price_cwt' => true,
        'total_price' => true,
        'visible' => true,
        'subprime' => true,
        'created' => true,
        'modified' => true,
        'group' => true
    ];
}
