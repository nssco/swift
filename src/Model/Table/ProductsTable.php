<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Products Model
 *
 * @property \App\Model\Table\GroupsTable|\Cake\ORM\Association\BelongsTo $Groups
 *
 * @method \App\Model\Entity\Product get($primaryKey, $options = [])
 * @method \App\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Product|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Product findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('products');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        $validator
            ->numeric('item_ser_no')
            ->allowEmpty('item_ser_no')
            ->add('item_ser_no', ['unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'This serial number has already been used.'
                ]
        ]); 
        $validator
            ->scalar('tag_no')
            ->maxLength('tag_no', 20)
            ->allowEmpty('tag_no');

        $validator
            ->scalar('grade')
            ->maxLength('grade', 25)
            ->allowEmpty('grade');

        $validator
            ->numeric('thickness')
            ->allowEmpty('thickness');

        $validator
            ->scalar('dimensions')
            ->maxLength('dimensions', 20)
            ->allowEmpty('dimensions');

        $validator
            ->numeric('lbs')
            ->allowEmpty('lbs');

        $validator
            ->scalar('heat')
            ->maxLength('heat', 25)
            ->allowEmpty('heat');

        $validator
            ->scalar('slab')
            ->maxLength('slab', 15)
            ->allowEmpty('slab');

        $validator
            ->scalar('country_of_origin')
            ->maxLength('country_of_origin', 20)
            ->allowEmpty('country_of_origin');

        $validator
            ->scalar('mill_name')
            ->maxLength('mill_name', 20)
            ->allowEmpty('mill_name');

        $validator
            ->numeric('price_cwt')
            ->allowEmpty('price_cwt');

        $validator
            ->numeric('total_price')
            ->allowEmpty('total_price');

        $validator
            ->scalar('visible')
            ->allowEmpty('visible');
        
        
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['group_id'], 'Groups'));

        return $rules;
    }
}
