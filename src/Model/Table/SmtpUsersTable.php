<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
class SmtpUsersTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');	
    
    }
    public function validationPassword(Validator $validator)
    {
        $validator->add('old_password', 'custom', ['rule' =>
        function ($value, $context)
            {
            $user = $this->get($context['data']['id']);
            if ($user)
                {
                if ((new DefaultPasswordHasher)->check($value, $user->password))
                    {
                    return true;
                    }
                }
            return false;
            }, 'message' => 'Old password not match the current password!', ])->notEmpty('old_password');
        $validator 
           ->add('password',[
                    'match' => [
                        'rule' => ['compareWith','confirm_password'],
                        'message' => 'Sorry! Password does not match. Please try again!'
                    ]
                ])
            ->add('confirm_password',[ 'match'=>[ 'rule'=> ['compareWith','password'], 'message'=>'The confirm password does not match!',]])->notEmpty('password');
        return $validator;
    }
}
?>
