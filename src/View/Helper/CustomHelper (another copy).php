<?php
/**
 * Custom Helper
 *
 * For custom theme specific methods.
 *
 * If your theme requires custom methods,
 * copy this file to /app/views/themed/your_theme_alias/helpers/custom.php and modify.
 *
 * You can then use this helper from your theme's views using $custom variable.
 *
 * @category Helper
 * @package  Croogo
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
namespace App\View;

namespace App\View\Helper;
use Cake\Controller\Component;
use Cake\Network\Response;
use Cake\View\Helper;
use Cake\ORM\TableRegistry;
 
class CustomHelper extends Helper {

/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    // public $helpers = array('Cookie');
    // public $components = array('Cookie');
    
  
    


         
    public function checkproductvisibility($id = null)
        {
             $permissions_obj = TableRegistry::get('Products');
             $query = $permissions_obj->find()->where(['group_id' => $id, 'visible' => 1])->count();;
             return $query;
             
            
        }


         public function showDeletebutton($id = null,$Cookie = null)
        {
        	$permissions_obj = TableRegistry::get('Carts');
        	 if(empty($this->request->session()->read('Auth.User.id'))) {
        	 	
        	 $query = $permissions_obj->find()->where(['user_session_id' => $Cookie,'user_id IS' => null, 'product_id' => $id])->count();
     }else{
        $query = $permissions_obj->find()->where(['user_session_id' => $Cookie,'user_id' => $this->request->session()->read('Auth.User.id'),'user_type' => $this->request->session()->read('Auth.User.type'), 'product_id' => $id])->count();
     }
            
             return $query;
             
            
        }
 	public function getgradecount($id = null)
        {
             $permissions_obj = TableRegistry::get('Products');
             $query = $permissions_obj->find()->where(['visible' => 1,'group_id'=>$id])->count();
             return $query;
              
        }
        public function getgradecountthic($id = null,$thick)
        {
             $permissions_obj = TableRegistry::get('Products');
             if(!empty($thick) && !empty($id))
         {
             $query = $permissions_obj->find()->where(['visible' => 1,'group_id'=>$id,'thickness in'=>explode(',',rtrim($thick,','))])->count();
         }
         else if(!empty($thick) && empty($id))
         {
            $query = $permissions_obj->find()->where(['visible' => 1,'thickness in'=>explode(',',rtrim($thick,','))])->count();
         }
         else
         {
            $query = $permissions_obj->find()->where(['visible' => 1,'group_id'=>$id])->count();
         }

             return $query;
         
              
        }
	public function getthickcount($id=null,$thick)
	{
		 $permissions_obj = TableRegistry::get('Products');
         if(!empty($id))
         {
		 $query = $permissions_obj->find()->where(['visible' => 1,'thickness'=>$thick,'group_id'=>$id])->count();
        }
        else
        {
         $query = $permissions_obj->find()->where(['visible' => 1,'thickness'=>$thick])->count();
        }
             	 return $query;
	}


    
}
