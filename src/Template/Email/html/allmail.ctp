<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>NSSCO: New order</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet"> 
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	
</head>


<body style="margin: 0; padding: 0;">









	<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#fff" >


	</table>
	<tbody>
		<tr>
			<td width="100%" valign="top" align="center">


				<table width="680" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#f2f2f2" style="">
					<tbody>

						<tr>
							
							<td>							
								<!-- Wrapper -->
								<table width="620" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border-radius:8px; margin-top: 30px;">
									<tbody>

										<tr>
											<td height="" width="100%" style="padding:20px;"> <a href="<?php echo $url;?>"><img width="87" border="0" alt="" src="<?php echo $url;?>img/mail_logo.png"></a> </td>
										</tr>

										<tr>						

											<td valign="middle" width="100%" style="text-align: left; font-family: 'Open Sans', sans-serif; font-size:17px; color: #60605e; line-height:28px; font-weight:normal;  padding:0 20px;">
												<h1 width="100%" style="text-align: left; font-family: 'Open Sans', sans-serif; font-size:30px; color: #60605e; line-height:38px; font-weight: 600; margin: 0 0 20px; padding: 0;">
													<?php echo $heading;?>
												</h1>
												<p>Hi <?php echo $project_name;?></P>

													<p>You have received an order from <?php echo $email;?></P> 

														<p>Please find attached document or can view the text for order generated.</p> 



														<table width="620" border="1" align="center" style="border-collapse: collapse">
															<tr>
																<td><b>Name</b></td>
																<td colspan="5"><?php echo $userdetail['name'];?></td>
															</tr>
															<tr>
																<td><b>Company</b></td>
																<td colspan="5"><?php echo $userdetail['company'];?></td>
															</tr>
															<tr>
																<td><b>Email</b></td>
																<td colspan="5"><?php echo $userdetail['username'];?></td>
															</tr>
															<tr>
																<td><b>Phone</b></td>
																<td colspan="5"><?php echo $userdetail['phone'];?></td>
															</tr>
															
															<?php if(!empty($need_by_date['comment'])){?>
															<tr>
																<td><b>Comment</b></td>
																<td colspan="5"><?php echo $need_by_date['comment'];?></td>
															</tr>
															<?php } ?>
															<?php if(!empty($need_by_date['po'])){?>
															<tr>
																<td><b>PO</b></td>
																<td colspan="5"><?php echo $need_by_date['po'];?></td>
															</tr>
															<?php } ?>
															<?php if(!empty($need_by_date['need_by_date'])){?>
															<tr>
																<td><b>NeededByDate</b></td>
																<td colspan="5"><?php echo $need_by_date['need_by_date'];?></td>
															</tr>
															<?php } ?>
															<tr>
																<td><b>Current Customer </b></td>
																<td colspan="5"><?php echo ($userdetail['existing_user']==1)?"Yes":"No";?></td>
															</tr>
															<?php
															 $address = '';
															 if(!empty($need_by_date['address_one'])){
															 	$address .= $need_by_date['address_one'];
															 }
															 if(!empty($need_by_date['address_two'])){
															 	$address .= ", ".$need_by_date['address_two'];
															 }

															 	?>
															 	<?php if($address != ''){?>
															<tr>
																<td><b>Ship To</b></td>
																<td colspan="5"> <?php echo $address; ?></td>
															</tr>
															<?php }  ?>
															
															<?php if(!empty($need_by_date['city'])){?>
															<tr>
																<td><b>City</b></td>
																<td colspan="5"><?php echo $need_by_date['city'];?></td>
															</tr>
															<?php } ?>
															<?php if(!empty($need_by_date['state'])){?>
															<tr>
																<td><b>State</b></td>
																<td colspan="5"><?php echo $need_by_date['state'];?></td>
															</tr>
															<?php } ?>
															<?php if(!empty($need_by_date['zip'])){?>
															<tr>
																<td><b>Zip</b></td>
																<td colspan="5"><?php echo $need_by_date['zip'];?></td>
															</tr>
															<?php } ?>

															<?php if(!empty($need_by_date['payment_method'])){
																	if($need_by_date['payment_method'] == 'terms'){
																		$payment_method = 'Terms';
																	}else if($need_by_date['payment_method'] == 'credit_card'){
																		$payment_method = 'Credit card';
																	}else if($need_by_date['payment_method'] == 'check'){
																		$payment_method = 'Check';
																	}else{
																		$payment_method = 'Cash';
																	} 

																?>
															<tr>
																<td><b>Payment Method</b></td>
																<td colspan="5"><?php echo $payment_method;?></td>
															</tr>
															<?php } ?>


															<tr>
																<td><b>Tag Number</b></td>
																<td><b>Grade</b></td>
																<td><b>Thickness</b></td>
																<td><b>Dimensions</b></td>
																<td><b>Quantity</b></td>
																<td><b>Cwt</b></td>
															</tr>

															<?php 
															$grand_total = 0;
															for($i=0; $i<count($get_cart_data); $i++){
																 $tag_number =   ($get_cart_data[$i]['products']['tag_no'] != '' ? $get_cart_data[$i]['products']['tag_no'] : '');

																 $grade = ($get_cart_data[$i]['products']['grade'] != '' ? $get_cart_data[$i]['products']['grade'] : '');

														     $thickness = ($get_cart_data[$i]['products']['thickness'] != '' ? $get_cart_data[$i]['products']['thickness'] : '');

														     $dimension = ($get_cart_data[$i]['products']['dimensions'] != '' ? $get_cart_data[$i]['products']['dimensions'] : ''); 
																$quantity = $get_cart_data[$i]['quantity'];

																$cwt = ($get_cart_data[$i]['products']['price_cwt'] != '' ? '$'.number_format($get_cart_data[$i]['products']['price_cwt'],2) : '');
																$price = ($get_cart_data[$i]['price'] != '' ? '$'.number_format($get_cart_data[$i]['price'],2) : ''); 

     															$grand_total += $get_cart_data[$i]['price'];
															 ?>
															<tr>
																<td><?php echo $tag_number;?></td>
																<td><?php echo $grade;?></td>
																<td><?php echo $thickness;?></td>
																<td><?php echo $dimension;?></td>
																<td><?php echo $quantity;?></td>
																<td><?php echo $cwt;?></td>
															</tr>
															<?php  } ?>

															<tr>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td><strong>Total</strong></td>
																<td ><strong class="calculate_grand_total">$<?php echo number_format($grand_total,2);?></strong></td>
															</tr>



														</table>
														<?php
														// $fh = fopen($xml_filepath, 'r');

														// $pageText = fread($fh, 50000);

														// echo nl2br(htmlentities($pageText));

														?>
            

													</td>                  
												</tr>




												<tr>


												</tr>

												<tr>

												</tr>

												<tr>							
													<td align="center" height="30">	 </td>

												</tr>							

											</tbody></table><!-- End Wrapper 2 -->




											<table width="620" border="0" cellpadding="0" cellspacing="0" align="center" style="border-radius:8px; margin-top: 30px;">
												<tbody>

													<tr>


													</tr>

												</tbody></table>

											</td>

										</td>
									</tr>

								</tbody></table>	

							</tr>

						</tbody></table>


					</body>

					</html>






