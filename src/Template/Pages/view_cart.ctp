<?php 
if(!empty($this->request->session()->read("Auth.User.id"))){
  $text = $this->Html->image("6.png",['class' => 'img-responsive']) . "You are almost there!";
}else{
  $text = "Request for order";
}
?>
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container getintouch-cls-info">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab">
        <div class="bhoechie-tab-content table-box active">
            <div class="row">
            <div class="col-md-9 col-sm-5 col-xs-12">
                <h3 class="heading"><?php echo @$text;?></h3>
              </div>
            <div class="col-md-3 col-sm-7 col-xs-12 text-right">
                <a href="<?php echo $SITEURL;?>"><div class="more-products add-more-new-btn">
                + Add More Products
                </div></a>
              </div>
          </div>
            <h6 class="none">See More Table scroll to right <span class="glyphicon glyphicon-chevron-right color" aria-hidden="true"></span></h6>
            <div class="table-responsive">
            <table width="100%" border="1" class="font-size mtop-div" cellpadding="5" cellspacing="5" id="view_cart_table">
                <tr class="tbody">
                <td class="top" valign="top" bgcolor="#f0f0f0"><strong>#</strong></td>
                <td  class="top" valign="top" bgcolor="#f0f0f0"><strong>Tag Number</strong></td>
                <td  class="top" valign="top" align="center" bgcolor="#f0f0f0"><strong>Quantity</strong></td>
                <td  class="top" valign="top" bgcolor="#f0f0f0"><strong>Grade</strong></td>
                <td  class="top" valign="top" bgcolor="#f0f0f0"><strong>Thickness<br>
                  (in)</strong></td>
                <td class="top" valign="top" bgcolor="#f0f0f0"><strong>Dimensions<br>
                  (in)</strong></td>
                <td class="top" valign="top" bgcolor="#f0f0f0"><strong>Weight<br>
                  (lb)</strong></td>
                <td  class="top" valign="top" bgcolor="#f0f0f0"><strong>Country of Origin</strong></td>
                <td  class="top" valign="top" bgcolor="#f0f0f0"><strong>Mill Name</strong></td>
                <td  class="top" align="right" valign="top" bgcolor="#f0f0f0"><strong>Price/CWT</strong></td>
                <td  class="top" align="right" valign="top" bgcolor="#f0f0f0"><strong>Price/EA</strong></td>
                <td class="top" valign="top" bgcolor="#f0f0f0"><strong></strong></td>
                <td class="top" valign="top" bgcolor="#f0f0f0"><strong></strong></td>
              </tr>
                
               <?php echo $this->element("pages/view_cart");?>
                
              
              
                
              
              </table>
          </div>
            <h6 class="none">See More Table scroll to right <span class="glyphicon glyphicon-chevron-right color" aria-hidden="true"></span></h6>
            <?php if(empty($this->request->session()->read('Auth.User.id'))): ?>
            <a href="<?php echo $SITEURL;?>users/login"><button class="mrtppp">Continue</button></a>
          <?php endif;?>
          </div>
      </div>

      <?php if(!empty($this->request->session()->read('Auth.User.id'))):

       echo $this->element("pages/detail_form");
     endif;
       ?>
    
      </div>