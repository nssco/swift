<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
    <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 ">
    <h1 class="big-text-new">Buy Steel Plate Today!</h1>


<div class="col-md-12 col-sm-12 col-xs-12 boxpdgnone gapboxxnew">


    <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="border-color-new">
    <div class="col-md-1 col-sm-2 col-xs-12 boxpdgnone">
    <?php echo $this->Html->image("1.png",['class' => 'img-responsive']);?>
    </div>
    
    <div class="col-md-11 col-sm-10 col-xs-12 tittle-fonnnt pdgboxrightnone">
    <h1>Thank You</h1>
    <h2>Thank you for visiting SWIFT<sup>TM</sup> <br>our new online request-for-order system!</h2>
    </div>
    
    </div>
    </div>
    
    
    
    <div class="col-md-4 col-sm-4 col-xs-12 ">
<div class="border-color-new">

    <div class="col-md-1 col-sm-2 col-xs-12 boxpdgnone">
    <?php echo $this->Html->image("2.png",['class' => 'img-responsive']);?>
    </div>
    
    <div class="col-md-11 col-sm-10 col-xs-12 tittle-fonnnt pdgboxrightnone">
    <h1>Steel</h1>
    <h2>100% MTR traceability. <br>Prime condition unless noted with <span class="asterisk_sign" style="color: red; font-size: 16px;font-weight:bold;">*</span></h2>
    </div>
    
   </div> 
    </div>
    
    
    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="border-color-new">

    <div class="col-md-1 col-sm-2 col-xs-12 boxpdgnone">
    <?php echo $this->Html->image("3.png",['class' => 'img-responsive']);?>
    </div>
    
    <div class="col-md-11 col-sm-10 col-xs-12 tittle-fonnnt pdgboxrightnone">
    <h1>Speed</h1>
    <h2>Order confirmation in 2 hours or less! <br>Order today by 2pm, ship tomorrow!</h2>
    </div>
  </div>
    
    
    </div>
    
    
    <div class="col-md-4 col-sm-4 col-xs-12 gapbox">
      <div class="border-color-new">
    <div class="col-md-1 col-sm-2 col-xs-12 boxpdgnone">
    <?php echo $this->Html->image("4.png",['class' => 'img-responsive']);?>
    </div>
    
    <div class="col-md-11 col-sm-10 col-xs-12 tittle-fonnnt pdgboxrightnone">
    <h1>Our Goal</h1>
    <h2>Our goal is to make this process easy <br>and efficient for you, our valued customer.</h2>
    </div>
    </div>
    
    </div>
    
    
    <div class="col-md-4 col-sm-4 col-xs-12 gapbox">
      <div class="border-color-new">
    <div class="col-md-1 col-sm-2 col-xs-12 boxpdgnone">
    <?php echo $this->Html->image("5.png",['class' => 'img-responsive']);?>
    </div>
    
    <div class="col-md-11 col-sm-10 col-xs-12 tittle-fonnnt pdgboxrightnone">
    <h1>Sales Contact</h1>
    <h2>Brianee Waller - brianee@nssco.com <br>713-980-5804 / 713-980-5800</h2>
    </div>
    </div>
    
    </div>
    
    
    <div class="col-md-4 col-sm-4 col-xs-12 gapbox">
      <div class="border-color-new">
    <div class="col-md-1 col-sm-2 col-xs-12 boxpdgnone">
    <?php echo $this->Html->image("6.png",['class' => 'img-responsive']);?>
    </div>
    
    <div class="col-md-11 col-sm-10 col-xs-12 tittle-fonnnt pdgboxrightnone">
    <h1>Shipping</h1>
    <h2>Prices FOB North Shore Steel. <br>Delivery offered upon request.</h2>
    </div>
    
    </div>
    </div>
  </div>
    
    
    
    </div>
    
    </div>
     <div class="col-lg-12">
         
         <div class="row">
             <div class="col-lg-12" style="margin-bottom:20px;display:none;" id="filterdiv">
                 <div class="col-lg-2"></div>
                 
                 <div class="col-md-1">
                     <h1 style="font-size:14px; margin: 0px; padding-top: 20px;">Filter: </h1>
                     
                 </div>
                 
  <div class="col-md-8" id="filterUpdate">
                     
     

<!--<span style="cursor:pointer; border:1px solid #000; color:#1c1c1c; padding:5px 10px; border-radius:3px; font-size: 12px; margin:0 0px;">0.1345  <i class="fa fa-times-circle" aria-hidden="true"></i></span>
<span style="cursor:pointer; border:1px solid #000; color:#1c1c1c; padding:5px 10px; border-radius:3px; font-size: 12px; margin:0 0px;">0.1875 <i class="fa fa-times-circle" aria-hidden="true"></i></span>-->

  
  </div>
                 <div class="col-lg-1 text-right" style="padding:0;">  
<h1 style="font-size:14px; display:inline-block; margin-right: 5px;"> </h1><a href="javascript:;" style="background:#000; color: #fff; padding:7px 10px; border-radius:3px; font-size: 12px;" onclick="resetall();">Reset All</a>
               </div>
             
            </div> 
         </div>
         
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bhoechie-tab-menu left-content">
        <div class="hdiv"><h1>Grade</h1></div>
        <?php echo $this->element("pages/group_list");?>
	       <div class="hdiv"><h1>Thickness (in)</h1></div>
        <?php echo $this->element("pages/thickness_list");?>
      </div>
         
         
      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 bhoechie-tab right-content">
        <div class="bhoechie-tab-content active table-box">
          <h2 class="group_name"></h2>
          <h6 class="none">See More Table scroll to right <span class="glyphicon glyphicon-chevron-right color" aria-hidden="true"></span></h6>
          
            <div id="divUpdate">
            <?php echo $this->element("pages/index");?>

            </div>
            
            
          
          <h6 class="none">See More Table scroll to right <span class="glyphicon glyphicon-chevron-right color" aria-hidden="true"></span></h6>
        
        </div>
        
        

      </div>
         
         
    </div>
    </div>
