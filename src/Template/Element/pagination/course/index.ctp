<div class="x_content">
<?php echo $this->element('pagination/pagination_list_count_element');?>
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive1 nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th scope="col"><?= ('Sr.No') ?></th>
                                <!--<th scope="col"><?= ('category_id') ?></th>-->
                <!--                <th scope="col"><?= ('Course') ?></th>-->
                                <th scope="col"><?= ('Title') ?></th>
                                <th scope="col"><?= ('Repeat Type') ?></th>
                <!--                <th scope="col"><?= ('Repeat Every') ?></th>
                                <th scope="col"><?= ('repeat_day1') ?></th>
                                <th scope="col"><?= ('repeat_day2') ?></th>
                                <th scope="col"><?= ('repeat_day3') ?></th>
                                <th scope="col"><?= ('repeat_day4') ?></th>
                                <th scope="col"><?= ('repeat_day5') ?></th>
                                <th scope="col"><?= ('repeat_day6') ?></th>
                                <th scope="col"><?= ('repeat_day7') ?></th>
                                <th scope="col"><?= ('repeat_day') ?></th>-->
                                <th scope="col"><?= ('Start Date') ?></th>
                                <th scope="col"><?= ('Start Time') ?></th>
                                <th scope="col"><?= ('Duration') ?></th>
                <!--                <th scope="col"><?= ('repeat_indefinitely') ?></th>
                                <th scope="col"><?= ('repeat_until_date') ?></th>
                                <th scope="col"><?= ('repeat_times') ?></th>
                                <th scope="col"><?= ('status') ?></th>
                                <th scope="col"><?= ('is_public') ?></th>
                                <th scope="col"><?= ('is_all_day') ?></th>-->
                                <th scope="col"><?= ('Cost') ?></th>
                                <!--<th scope="col"><?= ('Deposit') ?></th>-->
                <!--                <th scope="col"><?= ('location_id') ?></th>-->
                <!--                <th scope="col"><?= ('max_dog') ?></th>
                                <th scope="col"><?= ('waiver_id') ?></th>
                                <th scope="col"><?= ('created_by') ?></th>
                                <th scope="col"><?= ('modified_by') ?></th>
                                <th scope="col"><?= ('created_on') ?></th>
                                <th scope="col"><?= ('updated_on') ?></th>-->
                                <th scope="col" class="actions table-group-btn"><?= __('Actions') ?></th>
                            </tr>

                        </thead>


                        <tbody>
                          <?php 
                          $count=1;
                          foreach ($courseSchedule as $courseSchedule): ?>
                            <tr>
                                <td><?= $count; ?></td>
               <!--                <td><?= $this->Number->format($courseSchedule->id) ?></td>-->
                               <!--<td><?= $courseSchedule->has('category') ? $this->Html->link($courseSchedule->category->title, ['controller' => 'Categories', 'action' => 'view', $courseSchedule->category->id]) : '' ?></td>-->
                               <!--<td><?= $courseSchedule->has('course') ? $this->Html->link($courseSchedule->course->title, ['controller' => 'Courses', 'action' => 'view', $courseSchedule->course->id]) : '' ?></td>-->
                                <td><?= h($courseSchedule->title) ?></td>
                                <td><?php
                
                $repeat_type = $this->Number->format($courseSchedule->repeat_type);
                if($repeat_type == 1)
                {
                    echo "Daily";
                }
                elseif($repeat_type == 2)
                {
                    echo "Weekly";
                }
                elseif($repeat_type == 3)
                {
                    echo "Monthly";
                }
                elseif($repeat_type == 4)
                {
                    echo "Annually";
                }
                else
                {
                    echo "Not Repeated";
                }
                ?>

                                </td>
                <!--                <td><?= $this->Number->format($courseSchedule->repeat_every) ?></td>
                                <td><?= h($courseSchedule->repeat_day1) ?></td>
                                <td><?= h($courseSchedule->repeat_day2) ?></td>
                                <td><?= h($courseSchedule->repeat_day3) ?></td>
                                <td><?= h($courseSchedule->repeat_day4) ?></td>
                                <td><?= h($courseSchedule->repeat_day5) ?></td>
                                <td><?= h($courseSchedule->repeat_day6) ?></td>
                                <td><?= h($courseSchedule->repeat_day7) ?></td>
                                <td><?= $this->Number->format($courseSchedule->repeat_day) ?></td>-->
                                <td><?= h($courseSchedule->start_date) ?></td>
                                <td><?= h($courseSchedule->start_time) ?></td>
                                <td><?= h($courseSchedule->duration) ?></td>
                <!--                <td><?= h($courseSchedule->repeat_indefinitely) ?></td>
                                <td><?= h($courseSchedule->repeat_until_date) ?></td>
                                <td><?= $this->Number->format($courseSchedule->repeat_times) ?></td>
                                <td><?= h($courseSchedule->status) ?></td>
                                <td><?= h($courseSchedule->is_public) ?></td>
                                <td><?= h($courseSchedule->is_all_day) ?></td>-->
                                <td><?php
                                if($cost = $this->Number->format($courseSchedule->cost))
                                {
                                    echo '$ '.$cost;
                                }
                                else {
                                    echo 'Free';
                                }
                                    
                                    ?>
                                </td>
                                <!--<td><?= $this->Number->format($courseSchedule->deposit) ?></td>-->
                                <!--<td><?= $courseSchedule->has('location') ? $this->Html->link($courseSchedule->location->title, ['controller' => 'Locations', 'action' => 'view', $courseSchedule->location->id]) : '' ?></td>-->
                <!--                <td><?= $this->Number->format($courseSchedule->max_dog) ?></td>
                                <td><?= $this->Number->format($courseSchedule->waiver_id) ?></td>
                                <td><?= $this->Number->format($courseSchedule->created_by) ?></td>
                                <td><?= $this->Number->format($courseSchedule->modified_by) ?></td>
                                <td><?= h($courseSchedule->created_on) ?></td>
                                <td><?= h($courseSchedule->updated_on) ?></td>-->
                <!--                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $courseSchedule->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $courseSchedule->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $courseSchedule->id], ['confirm' => __('Are you sure you want to delete # {0}?', $courseSchedule->id)]) ?>
                                </td>-->
                                <td class="actions">
                            <?= $this->Html->link(__('<i class="fa fa-search"></i>'), ['action' => 'view', $courseSchedule->id],['escape'=>false]) ?>
                            <?= $this->Html->link(__('<i class="fa fa-edit"></i>'), ['action' => 'edit', $courseSchedule->id],['escape'=>false]) ?>
                            <?= $this->Form->postLink(__('<i class="fa fa-remove"></i>'), ['action' => 'delete', $courseSchedule->id], ['escape'=>false,'confirm' => __('Are you sure you want to remove '. h($courseSchedule->title).' course ?', $courseSchedule->id)]) ?>


                                </td>
                            </tr>
            <?php 
            $count++;
            endforeach; ?>



                        </tbody>
                    </table>

                </div>
<?php echo $this->element('pagination/pagination_element');?>