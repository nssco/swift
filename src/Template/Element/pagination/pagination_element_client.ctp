   <?php

    // Change the template for sorting to use it via AJAX
    $this->Paginator->templates([
    'number' => '<li><a class="pageNumberClass" href="#" onclick="ajaxPagination_1(\'{{url}}\');return false;">{{text}}</a></li>',
    'nextActive' => '<li><a class="pageNumberClass" href="#" onclick="ajaxPagination_1(\'{{url}}\');return false;">{{text}}</a></li>',
    'prevActive' => '<li><a class="pageNumberClass" href="#" onclick="ajaxPagination_1(\'{{url}}\');return false;">{{text}}</a></li>',
    ]);

    ?>    
        
    <div class="paginator">
        <div class="row">
            
            
            <div class="col-md-6 col-sm-6 col-xs-12">
                 <?php $pages_count = $this->Paginator->counter('{{pages}}'); 
                 if($pages_count>1) { ?>
                <div id="datatable-responsive_info" class="dataTables_info" role="status" aria-live="polite">
                         <?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?>
                </div>
                <?php } ?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12"> 
                
                    <ul class="pagination" style="float: right;margin-top: 0px;">
           <?php $pages_count = $this->Paginator->counter('{{pages}}'); 
            // echo $pages_count;
           if ($this->Paginator->counter('{{page}}') >= 2): 

           ?>

         <?= $this->Paginator->prev('< ' . __('previous')) ?>
         <?php endif; ?>
            <?= $this->Paginator->numbers(['modulus' => 3]); ?>
            <?php
            if ($this->Paginator->counter('{{page}}') != $pages_count):  ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        <?php endif;?>
                    </ul>
                
            </div>
        </div>
        
    </div>













