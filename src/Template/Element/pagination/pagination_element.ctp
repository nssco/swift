    <?php

    // Change the template for sorting to use it via AJAX
    $this->Paginator->templates([
    'number' => '<li><a class="pageNumberClass" href="javascript:void(0)">{{text}}</a></li>',
    'nextActive' => '<li><a class="pageNumberClass" href="javascript:void(0)">{{text}}</a></li>',
    'prevActive' => '<li><a class="pageNumberClass" href="javascript:void(0)">{{text}}</a></li>',
    ]);

    ?>    
    <div class="paginator">
        <div class="row">
            
            
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div id="datatable-responsive_info" class="dataTables_info" role="status" aria-live="polite">
            <?= $this->Paginator->counter(['format' => __('Showing {{page}} to {{pages}} of {{count}} entries')]) ?>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12"> 
                
                    <ul class="pagination" style="float: right;margin-top: 0px;">
            <?= $this->Paginator->prev(  __('Previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Next')) ?>
                    </ul>
                
            </div>
        </div>
        
    </div>












