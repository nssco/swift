<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript">
            var site_url = '<?= $SITEURL; ?>';
            var groups_id = Array();
            var thick = Array();
        </script>  
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title> <?php
            if (!empty($title)) {
                echo $project_name . ": " . $title;
            } else {
                echo 'Web Services';
            }
            ?></title>
        <?php
        echo $this->Html->meta('icon', 'img/favicon.ico');
        echo $this->Html->css("jquery-ui");
        echo $this->Html->css("all");
        echo $this->Html->css("global");
        echo $this->Html->css("new");
        echo $this->Html->css("settings");
        echo $this->Html->css("pagination");
        echo $this->Html->css("pagination");
        echo $this->Html->css('layers');
        echo $this->Html->css('custom');
        echo $this->Html->css('font-awesome.min');
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('jquery.dataTables.min');
        echo $this->Html->script('jquery.min');
        echo $this->Html->script("bootstrap.min");
        echo $this->Html->script("jquery.dataTables.min");
        echo $this->Html->script("jquery.ui.min");
        echo $this->Html->script('sweetalert.min');
        echo $this->Html->css('sweetalert');
        echo $this->Html->script("jquery.validate.min");
        echo $this->Html->script("validation");
        ?>
    </head>
    <body>
        <div id="headerWrap" style="background:#000; position:relative;">
            <center><div class="col-md-6 texthedh1"><?php echo $this->Html->image("new-logo.png", ['class' => 'img-responsive']); ?></div></center>
            <div id="headerContainer">
                <div id="logo"><a href="<?= $SITEURL ?>setting"><?php echo $this->html->image("logo.png"); ?></a></div>
                <div class="col-md-3 pull-right right-box"> <center><div class="col-md-4 col-sm-12 right-none-div">
          <!--                  <a href="<?php echo $SITEURL; ?>pages/view_cart" class="">
                                <div class="cart right-spce"><?php echo $this->Html->image("cart.png", ['class' => 'img-responsive']); ?> <span class="img"><span class="number cart_total"><?php echo $get_cartitem; ?></span><?php echo $this->html->image("arrow.png"); ?></span> </div>
                            </a>-->
                            <?php if (empty($this->request->session()->read('Smtp'))) {
                                ?>
                            </div>
                        </center>
                        <div class="col-md-8 col-sm-12 pdg-nonne">
                            <a href="<?php echo $SITEURL; ?>setting/users/signin" class="singin"> Sign In</a>
                        <?php
                        } else if (!empty($this->request->session()->read('Smtp'))) {
                            echo $this->element("smtpmenu");
                        }
                        if ($this->request->params['action'] == "index" || $this->request->params['action'] == "filter") {
                            $class = "extra-width";
                        } else {
                            $class = "";
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="blankSpacer"></div>
        <div id="pageContent">
            <noscript><span style="width: 100%; margin: 0 auto;
                            text-align: center; position: absolute; color: red; font-size: 18px; font-weight: bold;     margin-top: -14px;">This website requires javascript to be enabled.</span></noscript>
            <div id="pageWrap" class="container top-mr <?php echo $class; ?>">
                <div class="row">
