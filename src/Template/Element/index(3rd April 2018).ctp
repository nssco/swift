<?php
if(isset($products) && count($products) > 0) {
    $extension = array("jpg", "jpeg", "png", "gif");
    $count = $offset+1;; foreach ($products as $pro): ?>
    <tr>
        <td><?= ($pro->item_ser_no != '' ? $pro->item_ser_no : '--'); ?></td>
        <td>
            <?php
            $i=0;
            if(count($get_mtr) > 0){

                foreach ($get_mtr as $key => $mtr_file) {
                    if($mtr_file['item_ser_no'] == $pro->item_ser_no){
                       $get_extension = explode("_", $mtr_file['mtr']);
                       $get_extension = end($get_extension);
                       if(file_exists(WEBROOT_MTR_PATH.$mtr_file['mtr'])){ ?>
                       <a class="download_pdf" title="<?php echo !empty($mtr_file['notes'])?$mtr_file['notes']:'';?>" download href="<?php echo $SITEURL;?>uploads/mtr/<?php echo $mtr_file['mtr'];?>"><?php echo $get_extension;?></a><br />
                       <?php    }
                       $i++; }
                   }
               } 
               if($i==0){
                echo '--';
            }
            ?>
        </td>
        <td><?= ($pro->tag_no != '' ? $pro->tag_no : '--'); ?></td>
        <td><?= ($pro->group->name != '' ? $pro->group->name : '--'); ?></td>
        <td align="center">1</td>
        <td><?= ($pro->grade != '' ? $pro->grade : '--'); ?></td>
        <td><?= ($pro->thickness != '' ? $pro->thickness : '--'); ?></td>
        <td><?= ($pro->dimensions != '' ? $pro->dimensions : '--'); ?></td>
        <td><?= ($pro->lbs != '' ? $pro->lbs : '--'); ?></td>
        <td><?= ($pro->sf != '' ? $pro->sf : '--'); ?></td>
        <td><?= ($pro->heat != '' ? $pro->heat : '--'); ?></td>
        <td><?= ($pro->slab != '' ? $pro->slab : '--'); ?></td>
        <td><?= ($pro->country_of_origin != '' ? $pro->country_of_origin : '--'); ?></td>
        <td><?= ($pro->mill_name != '' ? $pro->mill_name : '--'); ?></td>
        <td align="right"><?= ($pro->price_cwt != '' ? '$'.number_format($pro->price_cwt,2) : '--'); ?></td>
        <td align="right"><?= ($pro->total_price != '' ? '$'.number_format($pro->total_price,2) : '--'); ?></td>
        <td align="center"><center>
            <?php if($pro->mtr != '' && file_exists(WEBROOT_MTR_PATH.$pro->mtr)){ ?>
            <div class="tooltip" style="color:#000;"><a href="<?php echo $SITEURL;?>uploads/mtr/<?php echo $pro->mtr;?>" target="_blank" title="View MTR"><?php echo $this->Html->image("privew.png");?><span class="tooltiptext tooltip-top">View MTR</span></a>  </div>
            <div class="tooltip" style="color:#000;"><a href="<?php echo $SITEURL;?>uploads/mtr/<?php echo $pro->mtr;?>" download title="Download MTR"><?php echo $this->Html->image("download.png");?><span class="tooltiptext tooltip-top">Download MTR</span></a></div>
            <?php  }else{echo "--";} ?> 
        </center></td>
        <td><center>
            <div class="tooltip" style="color:#000;"><a href="javascript:void(0)" class="add_to_cart" id="<?php echo md5($pro->id);?>" title="Add to Cart"><?php echo $this->Html->image("cart-icon.png");?><span class="tooltiptext tooltip-top">Add to Cart</span></a></div>
        </center>
    </td>
</tr>
<?php $count = $count + 1; endforeach;
}else { ?>
<tr>
    <td class="text-center" colspan="16">
        No Products to Show
    </td>
</tr>
<?php } ?>


