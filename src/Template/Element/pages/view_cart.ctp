<?php
if(isset($get_cart_data) && count($get_cart_data) > 0) {
 $extension = array("jpg", "jpeg", "png", "gif", "pdf");
 $grand_total = 0;
 $j=0;
 $i=1; 
 foreach ($get_cart_data as $pro):
 if($pro['products']['subprime'] == 1){
        $show_star = "<span class='asterisk_sign' style='color: #000; font-size:11px;font-weight:bold;width: 100%;
    float: left;'>(subprime)</span>";
      }else{
        $show_star = '';
      }
 $grand_total += $pro['price'];

 ?>
 <tr class="list_cart_product show_product_incart" id="<?php echo $pro['products']['id'];?>">
    <td class="td_order" bgcolor="#0078ae"><?=$i;?></td>
   
    <td><?php if($pro['products']['tag_no'] != '') { echo $pro['products']['tag_no'].$show_star; } elseif(isset($pro['tag_no'] )) { echo $pro['tag_no'] .$show_star; } else { echo "---"; } ?></td>
    <td align="center"><?=$pro['quantity'];?></td>
    <td><?php if($pro['products']['grade'] != '') { echo $pro['products']['grade']; } elseif(isset($pro['grade'])) { echo $pro['grade']; } else { echo "---"; } ?></td>
    <td><?php if($pro['products']['thickness'] != '') { echo $pro['products']['thickness']; } elseif(isset($pro['thickness'])) { echo $pro['thickness']; } else { echo "---"; }?></td>
    <td><?php if($pro['products']['dimensions'] != '') { echo $pro['products']['dimensions']; } elseif(isset($pro['dimensions'])) { echo $pro['dimensions']; } else { echo "---";} ?></td>
    <td><?php  if($pro['products']['lbs'] != '') { echo $pro['products']['lbs']; } elseif(isset( $pro['lbs'])){ echo $pro['lbs']; } else { echo "---"; }?></td>
   
    <td><?php if($pro['products']['country_of_origin'] != '') { echo ucwords(strtolower($pro['products']['country_of_origin'])); } elseif(isset($pro['country_of_origin'])) { echo ucwords(strtolower($pro['country_of_origin'])); } else  { echo "---"; } ?></td>
    <td><?php if($pro['products']['mill_name'] != '') { echo ucwords(strtolower($pro['products']['mill_name'])); } elseif(isset($pro['mill_name'])) { echo ucwords(strtolower($pro['mill_name'])); } else { echo "---"; } ?></td>
    <td align="right"><?php if($pro['products']['price_cwt'] != '') { echo '$'.number_format($pro['products']['price_cwt'],2); } elseif(isset($pro['price_cwt'])) { echo '$'.number_format($pro['price_cwt'],2);} else { echo "---";}?></td>
    <td align="right"><?= ($pro['price'] != '' ? '$'.number_format($pro['price'],2) : '---'); ?></td>
    <td align="center"><center>
            <?php if($pro['products']['mtr'] != '' && file_exists(WEBROOT_MTR_PATH.$pro['products']['mtr'])){ ?>
            <a href="<?php echo $SITEURL;?>uploads/mtr/<?php echo $pro['products']['mtr'];?>" target="_blank" title="Test Reports"><div class="bluee-btn1"><span class="glyphicon glyphicon-list-alt blk-bg" aria-hidden="true"></span> Test Reports</div></a>
          
        <?php  }elseif(isset($pro['mtr']))
            {
    if($pro['mtr']!= '' && file_exists(WEBROOT_MTR_PATH.$pro['mtr'])){ 
       echo'<a href="'.$SITEURL.'uploads/mtr/'.$pro['mtr'].'" target="_blank" title="Test Reports"><div class="bluee-btn1"><span class="glyphicon glyphicon-list-alt blk-bg" aria-hidden="true"></span> Test Reports</div></a>';

    }
    else { echo "---"; }
}
    else { echo "---"; } ?> 
</center></td>
    <td><center>
        <a href="javascript:void(0)" class="delete_cartitem" id="<?php echo md5($pro['id']);?>" title="Delete"><div class="bluee-btn">
                    <span class="glyphicon glyphicon-trash blk-bg" aria-hidden="true"></span> Delete
                    </div></a>
     
    </center>
        </td>
</tr>
<?php 
if(in_array($pro['product_id'],$product_idss) &&  count($get_cart_data) > 0 && $pro['products']['tag_no']=='') { ?>
 <tr class="err"><td colspan="11" style="color:red;">Tag Number <?php echo $pro['tag_no'];?> is no longer available for sale.</td></tr>
<?php  }  $i++; $j++; endforeach;?>
<tr class="list_cart_product">
    <td style="text-align: right;" scope="col" colspan="10"><strong>Total</strong></td>
    <td align="right"><strong class="calculate_grand_total">$<?php echo number_format($grand_total,2);?></strong></td>
    <td></td>
    <td></td>
</tr>
<tr class="no_products" style="display: none;">
    <td class="text-center" colspan="16">
        No Products to Show
    </td>
</tr>
<?php }else { ?>
<tr>
    <td class="text-center" colspan="16">
        No Products to Show
    </td>
</tr>
<?php } ?>


