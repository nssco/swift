
<?php
if(!empty($count_cartitem)){
 if($count_cartitem > 0){
    $display = "";
} else{
    $display = "none";
} 
}?>
 <div class="table-responsive table-div product_table">
            <?php echo $this->element('pagination/pagination_list_count_element_client'); ?><!-- to Show nuber sorting -->
            <table id="ifta_table"  width="100%" border="1" class="font-size" cellpadding="5" cellspacing="5">
              <thead>
              <tr>
                <td width="10%" valign="top" bgcolor="#f0f0f0" class="tag_number"><strong>Tag #</strong></td>
                <td width="5%" align="center" valign="top" bgcolor="#f0f0f0"><strong>Quantity</strong></td>
                <td width="12%" valign="top" bgcolor="#f0f0f0"><strong>Grade</strong></td>
                <td width="4%" valign="top" bgcolor="#f0f0f0"><strong>Thickness<br>
                  (in)</strong></td>
                <td width="17%" valign="top" bgcolor="#f0f0f0"><strong>Dimensions<br>
                  (in)</strong></td>
                <td width="5%" valign="top" bgcolor="#f0f0f0"><strong>Weight<br>
                  (lb)</strong></td>
                <td width="16%" valign="top" bgcolor="#f0f0f0"><strong>Country of Origin</strong></td>
                <td width="18%" valign="top" bgcolor="#f0f0f0"><strong>Mill Name</strong></td>
                <td width="6%" align="right" valign="top" bgcolor="#f0f0f0"><strong>Price/CWT</strong></td>
                <td width="5%" align="right" valign="top" bgcolor="#f0f0f0"><strong>Price/EA</strong></td>
                <td width="10%" valign="top" bgcolor="#f0f0f0"><strong></strong></td>
                <td width="15%" valign="top" bgcolor="#f0f0f0">&nbsp;</td>
              </tr>
     
       </thead>
<?php
//print_r($products);
if(isset($products) && count($products) > 0) {
    $extension = array("jpg", "jpeg", "png", "gif", "pdf");
     foreach ($products as $pro): 
       if($pro['subprime'] == 1){
        $show_star = "<span class='asterisk_sign' style='color: #000; font-size:11px;font-weight:bold;width: 100%;
    float: left;'>(subprime)</span>";
      }else{
        $show_star = '';
      }
    $show_delete = $this->Custom->showDeletebutton($pro->id,$cookie);
    if($show_delete > 0) { 
      
        $class = "delete_from_cart";
          $title = "Delete";
          $div_class = 'green-icon';
    }else{

        $class = "add_to_cart";
        $title = "Add to Cart";
        $div_class = '';
    }

    ?>
    <tr>
        <td><?= ($pro->tag_no != '' ? $pro->tag_no.$show_star : '--'); ?></td>
        <td align="center">1</td>
	<?php $gradecount=$this->Custom->getgradecount($pro->id,$pro->grade); ?>
        <td><?= ($pro->grade != '' ? $pro->grade : '--'); ?></td>
        <td><?= ($pro->thickness != '' ? $pro->thickness : '--'); ?></td>
        <td><?= ($pro->dimensions != '' ? $pro->dimensions : '--'); ?></td>
        <td><?= ($pro->lbs != '' ? $pro->lbs : '--'); ?></td>
       
        <td><?= ($pro->country_of_origin != '' ? ucwords(strtolower($pro->country_of_origin)) : '--'); ?></td>
        <td><?= ($pro->mill_name != '' ? ucwords(strtolower($pro->mill_name)) : '--'); ?></td>
        <td align="right"><?= ($pro->price_cwt != '' ? '$'.number_format($pro->price_cwt,2) : '--'); ?></td>
        <td align="right"><?= ($pro->total_price != '' ? '$'.number_format($pro->total_price,2) : '--'); ?></td>
        
        <td align="center"><center>
            <?php if($pro->mtr != '' && file_exists(WEBROOT_MTR_PATH.$pro->mtr)){ ?>
            <a href="<?php echo $SITEURL;?>uploads/mtr/<?php echo $pro->mtr;?>" target="_blank" title="Test Reports"><div class="bluee-btn"><span class="glyphicon glyphicon-list-alt blk-bg" aria-hidden="true"></span> Test Reports</div></a>
          
        <?php  }else{echo "--";} ?> 
</center></td>
    <td class="item_in_cart<?php echo md5($pro->id);?>"><center>
        <a href="javascript:void(0)" class="<?php echo $class;?>" id="<?php echo md5($pro->id);?>" title="<?php echo $title;?>"><div class="bluee-btn <?php echo $div_class;?>">
                    <span class="glyphicon glyphicon-shopping-cart blk-bg" aria-hidden="true"></span> <?php echo $title;?>
                    </div></a>
     
    </center>
        </td>
      
    </tr>
<?php endforeach;
}else { ?>
    <tr>
        <td class="text-center" colspan="16">
            No Products to Show
        </td>
    </tr>
<?php } ?>
 </table>
</div>



<?php
    if($products_count > 10) {
 echo $this->element('pagination/pagination_element_client'); 

} ?>




