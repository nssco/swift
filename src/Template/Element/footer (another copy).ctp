</div>
</div>
<div>
  <footer id="foot" class="subPages">
    <div class="footerContain">
      <div class="brochure tpbttn tpbtnn">          
        <p><a href="http://www.nssco.com/" target="_blank">Go to www.nssco.com</a></p>
      </div>
      <div class="brochure">          
        <p><a href="http://www.nssco.com/assets/pdf/NSSCO_BROCHURE.pdf" target="_blank">Download Our Brochure</a></p>
      </div>
      <div class="f1"><a href="<?php echo $SITEURL;?>" target="_blank"><?php echo $this->Html->image("footer-logo.png");?></a></div>
      <div class="f2">
        <h4>Houston Sales:</h4>
        <p>713-980-5800</p>
      </div>
      <div class="f3">
        <h4>Beaumont  Sales:</h4>
        <p>409-832-5604</p>
      </div>
      <div class="f4">
        <h4>Corporate Office:</h4>
        <p>713-453-3533</p>
      </div>
    </div>
  </footer>
</div>
</div>


<div class="floatingBtn">
  <div class="floatTab">
    <p><a href="mailto:sales@nssco.com"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;<span>Email Us</span></a></p>
  </div>
</div>
<?php echo $this->Html->script('custom');
echo $this->Html->script("jquery_mask");?>
<script>
  var thick='';
   var htm='';
   var htm2='';
   var seen = {};
   var c=0;
   var d=0;
  $(document).ready(function() {
    function setHeight() {
      windowHeight = $(window).innerHeight();
      $('.overlay').css('min-height', windowHeight - 200);
    };
    setHeight();

    $(window).resize(function() {setHeight();});
  });

  jQuery(document).ready(function($) {
    jQuery("*").find("a[href='" + window.location.href + "']").each(function() {
      jQuery(this).addClass("active");
    })
  });

  $(document).ready(function(){
    $(".success_message,.error_message").delay(5000).hide("slow");
    $("#datepicker").datepicker(
      { minDate: 0,dateFormat: 'mm-dd-yy'}
      );

  });

  
  function ajaxPagination_1(ajaxURL, group_id) {
    if (typeof (group_id) === 'undefined')
      group_id = '';
   
    var paginationCountChange = $('#pageListCount option:selected').val();
    if(typeof(paginationCountChange) === 'undefined')
    paginationCountChange=50;
    
    var group_id = group_id;
    
    
    if (paginationCountChange != "") {
      ajaxURL = ajaxURL + '?paginationCountChange=' + paginationCountChange;
    }
    if (group_id != "") {

      ajaxURL = ajaxURL + '&group_id=' + group_id;
    }
    $('.th a.active').each(function( index ) {
    if($(this).text()!="View All" && typeof($(this)) !== 'undefined')
    {
   

      thk=$(this).text().split('(');
      thick=thick+thk[0]+',';
    
    }
    });
    //console.log(thick);
    ajaxURL = ajaxURL + '&thickness=' + thick;
    $.ajax({
      url: ajaxURL,
      beforeSend: function () {
        $("#divUpdate").html('<div class="loading-main"> <?= $this->Html->image('loading.gif', array('alt' => 'Loading...', 'id' => 'loading-image')); ?></div>');
      },
      success: function (data) {
         console.log($('.gl a.active').text());
          $('.gl a.active').each(function( index ) {
    if($(this).text()!='View All')
    {
      $('#filterdiv').show();
    if(c==0)
    {
      arr=$(this).text().split('(');
      idd=$(this).attr('id');
      htm2='<span class="gd" style="cursor:pointer; background:#0078ae;  color:#fff; padding:0px 0px 0px 10px; border-radius:3px; font-size: 12px; margin:0 0px; display:inline-block;" id="'+idd+'">'+arr[0]+'<i class="fa fa-times-circle" style="background:#000; padding:7px; margin-left:10px; border-top-right-radius: 3px;border-bottom-right-radius: 3px;" aria-hidden="true" onclick="reversePagination(this);"></i></span>';
      //htm=htm2+htm;
      c=0;
    }
    else
    {
      $(this).removeClass('active');
    }
      
      
    }
    });
   
    $('.th a.active').each(function( index ) {
    if($(this).text()!='View All')
    {

      $('#filterdiv').show();
       arr=$(this).text().split('(');
       if (seen[arr[0]])
       {
        //$(this).remove();
       }
        else
        {
          
        seen[arr[0]] = true;
      htm=htm+'<span style="cursor:pointer; background:#0078ae;  color:#fff; padding:0px 0px 0px 10px; border-radius:3px; font-size: 12px; margin:0 0px; display:inline-block;" class="gt">'+arr[0]+'<i class="fa fa-times-circle" style="background:#000; padding:7px; margin-left:10px; border-top-right-radius: 3px;border-bottom-right-radius: 3px;" aria-hidden="true" onclick="reversePagination(this);"></i></span>';
      
      }
    }
    });


  $('#filterUpdate').html(htm2+htm);
        $("#divUpdate").html(data);
        $('[data-toggle="tooltip"]').tooltip();
        $('li.active').addClass('disabled');
        $('li.active a').attr("href",'javascript:void(0)');

      }
    });
   
  }
function ajaxthickness(ajaxURLNEW, group_id)
{
  if (typeof (group_id) === 'undefined')
      group_id = '';

    var paginationCountChange = $('#pageListCount option:selected').val();
    if(typeof(paginationCountChange) === 'undefined')
    paginationCountChange=50;
    
    var group_id = group_id;
    
    if (paginationCountChange != "") {
      ajaxURLNEW = ajaxURLNEW + '?paginationCountChange=' + paginationCountChange;
    }
    if (group_id != "") {

      ajaxURLNEW = ajaxURLNEW + '&group_id=' + group_id;
    }
   
    $.ajax({
      url: ajaxURLNEW,
      beforeSend: function () {
        $("#divUpdate").html('<div class="loading-main"> <?= $this->Html->image('loading.gif', array('alt' => 'Loading...', 'id' => 'loading-image')); ?></div>');
      },
      success: function (data) {
         $('.th').html(data);
        $('[data-toggle="tooltip"]').tooltip();
        
        $('li.active').addClass('disabled');
        $('li.active a').attr("href",'javascript:void(0)');
      }
    });
    
}
function reversePagination(obj)
{
   var ajaxURL='<?php echo $this->Url->build('/index.php/pages/index/', true); ?>';
   var ajaxURLNEW='<?php echo $this->Url->build('/index.php/pages/thickness/', true); ?>';
   var ajaxURLCount='<?php echo $this->Url->build('/index.php/pages/updatecount/', true); ?>';
   var tx='';
   var grp='';
   if($(obj).parent().next().length!=0 || $(obj).parent().prev().length!=0)
   {

       htm='';
       thick='';
       seen={};
       htm2='';
       txtt=$(obj).parent().text();
       //console.log(txtt);
        $(obj).parent().remove();
       $('#tt').addClass('active');
        $('.th a.active').each(function( index ) {

          if($(this).attr('id')==$.trim(txtt))
          {
            d=1;
           $(this).removeClass('active');
          }
        });
         $('.gl a.active').each(function( index ) {
          ck= $(this).text().split('(');
          //console.log(ck);
          if( $.trim(ck[0])==$.trim(txtt))
          {
            c=1;
           $(this).removeClass('active');
          }
        });
        id =  $('.gd').attr('id');
        if (typeof (id) === 'undefined')
          id = '';
        //console.log(id);
        $('.gd').each(function( index ) {
        if($(this).text()!='View All')
        {
      arr=$(this).text();
      idd=$(this).attr('id');
      htm2='<span class="gd" style="cursor:pointer; background:#0078ae;  color:#fff; padding:0px 0px 0px 10px; border-radius:3px; font-size: 12px; margin:0 0px; display:inline-block;" id="'+idd+'">'+arr+'<i class="fa fa-times-circle" style="background:#000; padding:7px; margin-left:10px; border-top-right-radius: 3px;border-bottom-right-radius: 3px;" aria-hidden="true" onclick="reversePagination(this);"></i></span>';
      //htm=htm2+htm;
      
      
    }
    });
   
    $('.gt').each(function( index ) {
    if($(this).text()!='View All')
    {
      $('#filterdiv').show();
       arr=$(this).text();
       if (seen[arr[0]])
       {
        //$(this).remove();
       }
        else
        {
        seen[arr] = true;
      htm=htm+'<span style="cursor:pointer; background:#0078ae;  color:#fff; padding:0px 0px 0px 10px; border-radius:3px; font-size: 12px; margin:0 0px; display:inline-block;" class="gt">'+arr+'<i class="fa fa-times-circle" style="background:#000; padding:7px; margin-left:10px; border-top-right-radius: 3px;border-bottom-right-radius: 3px;" aria-hidden="true" onclick="reversePagination(this);"></i></span>';
      }
    }
    });
    $('.gt').each(function( index ) {
    if($(this).text()!="View All" && typeof($(this)) !== 'undefined')
    {
   

      thk=$(this).text();
      thick=thick+thk+',';
    
    }
    });
        //console.log(htm2);
        ajaxPagination_1(ajaxURL, id);
       ajaxthickness(ajaxURLNEW, id);
        updatecount(ajaxURLCount,id);

   }
   else
   {
      htm2='';
      htm='';
      seen={};
      thick='';
      d=0;
      c=0;
      $(obj).parent().remove();
      
      $('#filterdiv').hide();
        id = 0;
        $('.th a').removeClass('active');
        $('.gl a').removeClass('active');
       $('.get_product_bygroup.active,.view_all_product.active').removeClass('active');
            $('#0').addClass('active');
             $('#tt').addClass('active');
        
        ajaxPagination_1(ajaxURL, id);
        ajaxthickness(ajaxURLNEW, id);
        updatecount(ajaxURLCount,id);
        
   }
}
function resetall()
{
      var ajaxURL='<?php echo $this->Url->build('/index.php/pages/index/', true); ?>';
   var ajaxURLNEW='<?php echo $this->Url->build('/index.php/pages/thickness/', true); ?>';
   var ajaxURLCount='<?php echo $this->Url->build('/index.php/pages/updatecount/', true); ?>';
      $('#filterUpdate').html('');
      $('#filterdiv').hide();
       var id = 0;
       $('.get_thick_bythick').removeClass('active');
       $('.th a').removeClass('active');
       $('.get_product_bygroup.active,.view_all_product.active').removeClass('active');
            $('#0').addClass('active');
             $('#tt').addClass('active');
        htm2='';
        htm='';
        seen={};
        thick='';
        ajaxPagination_1(ajaxURL, id);
        updatecount(ajaxURLCount,id);
        ajaxthickness(ajaxURLNEW, id);
         //$(document).find('#0.0-').trigger("click");
}

function updatecount(ajaxURLCount, group_id)
{

     if (typeof (group_id) === 'undefined')
      group_id = '';
   
     ajaxURLCount = ajaxURLCount + '?paginationCountChange=' + 50;
    
    var group_id = group_id;
    
   
    if (group_id != "") {

      ajaxURLCount = ajaxURLCount + '&group_id=' + group_id;
    }

    $('.th a.active').each(function( index ) {
    if($(this).text()!="View All" && typeof($(this)) !== 'undefined')
    {
   

      thk=$(this).text().split('(');
      thick=thick+thk[0]+',';
    
    }
    });
    //console.log(thick);
    ajaxURLCount = ajaxURLCount + '&thickness=' + thick;
    $.ajax({
      url: ajaxURLCount,
      beforeSend: function () {
        $("#divUpdate").html('<div class="loading-main"> <?= $this->Html->image('loading.gif', array('alt' => 'Loading...', 'id' => 'loading-image')); ?></div>');
      },
      success: function (data) {
         $('.gl').html(data);
    }


      
    });
}
</script> 
<div class="overlay">
  <div class="imageHol">  
    <?php echo $this->Html->image('pageloader.gif'); ?>
  </div>
</div>


</body>

</html>
