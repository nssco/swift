</div>
</div>
<div>
  <footer id="foot" class="subPages">
    <div class="footerContain">
      <!--<div class="brochure tpbttn tpbtnn">          
        <p><a href="http://www.nssco.com/" target="_blank">Go to www.nssco.com</a></p>
      </div>-->
      <div class="brochure">          
        <p><a href="http://www.nssco.com/assets/pdf/NSSCO_BROCHURE.pdf" target="_blank">Download Our Brochure</a></p>
      </div>
      <div class="f1"><a href="https://www.nssco.com" target="_blank"><?php echo $this->Html->image("footer-logo.png");?></a></div>
      <div class="f2">
        <h4>Houston Sales:</h4>
        <p>713-980-5800</p>
      </div>
      <div class="f3">
        <h4>Beaumont  Sales:</h4>
        <p>409-832-5604</p>
      </div>
      <div class="f4">
        <h4>Corporate Office:</h4>
        <p>713-453-3533</p>
      </div>
    </div>
  </footer>
</div>
</div>
<!--<div class="floatingBtn">
  <div class="floatTab">
    <p><a href="mailto:sales@nssco.com"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;<span>Email Us</span></a></p>
  </div>
</div>-->
<?php echo $this->Html->script('custom');
echo $this->Html->script("jquery_mask");?>
<script>
   var seen = {};
   $(document).on('click','.checkgroup',function()
     {
        var ajaxURL='<?php echo $this->Url->build('/index.php/pages/index/', true); ?>';
       var ajaxURLNEW='<?php echo $this->Url->build('/index.php/pages/thickness/', true); ?>';
       var ajaxURLCount='<?php echo $this->Url->build('/index.php/pages/updatecount/', true); ?>';
        id = $(this).parent('div').next('a').attr("id");
        //console.log($(this).is(":checked"));
        if(id=='0')
        {
          groups_id=[];
          htm2='';
        }
        else
        {
          //console.log($(this).prev('a'));
          if($(this).is(":checked"))
          {
              $(this).prop('checked', true);
          }
           else
          {
             var ggid=$(this).parent('div').next('a').attr("id");
              $(this).prop('checked', false);
              groups_id.splice( $.inArray(ggid, groups_id), 1 );
             // console.log(groups_id);
              id=0;
          }
         
        }
       
        ajaxPagination_1(ajaxURL, id,$(this).next('a'));
        ajaxthickness(ajaxURLNEW, id);
         updatecount(ajaxURLCount,id);
     });
     $(document).on('click','.checkthick',function()
     {
         var ajaxURL='<?php echo $this->Url->build('/index.php/pages/index/', true); ?>';
        var ajaxURLNEW='<?php echo $this->Url->build('/index.php/pages/thickness/', true); ?>';
        var ajaxURLCount='<?php echo $this->Url->build('/index.php/pages/updatecount/', true); ?>';
        var idd=0;
         //console.log($(this).is(":checked"));
         if($(this).parent('div').next('a').attr('id')=='tt')
        {
          thick=[];
          htm='';
        }
        else
        {
          if($(this).is(":checked"))
        {
           thk=$(this).parent('div').next('a').text().split('(');
        thick.push(thk[0]);
         $(this).prop('checked', true);
        }
        else
        {
        
         tiid=$(this).parent('div').next('a').text().split('(');
          //console.log( tiid);
          thick.splice($.inArray(tiid[0], thick), 1 );
          //console.log( thick);
          $(this).prop('checked', false);
         
       }
       }
       
    ajaxPagination_1(ajaxURL, idd,'');
     ajaxthickness(ajaxURLNEW, idd);
     updatecount(ajaxURLCount,idd);
     });
    $(document).on('click','.get_product_bygroup',function(){
        var ajaxURL='<?php echo $this->Url->build('/index.php/pages/index/', true); ?>';
       var ajaxURLNEW='<?php echo $this->Url->build('/index.php/pages/thickness/', true); ?>';
       var ajaxURLCount='<?php echo $this->Url->build('/index.php/pages/updatecount/', true); ?>';
        id = $(this).attr("id");
        //console.log($(this).is(":checked"));
        if(id=='0')
        {
          groups_id=[];
          htm2='';
        }
        else
        {
          //console.log($(this).next('div .checkgroup').is(":checked"));
          if($(this).prev().children('.checkgroup').is(":checked"))
          {
            var ggid=$(this).attr("id");
              $(this).prev().children('.checkgroup').prop('checked', false);
              groups_id.splice( $.inArray(ggid, groups_id), 1 );
             // console.log(groups_id);
              id=0;
              
          }
           else
          {
             $(this).prev().children('.checkgroup').prop('checked', true);
          }
         
        }
       
        ajaxPagination_1(ajaxURL, id,$(this));
        ajaxthickness(ajaxURLNEW, id);
         updatecount(ajaxURLCount,id);
      });
    $(document).on('click','.get_thick_bythick',function(){
       var ajaxURL='<?php echo $this->Url->build('/index.php/pages/index/', true); ?>';
        var ajaxURLNEW='<?php echo $this->Url->build('/index.php/pages/thickness/', true); ?>';
        var ajaxURLCount='<?php echo $this->Url->build('/index.php/pages/updatecount/', true); ?>';
        var idd=0;
         //console.log($(this).prev().children('.checkthick').is(":checked"));
         if($(this).attr('id')=='tt')
        {
          thick=[];
          htm='';
        }
        else
        {
          if($(this).prev().children('.checkthick').is(":checked"))
        {
         
         tiid=$(this).text().split('(');
          //console.log( tiid);
          thick.splice($.inArray(tiid[0], thick), 1 );
          //console.log( thick);
          $(this).prev().children('.checkthick').prop('checked', false);
        }
        else
        {
        
           thk=$(this).text().split('(');
        thick.push(thk[0]);
         $(this).prev().children('.checkthick').prop('checked', true);
         
       }
       }
       
    ajaxPagination_1(ajaxURL, idd,'');
     updatecount(ajaxURLCount,idd);
      });
  $(document).ready(function() {
    function setHeight() {
      windowHeight = $(window).innerHeight();
      $('.overlay').css('min-height', windowHeight - 200);
    };
    setHeight();
    $(window).resize(function() {setHeight();});
  });
  jQuery(document).ready(function($) {
    jQuery("*").find("a[href='" + window.location.href + "']").each(function() {
      jQuery(this).addClass("active");
    })
  });
  $(document).ready(function(){
    $(".success_message,.error_message").delay(5000).hide("slow");
    $("#datepicker").datepicker(
      { minDate: 0,dateFormat: 'mm-dd-yy'}
      );
  });
  
  function ajaxPagination_1(ajaxURL, group_id,obj) {
    if (typeof (group_id) === 'undefined')
    {
      group_id=0;
      groups_id.push(0);
    }
      var htm2='';
      var htm='';
    var paginationCountChange = $('#pageListCount option:selected').val();
    if(typeof(paginationCountChange) === 'undefined')
    paginationCountChange=50;
    
     groups_id.push(group_id);
    
    console.log(groups_id);
    if (paginationCountChange != "") {
      ajaxURL = ajaxURL + '?paginationCountChange=' + paginationCountChange;
    }
     // groups_id=$.unique(groups_id);
      groups_id = $.unique(groups_id);
    //console.log(groups_id);
      var group_id=groups_id.toString();
      ajaxURL = ajaxURL + '&group_id=' + group_id;
   
    thick=$.unique(thick);
    var thick1=thick.toString();
    ajaxURL = ajaxURL + '&thickness=' + thick1;
    $.ajax({
      url: ajaxURL,
      beforeSend: function () {
        $('.loading-main').remove();
        $("#divUpdate").prepend('<div class="loading-main"> <?= $this->Html->image('loading.gif', array('alt' => 'Loading...', 'id' => 'loading-image')); ?></div>');
      },
      success: function (data) {
        console.log(groups_id);
        for (i = 0; i < groups_id.length; i++) { 
          if(groups_id[i]!=0)
          {
            console.log(groups_id[i]);
           $('#filterdiv').show();
          idgrade=$('.gl div a#'+groups_id[i]).text().split('(');
          console.log(idgrade[0]);
          if(idgrade[0]=='')
          {
          idgrade[0]=$('#'+groups_id[i]+'-g').text();
          }
          htm2=htm2+'<span class="gd" style="cursor:pointer; background:#0078ae;  color:#fff; padding:0px 0px 0px 10px; border-radius:3px; font-size: 12px;    margin: 15px 5px 0px 5px;display:inline-block;" id="'+groups_id[i]+'-g">'+idgrade[0]+'<i class="fa fa-times-circle" style="background:#000; padding:7px; border-top-right-radius: 3px;border-bottom-right-radius: 3px;" aria-hidden="true" onclick="reversePagination(this);"></i></span>';
           
        }
        }
        //console.log(thick);
         for (i = 0; i < thick.length; i++) { 
          if(thick[i]!=0)
          {
           $('#filterdiv').show();
          htm=htm+'<span class="gt" style="cursor:pointer; background:#0078ae;  color:#fff; padding:0px 0px 0px 10px; border-radius:3px; font-size: 12px;margin: 15px 5px 0px 5px; display:inline-block;">'+thick[i]+'<i class="fa fa-times-circle" style="background:#000; padding:7px;border-top-right-radius: 3px;border-bottom-right-radius: 3px;" aria-hidden="true" onclick="reversePagination(this);"></i></span>';
        }
        }
         if(htm=='' && htm2=='')
          {
            $('#filterdiv').hide();
          }
        $('#filterUpdate').html(htm2+htm);
        $("#divUpdate").html(data);
       
      }
    });
   
  }
function ajaxthickness(ajaxURLNEW, group_id)
{
    var paginationCountChange = $('#pageListCount option:selected').val();
    if(typeof(paginationCountChange) === 'undefined')
    paginationCountChange=50;
    
    
    if (paginationCountChange != "") {
      ajaxURLNEW = ajaxURLNEW + '?paginationCountChange=' + paginationCountChange;
    }
     var group_id=groups_id.toString();
      ajaxURLNEW = ajaxURLNEW + '&group_id=' + group_id;
     var thick1=thick.toString();
    //console.log(thick);
    ajaxURLNEW = ajaxURLNEW + '&thickness=' + thick1;
	 
    $.ajax({
      url: ajaxURLNEW,
      beforeSend: function () {
        $('.loading-main').remove();
        $("#divUpdate").prepend('<div class="loading-main"> <?= $this->Html->image('loading.gif', array('alt' => 'Loading...', 'id' => 'loading-image')); ?></div>');
      },
      success: function (data) {
	       $('.th').html(data);
        //$('[data-toggle="tooltip"]').tooltip();
        
        $('li.active').addClass('disabled');
        $('li.active a').attr("href",'javascript:void(0)');
      }
    });
    
}
function updatecount(ajaxURLCount, group_id)
{
   
     ajaxURLCount = ajaxURLCount + '?paginationCountChange=' + 50;
    
    var group_id=groups_id.toString();
    console.log(groups_id);
   
    
      ajaxURLCount = ajaxURLCount + '&group_id=' + group_id;
    
    
    var thick1=thick.toString();
    //console.log(thick);
    ajaxURLCount = ajaxURLCount + '&thickness=' + thick;
    $.ajax({
      url: ajaxURLCount,
      beforeSend: function () {
      $('.loading-main').remove();
        $("#divUpdate").prepend('<div class="loading-main"> <?= $this->Html->image('loading.gif', array('alt' => 'Loading...', 'id' => 'loading-image')); ?></div>');
      },
      success: function (data) {
         $('.gl').html(data);
    }
      
    });
}
function reversePagination(obj)
{
   var ajaxURL='<?php echo $this->Url->build('/index.php/pages/index/', true); ?>';
   var ajaxURLNEW='<?php echo $this->Url->build('/index.php/pages/thickness/', true); ?>';
   var ajaxURLCount='<?php echo $this->Url->build('/index.php/pages/updatecount/', true); ?>';
	 if($(obj).parent().next().length!=0 || $(obj).parent().prev().length!=0)
   {
       $(obj).parent().remove();
       //console.log($(obj).parent().attr("id"));
       id=0;
       if(typeof($(obj).parent().attr("id")) !== 'undefined')
       {
       var gid=$(obj).parent().attr("id").split('-');
        groups_id.splice( $.inArray(gid[0], groups_id), 1 );
        $('#'+gid[0]).prev('div .checkgroup').prop('checked',false);
       }
       else
       {
          var tid=$(obj).parent().text();
          //console.log(tid);
        thick.splice( $.inArray(tid, thick), 1 );
         $('#'+tid).prev('div .checkthick').prop('checked',false);
       }
        ajaxPagination_1(ajaxURL,id,'');
        ajaxthickness(ajaxURLNEW, id);
        updatecount(ajaxURLCount,id);
   }
   else
   {
      groups_id=[];
      thick=[];
      $(obj).parent().remove();
      
      $('#filterdiv').hide();
        id = 0;
        
        ajaxPagination_1(ajaxURL, id,'');
        ajaxthickness(ajaxURLNEW, id);
        updatecount(ajaxURLCount,id);
        
   }
}
function resetall()
{
      var ajaxURL='<?php echo $this->Url->build('/index.php/pages/index/', true); ?>';
   var ajaxURLNEW='<?php echo $this->Url->build('/index.php/pages/thickness/', true); ?>';
   var ajaxURLCount='<?php echo $this->Url->build('/index.php/pages/updatecount/', true); ?>';
      $('#filterUpdate').html('');
      $('#filterdiv').hide();
      groups_id=[];
      thick=[];
        id = 0;
        
        ajaxPagination_1(ajaxURL, id,'');
        ajaxthickness(ajaxURLNEW, id);
        updatecount(ajaxURLCount,id);
}
</script> 
<div class="overlay">
  <div class="imageHol">  
    <?php echo $this->Html->image('pageloader.gif'); ?>
  </div>
</div>
</body>
</html>
