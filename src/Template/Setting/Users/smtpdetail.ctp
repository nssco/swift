<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container getintouch-cls-info">
        <div class="row">
        <div class="col-md-10 col-sm-5 col-xs-6 width">
            <h3 class="heading btm-spce left-mr"> Add Smtp Details</h3>
          </div>
<!--        <div class="col-md-2 col-sm-7 col-xs-6">
            <a href="<?php echo $previous_page;?>"><button class="mrtppp null-top right-btn1">
            Back
            </button></a>
          </div>-->
      </div>
      
        <div class="col-md-6 col-sm-6 col-xs-12 pdddg nopdggg">
       <?php echo $this->Form->create("/setting", ['method' => 'post',"id" => "smtpdetail",'autocomplete' => "off"]);
       echo $this->Flash->render();
       ?>
<!--             <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Classname</label>
                <?php // echo $this->Form->input("classname",['class' => 'form-control1 common',"label" => false]);?>
              </div>-->
                <div class="form-group col-md-12">
                <label for="exampleInputPassword1">Host</label>
                <?php echo $this->Form->input("host",['class' => 'form-control1 common',"label" => false]);?>
              </div>
                <div class="form-group col-md-12">
                <label for="exampleInputPassword1">Port</label>
                    <?php echo $this->Form->input("port",['class' => 'form-control1 common',"label" => false,"type" =>'password']);?>
              </div>
             <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Timeout</label>
                <?php echo $this->Form->input("timeout",['label' => false, 'class' => "form-control1 common"]); ?>
            </div>
            <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Username</label>
            <?php echo $this->Form->input("username",['class' => 'form-control1 common',"label" => false]);?>
          </div>
            <div class="form-group col-md-12">
            <label for="exampleInputPassword1">Password</label>
            <?php echo $this->Form->input("password",['class' => 'form-control1 common',"label" => false]);?>
          </div>
            <div class="col-md-12">
            <button type="submit" class="btn btn-default btn_1">
            Submit
            </button>
          </div>
          </form>
      </div>
      </div>
<script>
    $(document).ready(function(){
   /******Add User form validation**********/
    jQuery("#smtpdetail").validate({
        // Specify the validation rules
        rules: {
        "classname": {
                required: true,
//                lettersOnly: true,
            },
            "host": {
                required: true,
                // lettersOnly: true,
            },
            "port": {
                required: true,
            },
            "timeout": {
                required: true,
            },
            
            "username": {
                required: true,
                email:true
            },
            "password": {
                required: true,
            },
        },
        // Specify the validation error messages
        messages: {
            "classname": {
                required: "Please enter class name."
            },
            "host": {
                required: "Please enter host."
            },
            "port": {
                required: "Please enter port.",
//                remote: "Email already exists"
            },
            "timeout": {
                required: "Please enter timeout.",
            
            },
            "username": {
                required: "Please enter smtp username.",
//                remote: "Email already exists"
            },
            "password": {
                required: "Please enter smtp password.",
            
            }
        }
    });
    });
</script>