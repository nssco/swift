<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container getintouch-cls-info">
    <div class="row">
        <div class="col-md-10 col-sm-5 col-xs-6 width">
            <h3 class="heading btm-spce left-mr"> Change password</h3>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 pdddg nopdggg">
        <?php
        echo $this->Form->create("/setting", ['method' => 'post', "id" => "change_password"]);
        echo $this->Flash->render();
        ?>
        <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Current Password</label>
<?php echo $this->Form->input('old_password', ['class' => 'form-control', 'label' => false, 'type' => 'password']); ?>
        </div>
        <div class="form-group col-md-12">
            <label for="exampleInputPassword1">Password</label>
<?php echo $this->Form->input('password', ['class' => 'form-control', 'label' => false, 'type' => 'password']); ?>
        </div>
        <div class="form-group col-md-12">
            <label for="exampleInputPassword1">Confirm Password</label>
<?php echo $this->Form->input('confirm_password', ['class' => 'form-control', 'label' => false, 'type' => 'password']); ?>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6"><button type="submit" class="btn btn-default btn_1">Change Password</button></div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6"><?php echo $this->Html->link(__('Back'), ['action' => 'smtpdetailedit/1'],['class'=>'btn btn-default btn_1']) ?></div>
        </form>
    </div>
</div>