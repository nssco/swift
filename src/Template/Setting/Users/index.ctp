<?php

/**
  * @var \App\View\AppView $this
  */
?>

<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><?= __('Smtp List') ?></h3>
        </div> 
        <div class="title_right">
            <div class="col-md-1 col-sm-1 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <span class="input-group-btn">
                        <?php // $this->Html->link(__('Add Smtp'), ['action' => 'smtpdetail'],['class'=>'btn btn-success']) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel table-responsive">
            <div class="x_content">
                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th scope="col"><?= ('Sr.No') ?></th>
                            <!--<th scope="col"><?= ('Classname') ?></th>-->
                            <th scope="col"><?= ('Host') ?></th>
                            <th scope="col"><?= ('Port') ?></th>
                            <th scope="col"><?= ('Timeout') ?></th>
                            <th scope="col"><?= ('Username') ?></th>
                            <th scope="col"><?= ('Password') ?></th>
                            <th scope="col"><?= ('Created on') ?></th>
                            
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
            <?php
            $count = 1;
            foreach ($Smtpdetails as $Smtpdetails): ?>
                        <tr class="clickable-row">
                            <td><?= $count; ?></td>
                            <!--<td><?= ($Smtpdetails->classname) ?></td>-->
                            <td><?= ($Smtpdetails->host) ?></td>
                            <td><?= ($Smtpdetails->port) ?></td>
                            <td><?= ($Smtpdetails->timeout) ?></td>
                            <td><?= ($Smtpdetails->username) ?></td>
                            <td><?= ($Smtpdetails->password) ?></td>
                            <td><?php echo date('d-m-Y',  strtotime($Smtpdetails->createdon)); ?></td>
                            <td class="actions">
                    <?php echo $this->Html->link(__('<i class="fa fa-edit"></i>'), ['action' => 'smtpdetailedit', $Smtpdetails->id],['escape'=>false,'title'=>'Edit','data-toggle'=>"tooltip", 'data-placement'=>"right",'class'=>'custom-tooltip']) ?>
                    <?php // $this->Form->postLink(__('<i class="fa fa-remove"></i>'), ['action' => 'delete', $Smtpdetails->id], ['title'=>"Delete",'data-toggle'=>"tooltip", 'data-placement'=>"right", 'class'=>'custom-tooltip','escape'=>false,'confirm' => __('Are you sure you want to delete # {0}?', $Smtpdetails->host),'title'=>'Delete']) ?>
                            </td>
                        </tr>
            <?php $count++; endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>    
