
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container getintouch-cls-info">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab">
        <div class="bhoechie-tab-content table-box active">
            <div class="row">
            <div class="col-md-9 col-sm-5 col-xs-12">
                <h3 class="heading">Request for order</h3>
              </div>
            <div class="col-md-3 col-sm-7 col-xs-12 text-right">
                <a href="<?php echo $previous_page;?>"><div class="more-products">
                Back
                </div></a>
              </div>
          </div>
            <h6 class="none">See More Table scroll to right <span class="glyphicon glyphicon-chevron-right color" aria-hidden="true"></span></h6>
            <div class="table-responsive">

            <table width="100%" border="1" class="font-size mtop-div" cellpadding="5" cellspacing="5" id="view_cart_table">
                <tr class="tbody">
                <td class="top" valign="top" bgcolor="#f0f0f0"><strong>#</strong></td>
                <td  class="top" valign="top" bgcolor="#f0f0f0"><strong>Tag Number</strong></td>
                <td  class="top" valign="top" align="center" bgcolor="#f0f0f0"><strong>Quantity</strong></td>
                <td  class="top" valign="top" bgcolor="#f0f0f0"><strong>Grade</strong></td>
                <td  class="top" valign="top" bgcolor="#f0f0f0"><strong>Thickness<br>
                  (in)</strong></td>
                <td class="top" valign="top" bgcolor="#f0f0f0"><strong>Dimensions<br>
                  (in)</strong></td>
                <td class="top" valign="top" bgcolor="#f0f0f0"><strong>Weight<br>
                  (lb)</strong></td>
                <td  class="top" valign="top" bgcolor="#f0f0f0"><strong>Country of Origin</strong></td>
                <td  class="top" valign="top" bgcolor="#f0f0f0"><strong>Mill Name</strong></td>
                <td  class="top" align="right" valign="top" bgcolor="#f0f0f0"><strong>Price/CWT</strong></td>
                <td  class="top" align="right" valign="top" bgcolor="#f0f0f0"><strong>Price/EA</strong></td>
                
              </tr>
              <?php
if(isset($list_user_order) && count($list_user_order) > 0) {
 $grand_total = 0;
 $i=1; foreach ($list_user_order as $pro):
 $grand_total += $pro['product']['total_price'];
 if($pro['product']['subprime'] == 1){
        $show_star = "<span class='asterisk_sign' style='color: #000; font-size:11px;font-weight:bold;width: 100%;
    float: left;'>(subprime)</span>";
      }else{
        $show_star = '';
      }

 ?>
 <tr class="list_cart_product show_product_incart">
    <td class="td_order" bgcolor="#0078ae"><?=$i;?></td>
  
    <td><?= ($pro['product']['tag_no'] != '' ? $pro['product']['tag_no']. $show_star : '--'); ?></td>
    <td align="center">1</td>
    <td><?= ($pro['product']['grade'] != '' ? $pro['product']['grade'] : '--'); ?></td>
    <td><?= ($pro['product']['thickness'] != '' ? $pro['product']['thickness'] : '--'); ?></td>
    <td><?= ($pro['product']['dimensions'] != '' ? $pro['product']['dimensions'] : '--'); ?></td>
    <td><?= ($pro['product']['lbs'] != '' ? $pro['product']['lbs'] : '--'); ?></td>
   
    <td><?= ($pro['product']['country_of_origin'] != '' ? ucwords(strtolower($pro['product']['country_of_origin'])) : '--'); ?></td>
    <td><?= ($pro['product']['mill_name'] != '' ? ucwords(strtolower($pro['product']['mill_name'])) : '--'); ?></td>
    <td align="right"><?= ($pro['product']['price_cwt'] != '' ? '$'.number_format($pro['product']['price_cwt'],2) : '--'); ?></td>
    <td align="right"><?= ($pro['product']['total_price'] != '' ? '$'.number_format($pro['product']['total_price'],2) : '--'); ?></td>
 
</tr>
<?php  $i++; endforeach;?>


<?php }else { ?>
<tr>
    <td class="text-center" colspan="16">
        No product to Show
    </td>
</tr>
<?php } ?>

                
              
              </table>
          </div>
            <h6 class="none">See More Table scroll to right <span class="glyphicon glyphicon-chevron-right color" aria-hidden="true"></span></h6>
           
          </div>
      </div>

    
      </div>