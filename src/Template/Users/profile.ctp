<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container getintouch-cls-info">
        <div class="row">
        <div class="col-md-10 col-sm-5 col-xs-6 width">
            <h3 class="heading btm-spce left-mr"> Update Account</h3>
          </div>
        <div class="col-md-2 col-sm-7 col-xs-6">
            <a href="<?php echo $SITEURL;?>pages/view_cart"><button class="mrtppp null-top right-btn1">
            Back
            </button></a>
          </div>
      </div>
        <div class="col-md-6 col-sm-6 col-xs-12 pdddg nopdggg">
       <?php echo $this->Form->create($get_user, ['method' => 'post',"id" => "profile"]);
       echo $this->Flash->render();
       ?>
             <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Email Address</label>
            <?php echo $this->Form->input("username",['class' => 'form-control1 common',"label" => false]);?>
          </div>
                      
             <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Name</label>
            <?php echo $this->Form->input("name",['label' => false, 'class' => "form-control1 common"]); ?>
          </div>
            <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Company Name</label>
            <?php echo $this->Form->input("company",['class' => 'form-control1 common',"label" => false]);?>
          </div>
           
            <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Phone Number</label>
            <?php echo $this->Form->input("phone",['class' => 'form-control1 common phone_masking',"label" => false]);?>
          </div>
           
           <div class="form-group col-md-12">
            <label class="checkbox-main margin-top-md"> <span class="checkmark"></span>
                 <input type="checkbox" value="1" <?php echo ($get_user['existing_user']==1?"checked":"");?> id="chkproperty" name="existing_user">
                I am a current customer</label>
          </div>
            <div class="col-md-12">
            <button type="submit" class="btn btn-default btn_1">
            Save
            </button>
          </div>
          </form>
      </div>
      </div>