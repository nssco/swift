
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container getintouch-cls-info">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab">
        <div class="bhoechie-tab-content table-box active">
            <div class="row">
            <div class="col-md-9 col-sm-5 col-xs-12">
                <h3 class="heading">Request for order</h3>
              </div>
            <div class="col-md-3 col-sm-7 col-xs-12 text-right">
                <a href="<?php echo $SITEURL;?>pages/view_cart"><div class="more-products">
                Back
                </div></a>
              </div>
          </div>
            <h6 class="none">See More Table scroll to right <span class="glyphicon glyphicon-chevron-right color" aria-hidden="true"></span></h6>
            <div class="table-responsive">

            <table width="100%" border="1" class="font-size mtop-div" cellpadding="5" cellspacing="5" id="view_cart_table">
                <tr class="tbody">
                <td class="top" valign="top" bgcolor="#f0f0f0"><strong>#</strong></td>
                <td  class="top" valign="top" bgcolor="#f0f0f0"><strong>Request for order date</strong></td>
                <td  class="top" valign="top" bgcolor="#f0f0f0"><strong>PO</strong></td>
                <td  class="top" valign="top" bgcolor="#f0f0f0"><strong>Need by date</strong></td>
                <td  class="top" valign="top" bgcolor="#f0f0f0"><strong>Comments</td>
                <td class="top" valign="top" bgcolor="#f0f0f0"><strong></td>
               
                
              </tr>
              <?php
if(isset($list_user_order) && count($list_user_order) > 0) {
 $i=1;
  foreach ($list_user_order as $pro):
  
    $change_date = date("Y-m-d",strtotime($pro['purchase_date']));
 ?>
 <tr class="list_cart_product show_product_incart">
    <td class="td_order" bgcolor="#0078ae"><?=$i;?></td>
  
    <td><?= ($pro['purchase_date'] != '' ? date("m-d-Y",strtotime($pro['purchase_date'])) : '--'); ?></td>
    <td><?= ($pro['customer']['po'] != '' ? $pro['customer']['po'] : '--'); ?></td>
    <td><?= ($pro['customer']['need_by_date'] != '' ? $pro['customer']['need_by_date'] : '--'); ?></td>
    <td><?= ($pro['customer']['comment'] != '' ? $pro['customer']['comment'] : '--'); ?></td>
    
    <td>
          <?php if($pro['item_count'] != ''){ ?>


        <a href="<?php echo $SITEURL;?>users/my_orders/<?php echo md5($change_date);?>" title="View items"><div class="bluee-btn">
                    <span class="glyphicon glyphicon-th-list blk-bg" aria-hidden="true"></span>&nbsp; <?php echo $pro['item_count'];?> items
                    </div></a>

            <?php } else { ?>


      <a href="javascript:void(0)" title="View items"><div class="bluee-btn">
                    <span class="glyphicon glyphicon-th-list blk-bg" aria-hidden="true"></span>&nbsp; 0 items
                    </div></a>


        <?php } ?>

    </td>
   
 
</tr>
<?php  $i++; endforeach;?>


<?php }else { ?>
<tr>
    <td class="text-center" colspan="16">
        No product to Show
    </td>
</tr>
<?php } ?>

              </table>
          </div>
            <h6 class="none">See More Table scroll to right <span class="glyphicon glyphicon-chevron-right color" aria-hidden="true"></span></h6>
           
          </div>
      </div>

    
      </div>