    <?php echo $this->element("smtpheader");    
    echo $this->fetch('content');
    if($this->request->params['action'] != "documentation"){
      echo $this->element("smtpfooter");
    }
    ?>
    <div class="overlay">
      <div class="imageHol">  
        <?php echo $this->Html->image('pageloader.gif'); ?>
      </div>
    </div>
    
    <script>
      $(document).ready(function(){
        $(".success_message,.error_message").delay(5000).hide("slow");
//        $('#ifta_table').dataTable({   
//         "searching": false,
//          "paging":   false,
//          "info":false,
//          aoColumnDefs: [
//   {
//      'bSortable': false,
//      'aTargets': ['tag_number',1,4,6,7,8,9,10,11 ]
//   }
// ]
//          // "ordering":false     
//       }); 
       $("#datepicker").datepicker(
        { minDate: 0,dateFormat: 'mm-dd-yy'}
        );
     });
   </script>