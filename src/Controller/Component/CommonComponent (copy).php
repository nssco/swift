<?php
namespace App\Controller\Component;
use Cake\Core\Configure;

use Cake\Controller\Component;
use Cake\Mailer\Email;
use Cake\Network\Http\Client;
use DOMDocument;
use DOMAttr;

// In a controller or table method.
use Cake\ORM\TableRegistry;

class CommonComponent extends Component
{
	    
    public function getRandomString($count) {
        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $count);
        return $randomString;
    }
    /****
     * Function to return data with respect to md5 format id
     * ****/
    public function generateXml($get_cart_data = null, $filePath = null, $data){
        // pr($data); die;

        $dom = new DOMDocument();

        $dom->encoding = 'utf-8';

        $dom->xmlVersion = '1.0';

        $dom->formatOutput = true;

        $mtr_url = Configure::read("MTR_URL");        

   $root = $dom->createElement('Order-list'); 

   $userDetails = $dom->createElement('Customer-details');
    $name     = $dom->createElement('Name', $data['name']);
     $userDetails->appendChild($name); 

     $company     = $dom->createElement('Company', $data['company']);
     $userDetails->appendChild($company); 

      $email     = $dom->createElement('Email', $data['email']);
     $userDetails->appendChild($email); 

     $phone     = $dom->createElement('Phone', $data['phone']);
     $userDetails->appendChild($phone); 

     $need_by_date     = $dom->createElement('Needed-by-date', $data['need_by_date']);
     $userDetails->appendChild($need_by_date); 

     if(isset($data['current_user'])){
        $current_user     = $dom->createElement('Curent-user', 'Yes');
     $userDetails->appendChild($current_user); 
     }

      $root->appendChild($userDetails);

  
    $grand_total = 0;
   for($i=0; $i<count($get_cart_data); $i++){
     
     $item_ser_no  =  $get_cart_data[$i]['products']['item_ser_no'];  

     $mtr  =  ($get_cart_data[$i]['products']['mtr'] != '' && file_exists(WEBROOT_MTR_PATH.$get_cart_data[$i]['products']['mtr']) ? $mtr_url . $get_cart_data[$i]['products']['mtr'] : ''); 

      $tag_number =   ($get_cart_data[$i]['products']['tag_no'] != '' ? $get_cart_data[$i]['products']['tag_no'] : '--');

      $group_name =  ($get_cart_data[$i]['products']['group']['name'] != '' ? $get_cart_data[$i]['products']['group']['name'] : '--');

     $quantity = $get_cart_data[$i]['quantity'];

     $grade = ($get_cart_data[$i]['products']['grade'] != '' ? $get_cart_data[$i]['products']['grade'] : '--');

     $thickness = ($get_cart_data[$i]['products']['thickness'] != '' ? $get_cart_data[$i]['products']['thickness'] : '--');

     $dimension = ($get_cart_data[$i]['products']['dimensions'] != '' ? $get_cart_data[$i]['products']['dimensions'] : '--');

     $weight = ($get_cart_data[$i]['products']['lbs'] != '' ? $get_cart_data[$i]['products']['lbs'] : '--');

     $area = ($get_cart_data[$i]['products']['sf'] != '' ? $get_cart_data[$i]['products']['sf'] : '--');

     $heat = ($get_cart_data[$i]['products']['heat'] != '' ? $get_cart_data[$i]['products']['heat'] : '--');

     $slab = ($get_cart_data[$i]['products']['slab'] != '' ? $get_cart_data[$i]['products']['slab'] : '--');

     $country_origin = ($get_cart_data[$i]['products']['country_of_origin'] != '' ? $get_cart_data[$i]['products']['country_of_origin'] : '--');

     $mill_name = ($get_cart_data[$i]['products']['mill_name'] != '' ? $get_cart_data[$i]['products']['mill_name'] : '--');

     $cwt = ($get_cart_data[$i]['products']['price_cwt'] != '' ? '$'.number_format($get_cart_data[$i]['products']['price_cwt'],2) : '--');

     $price = ($get_cart_data[$i]['price'] != '' ? '$'.number_format($get_cart_data[$i]['price'],2) : '--'); 

     $grand_total += $get_cart_data[$i]['price'];


      $product_ordered = $dom->createElement('Product');

     $item_no = new DOMAttr('Item-serial-no', $item_ser_no);

    $product_ordered->setAttributeNode($item_no);

     $mtr_tag     = $dom->createElement('MTR', $mtr); 

     $product_ordered->appendChild($mtr_tag); 

     $tag_number_tag   = $dom->createElement('Tag-number', $tag_number); 

     $product_ordered->appendChild($tag_number_tag); 

     $group_name_tag   = $dom->createElement('Group-name', $group_name); 

     $product_ordered->appendChild($group_name_tag); 

     $quantity_tag    = $dom->createElement('Quantity', $quantity); 

     $product_ordered->appendChild($quantity_tag); 

     $grade_tag    = $dom->createElement('Grade', $grade); 

     $product_ordered->appendChild($grade_tag); 

     $thickness_tag    = $dom->createElement('Thickness', $thickness); 

     $product_ordered->appendChild($thickness_tag); 

     $dimensions_tag    = $dom->createElement('Dimensions', $dimension); 

     $product_ordered->appendChild($dimensions_tag); 

     $weight_tag    = $dom->createElement('Weight', $weight); 

     $product_ordered->appendChild($weight_tag); 

     $area_tag    = $dom->createElement('Area', $area); 

     $product_ordered->appendChild($area_tag); 

     $heat_tag    = $dom->createElement('Heat', $heat); 

     $product_ordered->appendChild($heat_tag); 

     $slab_tag    = $dom->createElement('Slab', $slab); 

     $product_ordered->appendChild($slab_tag); 

     $country_origin_tag    = $dom->createElement('Country-origin', $country_origin); 

     $product_ordered->appendChild($country_origin_tag); 

     $mill_name_tag    = $dom->createElement('Mill-name', $mill_name); 

     $product_ordered->appendChild($mill_name_tag); 

     $cwt_tag    = $dom->createElement('Cwt', $cwt); 

     $product_ordered->appendChild($cwt_tag); 

     $price_tag    = $dom->createElement('Price', $price); 

     $product_ordered->appendChild($price_tag); 

     $root->appendChild($product_ordered);

   }

   $total = $dom->createElement('Grand-total');
    $toal_sum     = $dom->createElement('Total', $grand_total);
     $total->appendChild($toal_sum); 
     $root->appendChild($total);

   $dom->appendChild($root); 

   if($dom->save($filePath)){
          $maildata = array();
          $url = Configure::read("SITEURL");
         $project_name = Configure::read('project_name');
         $from_email = Configure::read('FROM_EMAIL');
          $maildata['url'] = $url; 
          $maildata['to_email'] = $from_email;
          $maildata['email'] = $data['email'];
          $maildata['heading'] = $project_name. " New Order Received"; 
            $email = new Email();
            $email->template('allmail')
            ->emailFormat('html')
            ->from([$from_email => $project_name])
            ->to($data['email'])
            ->subject($project_name.': New Order')
            ->viewVars($maildata)            
            ->attachments([
                $filePath => [
                'file' => WWW_ROOT.$filePath                ]                
                ]);
            if($email->send()){
                $result['email_status'] = "success";
            }else{
                $result['email_status'] = "fail";
            }

            $result['xml_status'] = "xml_success";

    

        // echo $status; die;
   }else{
    $result['xml_status'] = "xml_fail";
   }

        return $result; 
        die;




        // $table_obj = TableRegistry::get($tablename);
        // $userDetails = $table_obj->find('all')->where(['md5(md5(id))' => $id])->first();      
        // return $userDetails;
    }
    

    public function send_mail($username, $password) {
        // pr($email_data); die;
         $maildata = array();
          $url = Configure::read("SITEURL");
         $project_name = Configure::read('project_name');
         $from_email = Configure::read('FROM_EMAIL');
          $maildata['url'] = $url; 
          $maildata['to_email'] = $from_email;
          $maildata['email'] = $username;
           $maildata['password'] = $password;
          $maildata['heading'] = $project_name. " Login Credentials"; 
        $email = new Email();
        $mail = $email->template('registration')
        ->emailFormat('html')
        ->to($username)
        ->from([$from_email => $project_name])
        ->subject($project_name.': Login Credentials')
        ->viewVars($maildata)
        ->send();  
    }

    public function send_forgotmail($email_data) {
        // pr($email_data); die;
         $from_email = Configure::read('FROM_EMAIL');
        $project_name = Configure::read('project_name');
        $email = new Email();
        $mail = $email->template('forgot')
        ->emailFormat('html')
        ->to($email_data['email'])
        ->from([$from_email => $project_name])
        ->subject($email_data['subject'])
        ->viewVars($email_data)
        ->send();  
    }    

    
}
