<?php
namespace App\Controller\Component;
use Cake\Core\Configure;

use Cake\Controller\Component;
use Cake\Mailer\Email;
use Cake\Network\Http\Client;
use DOMDocument;
use DOMAttr;
use Cake\ORM\TableRegistry;

// In a controller or table method.

class CommonComponent extends Component
{
	    
    public function getRandomString($count) {
        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $count);
        return $randomString;
    }
    /****
     * Function to return data with respect to md5 format id
     * ****/
    public function generateXml($get_cart_data = null, $filePath = null, $data = null, $optionaldetails = null){
      
        $address = '';
        if(!empty($optionaldetails['address_one'])){
           $address .= $optionaldetails['address_one'];
       }
       if(!empty($optionaldetails['address_two'])){
           $address .= ", ".$optionaldetails['address_two'];
       }

       if(!empty($optionaldetails['payment_method'])){
           if($optionaldetails['payment_method'] == 'terms'){
              $payment_method = 'Terms';
          }else if($optionaldetails['payment_method'] == 'credit_card'){
              $payment_method = 'Credit card';
          }else if($optionaldetails['payment_method'] == 'check'){
              $payment_method = 'Check';
          }else{
              $payment_method = 'Cash';
          } 
      }

      $dom = new DOMDocument();

      $dom->encoding = 'utf-8';

      $dom->xmlVersion = '1.0';

      $dom->formatOutput = true;

      $mtr_url = Configure::read("MTR_URL"); 




      $root = $dom->createElement('OrderDetail'); 

      $xmlsource = $dom->createElement('xmlsource', 'onlineplates');  
      $root->appendChild($xmlsource);

      $userDetails = $dom->createElement('CustomerDetail');
      $name     = $dom->createElement('Name', $data['name']);
      $userDetails->appendChild($name); 

      $company     = $dom->createElement('Company', $data['company']);
      $userDetails->appendChild($company); 

      $email     = $dom->createElement('Email', $data['username']);
      $userDetails->appendChild($email);

      $phone     = $dom->createElement('Phone', $data['phone']);
      $userDetails->appendChild($phone);
      if(!empty($address)){
       $ship_to  = $dom->createElement('ShipTo', $address);
       $userDetails->appendChild($ship_to); 
   }

   if(!empty($optionaldetails['city'])){

     $city  = $dom->createElement('City', $optionaldetails['city']);
     $userDetails->appendChild($city); 
 } 

 if(!empty($optionaldetails['state'])){

  $state  = $dom->createElement('State', $optionaldetails['state']);
  $userDetails->appendChild($state); 

}

if(!empty($optionaldetails['zip'])){ 

   $zip  = $dom->createElement('Zip', $optionaldetails['zip']);
   $userDetails->appendChild($zip); 

}

if(!empty($optionaldetails['po'])){
   $po  = $dom->createElement('PO', $optionaldetails['po']);
   $userDetails->appendChild($po);
}

$payment_method  = $dom->createElement('PaymentMethod', !empty($payment_method)?$payment_method:"");
$userDetails->appendChild($payment_method); 

if(!empty($optionaldetails['comment'])){

   $comment  = $dom->createElement('Comment', $optionaldetails['comment']);
   $userDetails->appendChild($comment); 
} 

if(!empty($optionaldetails['need_by_date'])){
   $need_by_date  = $dom->createElement('NeededByDate', $optionaldetails['need_by_date']);
   $userDetails->appendChild($need_by_date); 
}

     $currentuser     = $dom->createElement('CurrentUser', ($data['existing_user']==1)?"Yes":"No");
     $userDetails->appendChild($currentuser); 

     // if(isset($data['current_user'])){
     //    $current_user     = $dom->createElement('CurentUser', 'Yes');
     // $userDetails->appendChild($current_user); 
     // }

      $root->appendChild($userDetails);

  
    $grand_total = 0;
    $products = $dom->createElement('Products'); 
   for($i=0; $i<count($get_cart_data); $i++){
     
     $item_ser_no  =  $get_cart_data[$i]['products']['item_ser_no'];  

     $mtr  =  ($get_cart_data[$i]['products']['mtr'] != '' && file_exists(WEBROOT_MTR_PATH.$get_cart_data[$i]['products']['mtr']) ? $mtr_url . $get_cart_data[$i]['products']['mtr'] : ""); 

      $tag_number =   ($get_cart_data[$i]['products']['tag_no'] != '' ? $get_cart_data[$i]['products']['tag_no'] : '');

      $group_name =  ($get_cart_data[$i]['products']['group']['name'] != '' ? $get_cart_data[$i]['products']['group']['name'] : '');

     $quantity = $get_cart_data[$i]['quantity'];

     $grade = ($get_cart_data[$i]['products']['grade'] != '' ? $get_cart_data[$i]['products']['grade'] : '');

     $thickness = ($get_cart_data[$i]['products']['thickness'] != '' ? $get_cart_data[$i]['products']['thickness'] : '');

     $dimension = ($get_cart_data[$i]['products']['dimensions'] != '' ? $get_cart_data[$i]['products']['dimensions'] : '');

     $weight = ($get_cart_data[$i]['products']['lbs'] != '' ? $get_cart_data[$i]['products']['lbs'] : '');

     $area = ($get_cart_data[$i]['products']['sf'] != '' ? $get_cart_data[$i]['products']['sf'] : '');

     $heat = ($get_cart_data[$i]['products']['heat'] != '' ? $get_cart_data[$i]['products']['heat'] : '');

     $slab = ($get_cart_data[$i]['products']['slab'] != '' ? $get_cart_data[$i]['products']['slab'] : '');

     $country_origin = ($get_cart_data[$i]['products']['country_of_origin'] != '' ? $get_cart_data[$i]['products']['country_of_origin'] : '');

     $mill_name = ($get_cart_data[$i]['products']['mill_name'] != '' ? $get_cart_data[$i]['products']['mill_name'] : '');

     $cwt = ($get_cart_data[$i]['products']['price_cwt'] != '' ? '$'.number_format($get_cart_data[$i]['products']['price_cwt'],2) : '');

     $price = ($get_cart_data[$i]['price'] != '' ? '$'.number_format($get_cart_data[$i]['price'],2) : ''); 

     $grand_total += $get_cart_data[$i]['price'];


      $product_ordered = $dom->createElement('Product');

    //  $item_no = new DOMAttr('ItemSerialNo', $item_ser_no);

    // $product_ordered->setAttributeNode($item_no);
      $item_no     = $dom->createElement('ItemSerialNo', $item_ser_no); 

     $product_ordered->appendChild($item_no); 

     // $mtr_tag     = $dom->createElement('MTR', $mtr); 

     // $product_ordered->appendChild($mtr_tag); 

     // $tag_number_tag   = $dom->createElement('TagNumber', $tag_number); 

     // $product_ordered->appendChild($tag_number_tag); 

     // $group_name_tag   = $dom->createElement('GroupName', $group_name); 

     // $product_ordered->appendChild($group_name_tag); 

     $quantity_tag    = $dom->createElement('Quantity', $quantity); 

     $product_ordered->appendChild($quantity_tag); 

     // $grade_tag    = $dom->createElement('Grade', $grade); 

     // $product_ordered->appendChild($grade_tag); 

     // $thickness_tag    = $dom->createElement('Thickness', $thickness); 

     // $product_ordered->appendChild($thickness_tag); 

     // $dimensions_tag    = $dom->createElement('Dimensions', $dimension); 

     // $product_ordered->appendChild($dimensions_tag); 

     // $weight_tag    = $dom->createElement('Weight', $weight); 

     // $product_ordered->appendChild($weight_tag); 

     // $area_tag    = $dom->createElement('Area', $area); 

     // $product_ordered->appendChild($area_tag); 

     // $heat_tag    = $dom->createElement('Heat', $heat); 

     // $product_ordered->appendChild($heat_tag); 

     // $slab_tag    = $dom->createElement('Slab', $slab); 

     // $product_ordered->appendChild($slab_tag); 

     // $country_origin_tag    = $dom->createElement('CountryOrigin', $country_origin); 

     // $product_ordered->appendChild($country_origin_tag); 

     // $mill_name_tag    = $dom->createElement('MillName', $mill_name); 

     // $product_ordered->appendChild($mill_name_tag); 

     $cwt_tag    = $dom->createElement('Cwt', $cwt); 

     $product_ordered->appendChild($cwt_tag); 

     // $price_tag    = $dom->createElement('Price', $price); 

     // $product_ordered->appendChild($price_tag); 

     // $root->appendChild($product_ordered);
     $products->appendChild($product_ordered);

   }
   $root->appendChild($products);

   $total = $dom->createElement('CartTotal', '$'.number_format($grand_total,2));
    // $toal_sum     = $dom->createElement('Total', $grand_total);
    //  $total->appendChild($toal_sum); 
     $root->appendChild($total);

   $dom->appendChild($root); 

   if($dom->save($filePath)){
    chmod($filePath, 0777);
          $maildata = array();
          $url = Configure::read("SITEURL");
         $project_name = Configure::read('project_name');
         $from_email = Configure::read('FROM_EMAIL');
         $to_email = Configure::read('TO_EMAIL');
          $maildata['url'] = $url; 
          $maildata['project_name'] = $project_name;
          $maildata['to_email'] = $from_email;
          $maildata['email'] = '<strong>'.$data['name'].'<strong>.';
          $maildata['heading'] = $project_name. " Request for order";
          $maildata['xml_filepath'] = $filePath;
          $maildata['userdetail'] = $data; 
          $maildata['need_by_date'] = $optionaldetails; 
          $maildata['get_cart_data'] = $get_cart_data; 
            $email = new Email();
            $email->template('allmail')
            ->emailFormat('html')
            ->from([$data['username'] => $project_name])
            ->to($to_email)
            ->subject("SWIFT: request for order from " .$data['company']. " Corporation")
            ->viewVars($maildata)            
            ->attachments([
                $filePath => [
                'file' => WWW_ROOT.$filePath                ]                
                ]);
            if($email->send()){
                $result['email_status'] = "success";
            }else{
                $result['email_status'] = "fail";
            }

            $result['xml_status'] = "xml_success";
                

        // echo $status; die;
   }else{
    $result['xml_status'] = "xml_fail";
   }

        return $result; 
        die;




        // $table_obj = TableRegistry::get($tablename);
        // $userDetails = $table_obj->find('all')->where(['md5(md5(id))' => $id])->first();      
        // return $userDetails;
    }
    

    public function send_mail($username, $password, $name) {
        // pr($email_data); die;
         $maildata = array();
          $url = Configure::read("SITEURL");
         $project_name = Configure::read('project_name');
         $from_email = Configure::read('FROM_EMAIL');
          $maildata['url'] = $url; 
          $maildata['name'] = $name;
          $maildata['to_email'] = $from_email;
          $maildata['email'] = $username;
           $maildata['password'] = $password;
          $maildata['heading'] = $project_name. " Login Credentials"; 
        $email = new Email();
        $mail = $email->template('registration')
        ->emailFormat('html')
        ->to($username)
        ->from([$from_email => $project_name])
        ->subject($project_name.': Login Credentials')
        ->viewVars($maildata)
        ->send();  
    }

    public function send_forgotmail($email_data) {
        // pr($email_data); die;
         $from_email = Configure::read('FROM_EMAIL');
        $project_name = Configure::read('project_name');
        $email = new Email();
        $mail = $email->template('forgot')
        ->emailFormat('html')
        ->to($email_data['email'])
        ->from([$from_email => $project_name])
        ->subject($email_data['subject'])
        ->viewVars($email_data)
        ->send();  
    }

    public function apilogs($action = null, $item_ser_no = null){
        $apilogs_Table = TableRegistry::get('Apilogs');
          // $this->loadModel("Apilogs");
        $api_logs = $apilogs_Table->newEntity();
        $api_logs->product_item_ser_no = $item_ser_no;
        $api_logs->ip_address = $this->request->clientIp();
        $api_logs->action_performed = $action; 
        $api_logs->added_on = date("Y-m-d H:i:s");
        $apilogs_Table->save($api_logs);

    }    

    
}
