<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
/**
 * Index Controller
 *
 *
 * @method \App\Model\Entity\Index[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ApiController extends AppController
{

    public function initialize(){
        parent::initialize();
        $this->loadModel('Products');
        $this->loadModel('Groups');        
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $response = [];
        $response_type = RESPONSE_TYPE;
        $msg = __('METHOD_INVALID' ,'Method');
        $status = 'error';
        $data = null;
        
        if ($this->request->is(['POST','PUT','PATCH','GET'])) {

            $postdata = $this->request->data;
            if($this->request->is('get')){
                $postdata = $this->request->query();
            }
            
            $method = isset($postdata['method']) ? trim($postdata['method']) : METHOD;
            $token = isset($postdata['tokenID']) ? trim($postdata['tokenID']) : tokenID;
            if(isset($postdata['responseType'])){
                $response_type = trim($postdata['responseType']);
            }
            // check for token 
            $this->isValidToken($token,$response_type);
            $acceptedResponseTypes = ['json','xml'];
            $msg = 'Please provide method to operate.';
            if(isset($method) && (!empty($method))){
                $acceptedActions = ['item_update','hide_all','mtr_update','mtr_delete'];
                $msg = __('METHOD_INVALID' ,'Method');
                
                if(in_array($method, $acceptedActions)){
                    $msg = __('METHOD_INVALID','Response Type');
                    if(in_array($response_type, $acceptedResponseTypes)){


                        if($method == 'item_update'){
                            $return = $this->itemUpdate();
                            $status = $return['status'];
                            $msg = $return['message'];
                            $data = $return['data'];
                        }
                        if($method == 'hide_all'){
                            $return = $this->hideAll();
                            $status = $return['status'];
                            $msg = $return['message'];
                            $data = $return['data'];
                        }
                        if($method == 'mtr_update'){
                            $return = $this->mtrUpdate();
                            $status = $return['status'];
                            $msg = $return['message'];
                            $data = $return['data'];
                        }
                        if($method == 'mtr_delete'){
                            $return = $this->mtrDelete();
                            $status = $return['status'];
                            $msg = $return['message'];
                            $data = $return['data'];
                        }
                    }else{
                        $response_type = 'json';
                    }
                }
            }else{
                $response_type = 'json';
            }
        }
        $response = [
            'status' => $status,
            'message' => $msg,
            'data'  => $data
        ];
        $this->__sendResponse($response,$response_type); die;
    }

    /**
     * View method
     *
     * @param string|null $id Index id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $response = [];
        if ($this->request->is(['POST','PUT','PATCH','GET'])) {
            if(isset($id) && (!empty($id)) && ($this->Products->exists(['id' => $id]))){
                $product = $this->Products->get($id, [
                    'fields' => [
                       'id','group_name' => 'Groups.name','tag_no','mtr','grade','thickness','dimensions','lbs','heat','slab','sf',
                       'country_of_origin' ,'mill_name','price_cwt','total_price','visible'
                   ],
                   'contain' => ['Groups']
               ])->toArray();
                $response = [
                    'status' => 'success',
                    'message' => 'Product found successfully.',
                    'data'  => $product
                ];
            }else{
                $response = [
                    'status' => 'error',
                    'message' => 'Invalid data supplied.',
                    'data'  => null
                ];
            }
        }else{
            $response = [
                'status' => 'error',
                'message' => 'Method not allowed.',
                'data'  => null
            ];
        }
        
        return($response);
    }
    
    

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    
    public function itemUpdate(){
        $status = 'error';
        $add = 0;
        $msg = __('METHOD_INVALID' ,'Method');
        $return_data = null;
        if ($this->request->is(['POST','PUT','PATCH','GET'])) {
            // check post count = 16
            $msg = __('METHOD_INVALID' ,'post count');
            $postdata = $this->request->data;
            if($this->request->is('get')){
                $postdata = $this->request->query();
            }
            if(count($postdata) >= 7){
                $requiredParams = json_decode(ADD_PRO_REQUIRED);
                $error = '';
                $error = $this->checkApiErrors($postdata, $requiredParams);
                $msg = $error;

                if(empty($error)){
                    // check for group
                    $data = $postdata;
                    $group_name = $data['group_name'];
                    $group_data = $this->checkGroup($group_name);
                    $msg = $group_data['message'];

                    if($group_data['status'] == 'success'){
                        if($this->Products->exists(['item_ser_no' => $data['item_ser_no']])){
                            $add = 0;
                            $product = $this->Products->find()
                            ->where(['item_ser_no' => $data['item_ser_no']])
                            ->first();
                        }else{
                            $add = 1;
                            $product = $this->Products->newEntity();
                        }
                        $postarray = [
                            'group_id'      => $group_data['data'],
                            'item_ser_no'   => isset($data['item_ser_no']) ? $data['item_ser_no'] : '',
                            'tag_no'        => isset($data['tag_no']) ? $data['tag_no'] : '',
                            'grade'         => isset($data['grade']) ? $data['grade'] : '',
                            'thickness'     => isset($data['thickness']) ? $data['thickness'] : '',
                            'dimensions'    => isset($data['dimensions']) ? $data['dimensions'] : '',
                            'lbs'           => isset($data['lbs']) ? $data['lbs'] : '',
                            'heat'          => isset($data['heat']) ? $data['heat'] : '',
                            'slab'          => isset($data['slab']) ? $data['slab'] : '',
                            'country_of_origin'    => isset($data['country_of_origin']) ? $data['country_of_origin'] : '',
                            'mill_name'     => isset($data['mill_name']) ? $data['mill_name'] : '',
                            'price_cwt'     => isset($data['price_cwt']) ? $data['price_cwt'] : '',
                            'total_price'   => isset($data['total_price']) ? $data['total_price'] : '',
                            'visible'       => isset($data['visible']) ? $data['visible'] : '',
                            'subprime'       => isset($data['subprime']) ? $data['subprime'] : '0',
                            'sf'            => isset($data['sf']) ? $data['sf'] : '',
                            'created'       => date("Y-m-d H:i:s"),
                        ];
                        $product = $this->Products->patchEntity($product, $postarray);
                        $errors = $this->getErrors($product);
                        $msg = $errors;
                        if(empty($product->errors())){
                            if($add == 1){
                                $msg = __('ERROR' ,'add','product');
                            }else{
                                $msg = __('ERROR' ,'updated','product');
                            }
                            
                            if ($pro = $this->Products->save($product)) {
                                if($add == 1){
                                    $action = 'item_update';
                                    $msg = __('SUCCESS' ,'Product','added');
                                }else{
                                    $action = 'item_update';
                                    $msg = __('SUCCESS' ,'Product','updated');
                                }
                                $this->Common->apilogs($action,$pro->item_ser_no);                               
                                $status = 'success';
                                $return_data = ['item_ser_no' => $pro->item_ser_no];
                            }
                        }
                    }
                }
            }
        }
        
        return $response = [
            'status' => $status,
            'message' => $msg,
            'data'  => isset($return_data) ? $return_data : null
        ];
    }
    
    
    /*********checkGroup***********/
    
    public function checkGroup($group_name){
        if(!empty($group_name)){
            if($this->Groups->exists(['name' => $group_name])){
                $data = $this->Groups->find()
                ->select(['id'])
                ->where(['name' => $group_name])
                ->hydrate(false)
                ->first(); 
                $respose = [
                    'status'  => 'success',
                    'message' => 'Group found',
                    'data'    => $data['id']
                ];
            }else{
                // create group
                $postarray = [];
                $groupEntity = $this->Groups->newEntity();
                $postarray = ['name' => $group_name, 'status' => 1];
                $group = $this->Groups->patchEntity($groupEntity, $postarray);
                $errors = $this->getErrors($group);
                $respose = [
                    'status'  => 'error',
                    'message' => $errors
                ];
                if(empty($group->errors())){
                    $group = $this->Groups->save($group);
                    if($group){
                        $respose = [
                            'status'  => 'success',
                            'message' => 'Group created',
                            'data'    => $group->id
                        ];
                    }else{
                        $respose = [
                            'status'  => 'error',
                            'message' => 'Unable to create Group'
                        ];
                    }
                }
            }
        }else{
            $respose = [
                'status' => 'error',
                'message' => 'Group Name empty'
            ];
        }
        return $respose;
    }

    

    /**
     * Delete method
     *
     * @param string|null $id Index id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if(isset($id) && (!empty($id)) && ($this->Products->exists(['id' => $id]))){
            $this->request->allowMethod(['post', 'delete']);
            $product = $this->Products->get($id);
            if ($this->Products->delete($product)) {
                $response = [
                    'status' => 'success',
                    'message' => __('SUCCESS','Product','deleted'),
                    'data'  => null
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'message' => 'Error in deleting product. Try again later',
                    'data'  => null
                ];
            }
        }else{
            $response = [
                'status' => 'error',
                'message' => 'Invalid data supplied.',
                'data'  => null
            ];
        }
        return ($response);
    }
    
    /**function name : hideAll
     * purpose : will hide all products in database. i.e. will set visiblity to false
     * return : either success or error****/
    
    public function hideAll(){
        $response = [
            'status' => 'error',
            'message' => 'Invalid method.',
            'data'  => null
        ];
        if($this->request->is(['POST','PUT','PATCH','GET'])){
            if($this->Products->updateAll(
                ['visible' => 0], // fields
                ['visible' => 1])) { // conditions
                $action = 'hide_all';
                $this->Common->apilogs($action);
               $response = [
                'status' => 'success',
                'message' => 'All products visiblity is set to hidden.',
                'data'  => null
            ];
        }else{
            if($this->Products->exists(['visible' => 0])){
                $response = [
                    'status' => 'error',
                    'message' => 'Products visiblity is already set to hidden.',
                    'data'  => null
                ];
            }else{
                $response = [
                    'status' => 'error',
                    'message' => 'Unable to hide visiblity.',
                    'data'  => null
                ];
            }
        }
    }else{
        $response = [
            'status' => 'error',
            'message' => 'Invalid method.',
            'data'  => null
        ];
    }
    return($response);
}

    /**function name : unHideAll
     * purpose : will un-hide all products in database. i.e. will set visiblity to true
     * return : either success or error****/
    
    public function unHideAll(){
        $response = [
            'status' => 'error',
            'message' => 'Invalid method.',
            'data'  => null
        ];
        if($this->request->is(['POST','PUT','PATCH','GET'])){
            if($this->Products->updateAll(
                ['visible' => 1], // fields
                ['visible' => 0])) { // conditions
               $response = [
                'status' => 'success',
                'message' => 'All products are set to visible.',
                'data'  => null
            ];
        }else{
            if($this->Products->exists(['visible' => 1])){
                $response = [
                    'status' => 'error',
                    'message' => 'Products are already visible.',
                    'data'  => null
                ];
            }else{
                $response = [
                    'status' => 'error',
                    'message' => 'Unable to un-hide visiblity.',
                    'data'  => null
                ];
            }
        }
    }else{
        $response = [
            'status' => 'error',
            'message' => 'Invalid method.',
            'data'  => null
        ];
    }
    return($response);
}

public function unhide($item_ser_no){

    $response = [
        'status' => 'error',
        'message' => 'Invalid method.',
        'data'  => null
    ];
    if($this->request->is(['POST','PUT','PATCH','GET'])){
        if(empty($item_ser_no)){
            return $response = [
                'status' => 'error',
                'message' => __('FIELD_EMPTY','Item Serial Number is'),
                'data'  => null
            ];
        }
        $pro = $this->Products->find()
        ->where(['item_ser_no' => $item_ser_no])
        ->first();
        if(empty($pro)){
            return $response = [
                'status' => 'error',
                'message' => 'Product not found. Try another one.',
                'data'  => null
            ];
        }
        $patch_array = ['visible' => 1];
        $product = $this->Products->patchEntity($pro,$patch_array);
            if($this->Products->save($product)) { // conditions
               $response = [
                'status' => 'success',
                'message' => 'Product is set to visible.',
                'data'  => null
            ];
        }else{

            $response = [
                'status' => 'error',
                'message' => 'Unable to un-hide visiblity.',
                'data'  => null
            ];

        }
    }else{
        $response = [
            'status' => 'error',
            'message' => 'Invalid method.',
            'data'  => null
        ];
    }
    return($response);
}


public function mtrUpdate(){
    ini_set('memory_limit', '-1');
    $status = 'error';
    $add = 0;
    $msg = __('METHOD_INVALID' ,'Method');
    $return_data = null;
    
    if ($this->request->is(['POST','PUT','PATCH','GET'])) {
        $postdata = $this->request->data;
        $requiredParams = json_decode(ADD_MTR_REQUIRED);
        $error = '';

        $error = $this->checkApiErrors($postdata, $requiredParams);
        $msg = $error;

        if(empty($error)){

            $data = $postdata; 

            if(base64_encode(base64_decode($data['mtr'], true)) === $data['mtr']){

                $extension = array("jpg", "jpeg", "png", "gif", "pdf");                 
                // Get image extension from base64               
                $encoded_string = $data['mtr'];
                $imgdata = base64_decode($encoded_string);
                $f = finfo_open();

                $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE); 
                $ext = explode("/",$mime_type);
                $ext = end($ext); 
                //////////////////////////////////////////////////////////////

                $item_ser_no = trim($data['item_ser_no']);
                // $mtr_tag_sql_recid = trim($data['mtr_tag_sql_recid']);
                if($this->Products->exists(['item_ser_no' => $item_ser_no, 'visible' => 1, 'sold' => 0])){

                 $product = $this->Products->find()
                 ->select(['item_ser_no','mtr','id'])
                 ->where(['item_ser_no' => $item_ser_no])
                 ->first();
                 $image_path = $product['mtr'];
                 // get image size from base64 
                 $image_size = strlen(base64_decode($data['mtr']));
                 ////////////////////////////////////
             if($image_size > 50000000) { // 50 Mb size
                return $response = [
                    'status' => 'ERROR',
                    'message' => __('ERROR' ,'Upload MTR','Max upload size exceeds. Allowed size: 50Mb'), 
                    'data'  => null
                ];

            }    

            if (in_array($ext, $extension)){

                // $this->loadModel('Mtr');
                // $check_duplicate_mtr = $this->Mtr->find()->select(['item_ser_no','mtr_tag_sql_recid'])->where(['item_ser_no' => $item_ser_no, 'mtr_tag_sql_recid' => $mtr_tag_sql_recid])->count();
                 // if($check_duplicate_mtr == 0){
                        // Save base64 image to folder and dataabase
                // $name = $item_ser_no.rand(0,9999).".".$ext;
                    $name = $item_ser_no."_".rand(0,9999).".".$ext;    
                    $path = WEBROOT_MTR_PATH;
                    $mtr_image = 'data:image/'.$ext.';base64,'.$data['mtr'].'';
                    list($type, $mtr_image) = explode(';', $mtr_image);
                    list(, $mtr_image)      = explode(',', $mtr_image);
                    $mtr_image = base64_decode($mtr_image);
                    if(file_put_contents($path.$name, $mtr_image)){
                    /////////////////////////////////
                        @chmod($path.$name,0777);
                        
                    $patchArray = ['mtr' => $name];
                    $products = $this->Products->patchEntity($product,$patchArray);
                    if($pro = $this->Products->save($products)){ 

                        // $add_mtr = $this->Mtr->newEntity();
                        // $add_mtr->item_ser_no = $product['item_ser_no'];
                        // $add_mtr->mtr_tag_sql_recid = $data['mtr_tag_sql_recid'];
                        // $add_mtr->notes = !empty($data['notes'])?$data['notes']:Null;
                        // $add_mtr->mtr = $name;
                        // if($this->Mtr->save($add_mtr)){
                    // Unlink previous image 
                        if(isset($image_path) && ($image_path != '') && file_exists(WEBROOT_MTR_PATH.$image_path)){
                            @unlink(WEBROOT_MTR_PATH.$image_path);
                        }

                        $action = 'mtr_update';
                        $this->Common->apilogs($action,$item_ser_no);        
                        //////////////////////////////////                                                           
                            $mtr_url = Configure::read("MTR_URL");
                            $msg = __('SUCCESS' ,'MTR','updated');
                            $status = 'success'; 
                            $mtr_url = ['mtr' => $mtr_url . $name];
                        }else{
                            $msg = __('ERROR' ,'MTR','updated'); 
                            $status = 'Error';
                        }
                    }else{
                     $msg = __('ERROR' ,'MTR','updated');  
                     $status = 'Error'; 
                 }
             // }else{
             //    $msg = __('ERROR' ,'Upload MTR',', MTR tag sql recid already exists for the item number given'); 
             //     $status = 'Error';
             // }

         }else{
            // $msg = __('ERROR' ,'Upload MTR','Invalid Extension. Allowed extension: jpg, jpeg, png, gif'); 
            $msg = __('ERROR' ,'Upload MTR','Invalid extension. Allowed extension: jpg, jpeg, png, gif, pdf'); 
            $status = 'Error';
        }     

    }else{
        $msg = __('ERROR' ,'Upload MTR','Invalid item number'); 
        $status = 'Error';
    }
             //if (base64_decode($json, true) == true){
} else {
   $msg = __('ERROR' ,'Upload MTR','Corrupt image'); 
   $status = 'Error';
} 

}            
} 

return $response = [
    'status' => $status,
    'message' => $msg,
    'data'  => !empty($mtr_url)?$mtr_url:null
];
}

    /**function name : mtrDelete
     * purpose : delete mtr in database w.r.t. item serial number and mtr_tag_sql_recid
     * return : either success or error****/
    
    public function mtrDelete(){
        $status = 'error';
        $add = 0;
        $msg = __('METHOD_INVALID' ,'Method');
        $return_data = null;

        if ($this->request->is(['POST','PUT','PATCH','GET'])) {
            $postdata = $this->request->data;
            $requiredParams = json_decode(DELETE_MTR_REQUIRED);
            $error = '';

            $error = $this->checkApiErrors($postdata, $requiredParams);
            $msg = $error;

            if(empty($error)){

                $data = $postdata;
                $path = WEBROOT_MTR_PATH;
                $this->loadModel('Mtr');
                $item_ser_no = trim($data['item_ser_no']);
                // $mtr_tag_sql_recid = trim($data['mtr_tag_sql_recid']);
                if($this->Products->exists(['item_ser_no' => $item_ser_no, 'visible' => 1, 'sold' => 0])){
                    // $check_mtr = $this->Mtr->find()->select(['item_ser_no', 'mtr_tag_sql_recid', 'mtr'])->where(['item_ser_no' => $item_ser_no, 'mtr_tag_sql_recid' => $mtr_tag_sql_recid])->hydrate(false)->first();
                    $product = $this->Products->find()
                 ->select(['item_ser_no','mtr','id'])
                 ->where(['item_ser_no' => $item_ser_no])
                 ->first();
                    if(!empty($product['mtr'])){
                        $image_path = $product['mtr'];

                        $patchArray = ['mtr' => ''];
                    $products = $this->Products->patchEntity($product,$patchArray);
                    if($pro = $this->Products->save($products)){ 

                        // $add_mtr = $this->Mtr->newEntity();
                        // $add_mtr->item_ser_no = $product['item_ser_no'];
                        // $add_mtr->mtr_tag_sql_recid = $data['mtr_tag_sql_recid'];
                        // $add_mtr->notes = !empty($data['notes'])?$data['notes']:Null;
                        // $add_mtr->mtr = $name;
                        // if($this->Mtr->save($add_mtr)){
                    // Unlink previous image 
                        if(isset($image_path) && ($image_path != '') && file_exists(WEBROOT_MTR_PATH.$image_path)){
                            @unlink(WEBROOT_MTR_PATH.$image_path);
                        }  

                        $action = 'mtr_delete';
                        $this->Common->apilogs($action,$item_ser_no);        
                        //////////////////////////////////                                                           
                            $mtr_url = Configure::read("MTR_URL");
                            $msg = __('SUCCESS' ,'MTR','deleted');
                            $status = 'success'; 
                        }else{
                            $msg = __('ERROR' ,'MTR','deleted'); 
                            $status = 'Error';
                        }
                              
                        /////////////////////////////////////////////                         
                   }else{
                    $msg = __('ERROR' ,'Delete MTR','. Currently no MTR with given item number exists'); 
                    $status = 'Error';
                }
            }else{
                $msg = __('ERROR' ,'Delete MTR','Invalid item number'); 
                $status = 'Error';
            }

        }            
    } 

    return $response = [
        'status' => $status,
        'message' => $msg,
        'data'  => null
    ];
}

    /**function name : documentation
     * purpose : this function is just for testing
     * return : either success or error****/
    public function documentation(){
        $cakeDescription = 'Web Services: Guide Book';
        $this->set(compact('cakeDescription'));
    }
}
