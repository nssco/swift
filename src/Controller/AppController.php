<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public $pagelimit = 50;
    public function initialize()
    {
        parent::initialize();
        
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie'); 
        $this->loadComponent('Common'); 
        $SITEURL = Configure::read('SITEURL');
        $project_name = $title = Configure::read('project_name');

        $previous_page = $this->referer();


        if($this->Cookie->read('cart_user_id') != null){
              $cart_user_id = $this->Cookie->read('cart_user_id');
               $this->set(compact('cart_user_id'));
        }

        if($this->request->prefix == 'setting'){
//            $this->autologout();
            $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'userModel' => 'SmtpUsers',
                    'fields' => ['username' => 'username', 'password' => 'password'],
//                    'scope' => [
//                        'status'=> 0
//                    ]
                ],
            ],
            'loginAction' => [
                'controller' => 'users',
                'action' => 'change_password'
            ],
            'loginRedirect' => [
                'controller' => 'users',
                'action' => 'change_password'
            ],
            'logoutRedirect' => [
                'controller' => 'users',
                'action' => 'signin',
            ],
            'storage' => [
                'className' => 'Session',
                'key' => 'Smtp',              
            ],
        ]);
            
        }else{
         $this->loadComponent('Auth', [
                'authenticate' => array(
                    'Form' => array(
                        'fields' => ['username' => 'username', 'password' => 'password']
                    )
                ),
                'loginRedirect' => [
                    'controller' => 'Pages',
                    'action' => 'view_cart'
                ],
                'logoutRedirect' => [
                    'controller' => 'pages',
                    'action' => ''
                    
                ],
                'storage' => [
                    'className' => 'Session',
                    'key' => 'Auth.User',               
                ],
                
            ]);
        }
         $this->loadModel('Carts');
          $this->loadModel('Products');

         if(empty($this->Auth->user('id'))){
         $get_cartitem = $this->Carts->find()->where(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id IS' => null])->count();
     }else{

        $get_duplicatecart_data = $this->Carts->find()->contain(['Products','Products.Groups'])->where(['user_id' => $this->Auth->user("id"),'user_type' => $this->Auth->user("type")])->hydrate(false)->toArray();
    
    $product_ids = array_map(function ($entry) {
          return $entry['product_id'];
        }, $get_duplicatecart_data);
    $count_product_iserted = array_count_values($product_ids);
    if(!empty($count_product_iserted)){
      $conn = ConnectionManager::get('default');
      foreach ($count_product_iserted as $key => $value) {
        if($value > 1){
          $limit = $value -1 ;
          
          $stmt = $conn->execute('DELETE FROM carts where user_id = "'.$this->Auth->user("id").'" AND user_type = "'.$this->Auth->user("type").'" AND product_id = "'.$key.'" LIMIT '.$limit.'');
          // $delete_duplicate_cartItem = $this->Carts->deleteAll(['user_session_id ' => $this->Cookie->read('cart_user_id'),'user_id' => $this->Auth->user("id"),'user_type' => $this->Auth->user("type"),'product_id' => $key],['limit' => $limit]);
        }
        
      }
    }

        $get_cartitem = $this->Carts->find()->where(['user_id' => $this->Auth->user("id"),'user_type' => $this->Auth->user("type")])->count();

     }
    //echo count($get_cartitem);
  

     $this->set('cookie', $this->Cookie->read('cart_user_id'));
         
        $this->set(compact('SITEURL', 'project_name', 'title','get_cartitem','previous_page'));

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
        $this->checkCommonSettings(); // will use this function later
    }
    
    /*****function to check common settings**********/
    public function checkCommonSettings(){
        ///////////////API SECTION////////////////////////////
        
        $method = $this->request->header('method');
        $response_type = 'json';
        $token = '';
        if(null !== ($this->request->header('responseType'))){
            $response_type = $this->request->header('responseType');
        }
        if(null !== ($this->request->header('tokenID'))){
            $token = $this->request->header('tokenID');
        }
        define('METHOD', trim($method));
        define('RESPONSE_TYPE', trim($response_type));
        define('tokenID', trim($token));
    }
    
    /***************return api errors**********/
    public function checkApiErrors($data = null, $required = null){
        $error = '';
        if($data){
            // Search for required keys and notify user
            foreach ($required as $key) {
                if (!array_key_exists(trim($key), $data)){
                    $error[$key] = $key;
                }else{
                    if($data[$key] == ''){
                        $error[$key] = $key;
                    }
                }
            }

            if ($error) {
                $error = implode(', ', $error).' is required.';
            }

            // Search for unwanted keys and remove from post data
            foreach($data as $key => $val) {
                if (array_search(trim($key), $required) === false) {
                    unset($data[$key]);
                }
            }
        }else{
            $error = implode(', ', $required).' is required.';
        }
        return $error;
    }
    
    /**
     * sendJsonResponse
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function __sendResponse($response = null,$type = 'json') {
        if($type == 'xml'){
            header('Content-Type: text/xml;charset=utf-8');
            $xml = '<?xml version="1.0" encoding="utf-8"?>';
            $xml .= '<response>';
                $xml .= '<status>'.$response['status'].'</status>';
                $xml .= '<message>'.$response['message'].'</message>';
                $xml .= '<data>';
                if(isset($response['data']) && (!empty($response['data']))){
                    if(is_array($response['data'])){
                        foreach($response['data'] as $index => $res) {
                            if(is_array($res)) {
                                echo 'sd'; die;
                                foreach($res as $key => $value) {
                                    $xml .= '<'.$key.'>';
                                    if(is_array($value)) {
                                        foreach($value as $tag => $val) {
                                          $xml .= '<'.$tag.'>'.htmlentities($val).'</'.$tag.'>';
                                        }
                                    }
                                    $xml .= '</'.$key.'>';
                                }
                            }else{
                                $xml .= '<'.$index.'>';
                                    $xml .= htmlentities($res);
                                $xml .= '</'.$index.'>';
                            }
                        }
                    }else{
                        $xml .= htmlentities($response['data']);
                    }
                }
                $xml .= '</data>';
            $xml .= '</response>';
            echo $xml;
            die();
        }
        if($type == 'json'){
            header('Content-Type: application/json');
            echo json_encode($response);
            die();
        }
        
    }
    
    /**
     * getErrors
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function getErrors($model = null){
        $error = $model->errors();
        $err = '';
        foreach ($error as $key => $value) {
            foreach ($value as $k => $v) {
                $err .= str_replace('The', str_replace('_',' ',ucwords($key)), $v).'. ';
            }
        }
        return trim($err);
    }
    
    /*****isValidToken*********/
    public function isValidToken($token,$response_type){
        $response = [];
        if(empty($token)){
            $response = [
                'status'    => 'error',
                'message'   =>  __('FIELD_EMPTY','Token'),
                'code'   =>  115,
                'data'      => null
            ];
            $this->__sendResponse($response,$response_type);
        }
        if($token == TOKEN_KEY){
            return true;
        }else{
            $response = [
                'status'    => 'error',
                'message'   =>  __('METHOD_INVALID','token'),
                'code'   =>  116,
                'data'      => null
            ];
            $this->__sendResponse($response,$response_type);
        }
    }

    public function beforeFilter(Event $event)
    {    		
          $this->Auth->allow(['index', 'view', 'addCart', 'viewCart', 'deleteCartitem', 'getProductBygroup', 'refreshCaptcha','forgotPassword', 'resetpassword', 'documentation',"guestLogin","register","checkemailuniqueness","checkeguestemail",'signin','deleteFromCart','phpinfo','thickness','updatecount','filter','documentationMobileApi']);
         
    }
}
