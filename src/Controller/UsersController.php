<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Core\Exception\Exception;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Groups Controller
 *
 *
 * @method \App\Model\Entity\Group[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

 public function login()
 {
   if(!empty($this->Auth->user("id"))){
      return $this->redirect("/pages/view_cart");
      exit();
  }		
  $title = "Login";
    	// $this->autoRender = false; 
  
  if ($this->request->is("post") || $this->request->is("put")) {
    try {
        $url = Configure::read('SITEURL');
        $username = $this->request->data['username'];
        $findUser = $this->Users->find()->where(['username' =>  $username])->first();
        if ($findUser) {
            
            $user = $this->Auth->identify();
            
            if ($user) {
                // echo $this->Cookie->read("cart_user_id"); die;
                $cartsTable = TableRegistry::get("Carts");
                $query = $cartsTable->query();
                $query->update()
                    ->set(['user_id' => $findUser['id']])
                    ->where(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id IS' => null])->execute();

                    $update_type = $this->Users->patchEntity($findUser, ['type' => 0]);
                    $this->Users->save($update_type); 
                    $user['type'] = 0; 
                 
               $this->Auth->setUser($user);
               return $this->redirect($this->Auth->redirectUrl());
           } else {
            throw new Exception("Either your email and/or password is  incorrect.");
        }
    } else {
        throw new Exception("Either your email and/or password is  incorrect.");
    }
} catch (Exception $ex) {
 
    $message = $ex->getMessage();
    $this->Flash->error(__($message));
}
}
$this->set(compact('title'));
}


public function signin()
 {
   if(!empty($this->Auth->user("id"))){
      return $this->redirect("/");
      exit();
  }   
  $title = "Sign In";
      // $this->autoRender = false; 
  
  if ($this->request->is("post") || $this->request->is("put")) {
    try {
        $url = Configure::read('SITEURL');
        $username = $this->request->data['username'];
        $findUser = $this->Users->find()->where(['username' =>  $username])->first();
        if ($findUser) {
            
            $user = $this->Auth->identify();
            
            if ($user) {
                // echo $this->Cookie->read("cart_user_id"); die;
                $cartsTable = TableRegistry::get("Carts");
                $query = $cartsTable->query();
                $query->update()
                    ->set(['user_id' => $findUser['id']])
                    ->where(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id IS' => null])->execute();
                 
                 $update_type = $this->Users->patchEntity($findUser, ['type' => 0]);
                    $this->Users->save($update_type); 
                    $user['type'] = 0; 
                     
               $this->Auth->setUser($user);
               return $this->redirect('/');
           } else {
            throw new Exception("Either your email and/or password is  incorrect.");
        }
    } else {
        throw new Exception("Either your email and/or password is  incorrect.");
    }
} catch (Exception $ex) {
 
    $message = $ex->getMessage();
    $this->Flash->error(__($message));
}
}
$this->set(compact('title'));
}

public function guestLogin(){
    if($this->request->is("ajax")){
        try{
            $this->autoRender = false;
            $this->loadModel('Carts');
            
            if (empty($this->request->data['name'])) {
                throw new Exception("Please enter name.");
            }
            if (empty($this->request->data['company'])) {
                throw new Exception("Please enter company name.");
            }

            if (empty($this->request->data['username'])) {
                throw new Exception("Please enter email.");
            }

            if (empty($this->request->data['phone'])) {
                throw new Exception("Please enter phone number.");
            }

             if (empty($this->request->data['username']) || !filter_var($this->request->data['username'], FILTER_VALIDATE_EMAIL)) {
                    throw new Exception("Please enter valid email");
                }

            
            $users_table = TableRegistry::get("users");
            $check_user_exists = $users_table->find()->select(['username','id'])->where(['username' =>  $this->request->data['username']])->first();
            $user_id = '';
            if(count($check_user_exists) == 0){
                $password = uniqid();
                $create_user = $users_table->newEntity();
                $create_user->username = $this->request->data['username'];
                $create_user->password = $password;
                $create_user->name = $this->request->data['name'];
                $create_user->company = $this->request->data['company'];
                $create_user->phone = $this->request->data['phone'];
                $create_user->existing_user = !empty($this->request->data['existing_user'])?$this->request->data['existing_user']:0;
                $create_user->type = 1;
                $create_user->created = date("Y-m-d H:i:s");
                if($save_user = $users_table->save($create_user)){
                    $user_id =  $save_user->id;
                    $get_user = $users_table->find()->where(['id' => $user_id])->first();
                    $user = $this->Auth->setUser($get_user->toArray());
                    $cartsTable = TableRegistry::get("Carts");
                     $check_cart = $cartsTable->find()->where(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id IS' => null])->count();
                   if($check_cart > 0){  
                    $query = $cartsTable->query();
                    $query->update()
                        ->set(['user_id' => $get_user['id'],'user_type' => $get_user['type']])
                        ->where(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id IS' => null])->execute();
                    }
                    
                    // $this->Common->send_mail($this->request->data['username'],$password, $this->request->data['name']);  
                    
                    $result['status'] = "success";
                    $result["msg"] = "You are Successfully registered as guest. please wait while we redirect you to the cart page." ;
                    echo json_encode($result);
                    exit();   
                    
                }else{
                    throw new Exception("Something went wrong. Please try again later.");
                }
            }
            
            else{
              $update_user = $users_table->get($check_user_exists['id']);
                $update_user->name = $this->request->data['name'];
                $update_user->company = $this->request->data['company'];
                $update_user->phone = $this->request->data['phone'];
                $update_user->existing_user = !empty($this->request->data['existing_user'])?$this->request->data['existing_user']:0;
                $update_user->type = 1;
                if($save_user = $users_table->save($update_user)){
                $get_user = $users_table->find()->where(['id' => $check_user_exists['id']])->first();
                    $user = $this->Auth->setUser($get_user->toArray());
                    $cartsTable = TableRegistry::get("Carts");
                     $check_cart = $cartsTable->find()->where(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id IS' => null])->count();
                   if($check_cart > 0){  
                    $query = $cartsTable->query();
                    $query->update()
                        ->set(['user_id' => $get_user['id'],'user_type' => $get_user['type']])
                        ->where(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id IS' => null])->execute();
                    }
                    
                    // $this->Common->send_mail($this->request->data['username'],$password, $this->request->data['name']);  
                    
                    $result['status'] = "success";
                    $result["msg"] = "You are Successfully registered as guest. please wait while we redirect you to the cart page." ;
                    echo json_encode($result);
                    exit(); 
                    } else{
                    throw new Exception("Something went wrong. Please try again later.");
                } 
            }

        }catch (Exception $ex) {
            $result['status'] = "error";
            $result["msg"] = $ex->getMessage();
            echo json_encode($result);
        }
        exit();
    }
}
/*
     * function to check email uniqueness when register user
     */

    public function checkemailuniqueness() {
        $this->autoRender = false;
        if (!empty($this->request->query['username'])) {
            $users_table = TableRegistry::get('Users');
            $query = $users_table->find()->where(['username' => $this->request->query['username']])->count();
            if ($query > 0) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }

    /*
     * function to check email uniqueness when register guest
     */

    public function checkeguestemail() {
        $this->autoRender = false;
        if (!empty($this->request->query['email'])) {
            $users_table = TableRegistry::get('Users');
            $query = $users_table->find()->where(['username' => $this->request->query['email']])->count();
            if ($query > 0) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }

    /**
     * Function to check email uniqueness on edit user
     */
    public function checkemailuniquenessOnedit() {
        $this->autoRender = false;
        // pr($this->request->query['username']); die;
        if (!empty($this->request->query['username'])) {
            $users_table = TableRegistry::get('Users');
            $query = $users_table->find()->where(['username' => $this->request->query['username'], 'id NOT IN' => $this->Auth->User('id')])->count();
            //echo $query; die;
            if ($query > 0) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }
public function register()
    {
      if(!empty($this->Auth->user("id"))){
      return $this->redirect("/pages/view_cart");
      exit();
  }   
      $title = "Register";
      
        if ($this->request->is("ajax")) {
            try {
              if (empty($this->request->data['name'])) {
                throw new Exception("Please enter name.");
              }
              if (empty($this->request->data['company'])) {
                throw new Exception("Please enter company name.");
              }

              if (empty($this->request->data['username'])) {
                throw new Exception("Please enter email.");
              }

              if (empty($this->request->data['phone'])) {
                throw new Exception("Please enter phone number.");
              }
                if (empty($this->request->data['username']) || !filter_var($this->request->data['username'], FILTER_VALIDATE_EMAIL)) {
                    throw new Exception("Please enter valid email");
                }
                if (empty($this->request->data['password'])) {
                    throw new Exception("Please enter password");
                }

                if ($this->request->data['cpassword'] != $this->request->data['password']) {
                    throw new Exception("Password do not match");
                }
                $store_data = $this->Users->newEntity($this->request->data);
                //print_r($store_data);die();
                if ($save_user = $this->Users->save($store_data)) {
                  $user_id = $save_user->id;
                  $get_user = $this->Users->find()->where(['id' => $user_id])->first();
                    $user = $this->Auth->setUser($get_user->toArray());
                     $cartsTable = TableRegistry::get("Carts");
                     $check_cart = $cartsTable->find()->where(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id IS' => null])->count();
                   if($check_cart > 0){  
                $query = $cartsTable->query();
                $query->update()
                    ->set(['user_id' => $get_user['id']])
                    ->where(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id IS' => null])->execute();
                    }
                    $result['status'] = "success";
                    echo json_encode($result);
                } else {
                    throw new Exception("Something went wrong. Please try again later");
                }
            } catch (Exception $ex) {
                $result['status'] = "error";
                $result["msg"] = $ex->getMessage();
                echo json_encode($result);
            }
            exit();
        }
        $this->set(compact('title'));
    }

public function forgotPassword() {
    $title = 'Forgot Password';

    $this->set(compact('title'));
        // $this->autoRender = false;
    if($this->request->is("post")){
        try {
           
            if (empty($this->request->data['username'])) {
                throw new Exception("Please enter your email Id.");             
            }
            
            
            
            if (!empty($this->request->data['username'])) {
                $check_email = $this->Users->find()->where(['username' => $this->request->data['username']])->first();
                

                if (!empty($check_email)) {
                    $activation_key = md5(uniqid());
                    $activationlink = Configure::read('SITEURL') . 'users/reset-password/' . $activation_key;
                    $toEmail = $check_email->username;
//                    
                    $FROM_EMAIL = Configure::read('FROM_EMAIL');

                    $project_name = Configure::read('project_name');
//echo $project_name; die;
                    $url = Configure::read('SITEURL');
                    $mail_content="";
                    $maildata = array();

                    

                    $mail_content .= '<p>We got a request to change the password for the account with the username <strong style="font-weight: bold;">'.$check_email["username"].'</strong>.</P>
                    
                    <p>If you don&#39;t want to reset your password, you can ignore this email.</P>
                    
                    <p>If you request this change, Please click the link below to reset the password or copy and paste the link on the browser <b>'.$activationlink.'</b></P> 

                    <p>Many thanks, <br/> NSSCO Team</p>           
                    
                    </td>                 
                    </tr>              
                    <tr>           
                    
                    <td align="center">   <a href='.$activationlink.' style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Reset Your Password </a> </td>                 
                    
                    </tr>';

                    
                    $maildata['email'] = $this->request->data['username'];
                    $maildata['heading'] = "Forget your password? Let's get you a new one.";
                    $maildata['url'] = $url;
                    $maildata['data'] = $mail_content;
                    $maildata['subject'] = $project_name.': Reset Password';
                    
                    
                    
                    $user = $this->Users->newEntity();
// $update_password = $this->Users->updateAll(array("password" => $password), array("username" => $this->request->data['username']));
                    $user = $this->Users->patchEntity($check_email, ['reset_activation_key' => $activation_key]);
                    if($this->Users->save($user)){
                       $this->Common->send_forgotmail($maildata);   
                       $result['status'] = "success";
                       $result['response'] = 'An email has been sent to ' . $check_email["username"] . ', so you can reset your password';
                       echo json_encode($result);
                       exit();
                   } else {
                    throw new Exception("Something went wrong. Please try again later.");
                }
            } else {
                throw new Exception("Email does not exists. Please enter a valid email address");
            }
        }
    } catch (Exception $e) {
        $result['status'] = "error";
        $result['response'] = $e->getMessage();
        echo json_encode($result);
        exit;
    }
}

}

public function resetpassword($key = null) {
   if(!empty($this->Auth->user("id"))){
      return $this->redirect("/");
      exit();
  }	
  $project_name = Configure::read('project_name');
  $title = 'Reset Password';

  $this->set(compact('title'));
  $user = $this->Users->find('all', array(
    'conditions' => array(
        'Users.reset_activation_key' => $key,
    ),
))->first();

  if ($this->request->is("post")) {
    try {
      if (empty($user)) {
        throw new Exception("It seems that your token has been expired");
      }
      if (empty($this->request->data['password'])) {
        throw new Exception("Please enter password");
      }
      if (empty($this->request->data['password_confirm'])) {
        throw new Exception("Please confirm your password");
      }

      if ($this->request->data['password_confirm'] != $this->request->data['password']) {
        throw new Exception("Password does not match");
      }
      $update_password = $this->Users->patchEntity($user, ['password' => $this->request->data['password'], 'reset_activation_key' => ""]);
      if ($this->Users->save($update_password)) {
        $message = "Password updated successfully.";
        $this->Flash->success(__($message));
        $this->redirect("/users/reset-password");
      }
    } catch (Exception $ex) {
      $message = $ex->getMessage();
      $this->Flash->error(__($message));
    }
  }
}


public function profile()
    {
      if(empty($this->Auth->user("id"))){
      return $this->redirect("/");
      exit();
  }   
  $title = "Profile";

  $get_user = $this->Users->get($this->Auth->user('id'));
      
        if ($this->request->is("post") || $this->request->is("put")) {
            try {
                // pr($this->request->data); die;
              if (empty($this->request->data['name'])) {
                throw new Exception("Please enter name.");
              }
              if (empty($this->request->data['company'])) {
                throw new Exception("Please enter company name.");
              }

              // if (empty($this->request->data['username'])) {
              //   throw new Exception("Please enter email.");
              // }

              if (empty($this->request->data['phone'])) {
                throw new Exception("Please enter phone number.");
              }
                // if (empty($this->request->data['username']) || !filter_var($this->request->data['username'], FILTER_VALIDATE_EMAIL)) {
                //     throw new Exception("Please enter valid email");
                // }
                
                $this->request->data['existing_user'] = !empty($this->request->data['existing_user'])?$this->request->data['existing_user']:0;
                $store_data = $this->Users->patchEntity($get_user,$this->request->data);
                if ($save_user = $this->Users->save($store_data)) {
                  
                  $get_user = $this->Users->find()->where(['id' => $this->Auth->user('id')])->first();
                    $user = $this->Auth->setUser($get_user->toArray());
                    $this->Flash->success("Profile updated successfully");
                   return $this->redirect("/users/profile");
                } else {
                    throw new Exception("Something went wrong. Please try again later");
                }
            } catch (Exception $ex) {
                
                $message = $ex->getMessage();
                $this->Flash->error($message);
            }
            
        }
        $this->set(compact('title','get_user'));
    }

       /*
   Function to change password
    */
    public function changePassword()
    {
         if(empty($this->Auth->user("id"))){
      return $this->redirect("/");
      exit();
  }   

  $title = "Update Password";
        if($this->request->is('post')){

            $userObj = TableRegistry::get('Users');
            $userData =  $userObj->get($this->Auth->user('id'));
            
            $user =  $userObj->patchEntity($userData,$this->request->data,['validate' => 'password']);
            $error = $user->errors();
            if(empty($error)){
                
                $this->Flash->success('Password updated successfully');
                $userObj->save($user);
               return $this->redirect('/users/change_password');
            }else{
                foreach ($error as $key => $value) {
                    foreach ($value as $val => $error_msg) {
                        
                    }
                }
                $this->Flash->error($error_msg);
            } 

        }      

        $this->set(compact('title'));
    }

    public function orderRequest()
    {
        $title = "Order request";
        $productsModel = $this->loadModel("Purchases");
        $list_user_order = $this->Purchases->find()->contain(['Customers' =>function($q){return $q->select(['item_count' => $q->func()->count('Purchases.purchase_date'),'Purchases.purchase_date','Customers.po','Customers.comment','Customers.need_by_date','Customers.created'])->order(['Purchases.purchase_date' => 'DESC']);},
            ])
    ->where(["Purchases.user_id" =>  $this->Auth->user("id"),"Purchases.user_type" => $this->Auth->user("type")])->order(['Purchases.purchase_date desc'])->group(['Purchases.purchase_date'])->hydrate(false)->toArray();

      
        $this->set(compact('title','list_user_order'));

    }



     public function myOrders($date = null)
    { 
        
        $title = "Order request";
        $productsModel = $this->loadModel("Purchases");
       

         $list_user_order = $this->Purchases->find()->contain(['Products'])->where(["Purchases.user_id" =>  $this->Auth->user("id"),"Purchases.user_type" => $this->Auth->user("type"),'md5(purchase_date)' => $date])->hydrate(false)->toArray();
         
        $this->set(compact('title','list_user_order'));

    }

public function logout() {       
    $u_id = $this->request->session()->read('Auth.User.id');
    $session = $this->request->session()->delete($u_id);
     $active_group_table = TableRegistry::get('active_group');
    $active_group_id = $active_group_table->get(1); 
    $active_group_id->active_group = "";
    $active_group_table->save($active_group_id);
    return $this->redirect($this->Auth->logout());
}
}
 