<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Controller\Component\CookieComponent;
use Cake\Core\Exception\Exception;
use Cake\ORM\TableRegistry;
use DOMDocument;
use DOMAttr;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{


    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display(...$path)
    {
      $count = count($path);
      if (!$count) {
        return $this->redirect('/');
      }
      if (in_array('..', $path, true) || in_array('.', $path, true)) {
        throw new ForbiddenException();
      }
      $page = $subpage = null;

      if (!empty($path[0])) {
        $page = $path[0];
      }
      if (!empty($path[1])) {
        $subpage = $path[1];
      }
      $this->set(compact('page', 'subpage'));

      try {
        $this->render(implode('/', $path));
      } catch (MissingTemplateException $exception) {
        if (Configure::read('debug')) {
          throw $exception;
        }
        throw new NotFoundException();
      }
    }
    
    /******function name : index
     * purpose: listing of products added*****/
    
    public function index(){
      $this->loadModel('Products');
      $this->loadModel('Carts');
      $this->loadModel('Groups');
      $this->loadModel('Mtr');
      $title = "Product Listing";
      $thick='';
      $paginationCountChange = $this->pagelimit;
      if ($this->request->query('paginationCountChange')) {
            $paginationCountChange = $this->request->query('paginationCountChange');
        }
        
        $options=array();
        if($this->request->query('group_id')){ 
          $group_id = $this->request->query('group_id');
           $group_arr=array_filter(explode(',',$group_id));
          $explode = explode("?", $group_id);
            if(!empty($explode[0]) && !empty($group_arr)){
              $group_id = $explode[0];
		if($this->request->query('thickness') && $this->request->query('thickness')!=0){
		$thick=$this->request->query('thickness');
		$options = array(['Products.group_id in' => array_filter(explode(',',$group_id)),'Products.visible' => 1,'Products.sold' => 0,'Products.thickness in'=> explode(',', $thick)]);
	         }
		else
		{
               $options = array(['Products.group_id in' => array_filter(explode(',',$group_id)),'Products.visible' => 1,'Products.sold' => 0]);
		}
            }else if(!empty($explode[0]) && $explode[0]==0){
              $group_id = 0; 
		if($this->request->query('thickness') && $this->request->query('thickness')!=0){
		$thick=$this->request->query('thickness');
 		$options = array(['Products.visible' => 1,'Products.sold' => 0,'Products.thickness in'=> explode(',', $thick)]);

		}else {
              $options = array(['Products.visible' => 1,'Products.sold' => 0]);
		}
            }         
        }
        else{
          if($this->request->query('group_id')!=0)
          $group_id = $this->request->session()->read('active_group');
          else
           $group_id = 0;
	       if($this->request->query('thickness') && $this->request->query('thickness')!=0){
		     $thick=$this->request->query('thickness');
 		     $options = array(['Products.visible' => 1,'Products.sold' => 0,'Products.thickness in'=> explode(',', $thick)]);
		      }else {     
          $options = array(['Products.visible' => 1,'Products.sold' => 0]);
		      }
        }
	    
        /*code added for removing 0 thickness*/
        if(!empty($thick))
        {
        $tcount=array_values(array_filter(explode(',', $thick)));
        if(count($tcount)>0)
        {
          $garray=array_values(array_filter(explode(',', $group_id)));
          //print_r($garray);
          for($g=0;$g<count($garray);$g++)
          {
            $res=$this->Products->find()->where(['Products.visible' => 1,'Products.group_id'=>$garray[$g],'Products.thickness in'=>$tcount])->count();
            if(!$res)
            {
              $garray[$g]=0;
              //$garray=array_filter($garray);
              if(!empty(array_filter($garray)))
              {
              $options = array(['Products.group_id in' => array_filter($garray),'Products.visible' => 1,'Products.sold' => 0,'Products.thickness in'=>$tcount]);
            }
            }
          }
          if(empty(array_filter($garray)))
          {
            //$sel=1;
            $options = array(['Products.visible' => 1,'Products.sold' => 0,'Products.thickness in'=>$tcount]);
          }
          }

        }
        
        /* end code */
        $this->paginate = [
            'limit' => $paginationCountChange,
            'conditions' => $options,
            'order' => ['CAST(Products.grade AS UNSIGNED),Products.grade asc','CAST(Products.thickness AS DECIMAL(10,5)),Products.thickness asc','Products.lbs desc'],
            'contain' => ['Groups'],            
        ];     
        $products = $this->paginate($this->Products)->toArray();
        $products_count = $this->Products->find()->contain(['Groups'])->where($options)->count();
        //debug($products_count);
         $thickness = $this->Products->find()->select(['Products.id','Products.group_id','Products.thickness','Products.grade'])->distinct(['Products.thickness'])->where($options)->order('Products.thickness')->toArray();
      $groups = $this->Groups->find()->select(['id', 'name'])->where(['status' => 1 ])->order('name')->hydrate(false)->toArray();

      $get_mtr = $this->Mtr->find()->select(['Mtr.item_ser_no','Mtr.mtr_tag_sql_recid','Mtr.mtr','Mtr.notes'])->hydrate(false)->toArray();

      // pr($products);
      $product_ids = array_map(function ($entry) {
          return $entry['id'];
        }, $products);
     
      $this->loadModel('Carts');
      if(!empty($product_ids)){
         if(empty($this->Auth->user('id'))){
         $count_cartitem = $this->Carts->find()->where(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id IS' => null, 'product_id IN' => $product_ids])->count();
     }else{
        $count_cartitem = $this->Carts->find()->where(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id' => $this->Auth->user("id"),'user_type' => $this->Auth->user("type"),'product_id IN' => $product_ids])->count();
     }
   }
      
    
      $get_active_group_id = $group_id;
      $get_active_thick_id = $thick;
      $this->set(compact('products','offset','get_mtr','groups','title','paginationCountChange','products_count','get_active_group_id','count_cartitem','thickness','get_active_thick_id','group_id'));
     
      if ($this->request->is('ajax')) { 

        // pr($this->request);
            $this->viewBuilder()->layout(false);
           return $this->render('/Element/pages/index');
        }

      if($this->Cookie->read('cart_user_id')===null){        
         // $this->Cookie->write("cart_user_id",md5(uniqid()), FALSE, "1 week");        
        $this->Cookie->write("cart_user_id",md5(uniqid()), FALSE, "1 year");        
      }


    }

	 public function thickness(){
      $this->loadModel('Products');
      $this->loadModel('Carts');
      $this->loadModel('Groups');
      $this->loadModel('Mtr');
      $title = "Product Listing";
      $sel=0;
      $thick=$this->request->query('thickness');
      if ($this->request->query('paginationCountChange')) {
            $paginationCountChange = $this->request->query('paginationCountChange');
        }
        $options=array();
        if($this->request->query('group_id')){  
          $group_id = $this->request->query('group_id');
           $group_arr=array_filter(explode(',',$group_id));
          //$sel=$group_id;
          $explode = explode("?", $group_id);
            if(!empty($explode[0]) && !empty($group_arr)){
              $group_id = $explode[0];
               $options = array(['Products.group_id in' => array_filter(explode(',', $group_id)),'Products.visible' => 1,'Products.sold' => 0]);
            }else if(!empty($explode[0])){
              $group_id = 0; 
              $options = array(['Products.visible' => 1,'Products.sold' => 0]);
            }         
        }
        else{
          $group_id = 0;        
          $options = array(['Products.visible' => 1,'Products.sold' => 0]);
        }
        // echo  $this->request->session()->read('active_group');
        //print_r(@$explode1);
        if(strpos($group_id, '-') !== false) 
        {
          $group_id = 0; 
          $options = array(['Products.visible' => 1,'Products.sold' => 0]);
        }
        /*code added for removing 0 thickness*/
        if(!empty($thick))
        {
        $tcount=array_values(array_filter(explode(',', $thick)));
        if(count($tcount)>0)
        {
          $garray=array_values(array_filter(explode(',', $group_id)));
          //print_r($garray);
          for($g=0;$g<count($garray);$g++)
          {
            $res=$this->Products->find()->where(['Products.visible' => 1,'Products.group_id'=>$garray[$g],'Products.thickness in'=>$tcount])->count();
            if(!$res)
            {
              $garray[$g]=0;
              //$garray=array_filter($garray);
              if(!empty(array_filter($garray)))
              {
              $options = array(['Products.group_id in' => array_filter($garray),'Products.visible' => 1,'Products.sold' => 0]);
            }
            }
          }
          if(empty(array_filter($garray)))
          {
            $sel=1;
            $options = array(['Products.visible' => 1,'Products.sold' => 0]);
          }
          }

        }
        
        /* end code */
        //print_r($options);
         $thickness = $this->Products->find()->select(['Products.id','Products.group_id','Products.thickness','Products.grade'])->distinct(['Products.thickness'])->where($options)->order('Products.thickness')->hydrate(false)->toArray();
         //print_r($thickness);
      
	   $get_active_thick_id = $thick;
     $get_active_group_id = $group_id;
      
      $this->set(compact('thickness','get_active_thick_id','group_id','sel','get_active_group_id'));
     
      if ($this->request->is('ajax')) { 
      
        // pr($this->request);
            $this->viewBuilder()->layout(false);
            $this->render('/Element/pages/thickness');
        }


    }

   public function updatecount(){
      $this->loadModel('Products');
      $this->loadModel('Groups');
      $title = "Product Listing";
      $thick='';
    
        $options=array();
        if($this->request->query('group_id') ){  
          $group_id = $this->request->query('group_id');
          $explode = explode("?", $group_id);
            if(!empty($explode[0])){
              $group_id = $explode[0];
    if($this->request->query('thickness') && $this->request->query('thickness')!=0){
    $thick=$this->request->query('thickness');
           }
            }else if(!empty($explode[0])){
              $group_id = 0; 
    if($this->request->query('thickness') && $this->request->query('thickness')!=0){
    $thick=$this->request->query('thickness');

    }
            }         
        }
        else{
          if($this->request->query('group_id')!=0)
          $group_id = $this->request->query('group_id');
          else
           $group_id = 0;
         if($this->request->query('thickness') && $this->request->query('thickness')!=0){
         $thick=rtrim($this->request->query('thickness'),',');
          }
        }
       $get_active_group_id= $group_id;
        // echo  $this->request->session()->read('active_group');
       //query = $this->Products>find()->where(['visible' => 1,'group_id'=>$id])->count();
         $groups = $this->Groups->find()->select(['id', 'name'])->where(['status' => 1 ])->order('name')->hydrate(false)->toArray();
         //print_r($groups);
         $this->set(compact('groups','title','thick','get_active_group_id'));
     
      if ($this->request->is('ajax')) { 

      
            $this->viewBuilder()->layout(false);
           return $this->render('/Element/pages/group_count');
        }

    


    }

    public function addCart(){
      $this->autoRender = false;
      $this->loadModel('Carts');
      $this->loadModel('Products');
      if(!empty($this->request->data['id'])){
        $id = $this->request->data['id'];
        $check_product_exists = $this->Products->find()->where(['md5(id)' => $id])->count();
        if($check_product_exists ==1){
          if(empty($this->Auth->user("id"))){
            $check_cart_user_id = $this->Carts->find()->where(['user_session_id' =>  $this->Cookie->read("cart_user_id"), 'md5(product_id)' => $id,'user_id IS' => null])->count();
          }
          else{
            $check_cart_user_id = $this->Carts->find()->where(['user_session_id' =>  $this->Cookie->read("cart_user_id"), 'md5(product_id)' => $id, 'user_id' => $this->Auth->user("id"),'user_type' => $this->Auth->user("type")])->count();
          }

          if($check_cart_user_id == 0){
            $product = $this->Products->find()->select(['id','total_price','grade','thickness','country_of_origin','mill_name','price_cwt','mtr','tag_no','dimensions','lbs','heat','item_ser_no'])->contain(['Groups'])->where(['md5(Products.id)' => $id])->first();
            //print_r( $product ); 
           //echo $product['grade']; 
             //die();           

            $cart = $this->Carts->newEntity();
            $this->Cookie->write('CartItems',
              array('product_id' => $product['id'], 'quantity' => 1, 'price' => $product['total_price']));

            $postarray = [
              'user_id'      => !empty($this->Auth->user("id"))?$this->Auth->user("id"):NULL,
              'user_type'      => !empty($this->Auth->user("type"))?$this->Auth->user("type"):NULL,
              'user_session_id'      => !empty($this->Cookie->read("cart_user_id"))?$this->Cookie->read("cart_user_id"):"",  
              'product_id' =>     !empty($this->Cookie->read('CartItems.product_id')) ? $this->Cookie->read('CartItems.product_id') : '',   
              'quantity' =>     !empty($this->Cookie->read('CartItems.quantity')) ? $this->Cookie->read('CartItems.quantity') : '',                     
              'price'   => !empty($this->Cookie->read('CartItems.price')) ? $this->Cookie->read('CartItems.price') : '', 
              'tag_no'=>$product['tag_no'], 
              'grade'=>$product['grade'],
              'thickness'=>$product['thickness'],
              'dimensions'=>$product['dimensions'],
              'lbs'=>$product['lbs'],
              'country_of_origin'=>$product['country_of_origin'],
              'mill_name'=>$product['mill_name'],
              'price_cwt'=>$product['price_cwt'],
              'heat'=>$product['heat'],
              'item_ser_no'=>$product['item_ser_no'],
              'mtr'=>$product['mtr'],

            ];
           // print_r($postarray);die();
            $cart = $this->Carts->patchEntity($cart, $postarray);
            if ($this->Carts->save($cart)) {
              if(empty($this->Auth->user("id"))){
                $items_in_cart = $this->Carts->find()->where(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id IS' => null])->count();
                $user_id='null';
              }else{
               $items_in_cart = $this->Carts->find()->where(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id' => $this->Auth->user("id"),'user_type' => $this->Auth->user("type")])->count();
               $user_id=$this->Auth->user("id");
             }
             //if(!empty($this->request->session()->read("cart_data")))
            // {
            //$cart_data=$this->request->session()->read("cart_data");
            // }
            //$cart_data[]=array('user_id'=>$user_id,'pro_id'=>$product->id,'grade'=>$product->grade,'thickness'=>$product->thickness,'country_of_origin'=>$product->country_of_origin,'mill_name'=>$product->mill_name,'price_cwt'=>$product->price_cwt,'mtr'=>$product->mtr,'tag_no'=>$product->tag_no,'dimensions'=>$product->dimensions,'lbs'=>$product->lbs,'heat'=>$product->heat);
           // $this->request->session()->write("cart_data",$cart_data);
             // $product->sold = 2;
             // $this->Products->save($product);
             $result['status'] = 'success';
             $result['msg'] = 'Product added to cart';
             $result['item_count'] = $items_in_cart;
           }else{
            $result['status'] = 'error';
            $result['msg'] = 'Product not added to cart. Please try again later';
          }

          echo json_encode($result);
          exit();

        }else{
         $result['status'] = 'error';
         $result['msg'] = 'Product already added to cart';
         echo json_encode($result);
         exit();
       }

     }else{
       $result['status'] = 'error';
       $result['msg'] = 'Invalid Product';
       echo json_encode($result);
       exit();
     }

   }else{
     $result['status'] = 'error';
     $result['msg'] = 'Invalid Product key passed';
     echo json_encode($result);
     exit();
   }
 }

 public function viewCart(){
  $title = "Cart Listing";
  $this->loadModel('Carts');
  $this->loadModel('Products');
  if(empty($this->Auth->user('id'))){
    $get_cart_data = $this->Carts->find()->contain(['Products'=> function($q) {
                                return $q->where(['visible' => '1']);
                            },'Products.Groups'])->where(['user_session_id ' => $this->Cookie->read('cart_user_id'),'user_id IS' => null])->hydrate(false)->toArray();
  }else{
    $get_cart_data = $this->Carts->find()->contain(['Products'=> function($q) {
                                return $q->where(['visible' => '1']);
                            },'Products.Groups'])->where(['user_id' => $this->Auth->user("id"),'user_type' => $this->Auth->user("type")])->hydrate(false)->toArray();

  }
  if(!empty($this->Auth->user("id")))
  {
    $this->loadModel("Users");
    $get_user_detail = $this->Users->find()->select(['username','name','company','phone','existing_user'])->where(['id' => $this->Auth->user("id")])->first();
    $this->set(compact('get_user_detail')); 
  }

 $product_idss = array_map(function ($entry) {
          return $entry['product_id'];
        }, $get_cart_data);

  if(count($get_cart_data)>0)
  {
   if(!$this->request->is("Ajax"))
   {
        $check_product_exists = $this->Products->find()->select(['id','tag_no','count'=>'count(id)'])->where(['id IN' => $product_idss,'visible'=>1])->group('id')->hydrate(false)->toArray();
        if(!empty($check_product_exists))
        {
          if(count($check_product_exists)!=count($product_idss))
          {
            foreach($check_product_exists as $key=>$val)
            {
              $keys=array_search($val['id'],$product_idss);
              if($keys!==FALSE)
              {
                unset($product_idss[$keys]);
              }
            }
          
          }
        }
     
    }
  }


   $formatted = $this->Common->getRandomString(6);
   $store_in_session = $this->request->session()->write("code",$formatted);
   $this->set(compact('store_in_session'));
   $this->set(compact('get_cart_data','title','state_list','product_idss'));
}

public function orderitem()
{
   $result['pid']='';
   $result['condition']=1;
   $this->autoRender = false;
   $this->loadModel('Carts');
   $this->loadModel('Products');
  try
  {
    
      if(empty($this->Auth->user('id')))
      {
          $get_cart_data = $this->Carts->find()->contain(['Products'=> function($q) {
                                return $q->where(['visible' => '1']);
                            },'Products.Groups'])->where(['user_session_id ' => $this->Cookie->read('cart_user_id'),'user_id IS' => null])->hydrate(false)->toArray();
      }else{
          $get_cart_data = $this->Carts->find()->contain(['Products'=> function($q) {
                                return $q->where(['visible' => '1']);
                            },'Products.Groups'])->where(['user_id' => $this->Auth->user("id"),'user_type' => $this->Auth->user("type")])->hydrate(false)->toArray();
      }

      $product_idss = array_map(function ($entry) {
          return $entry['product_id'];
        }, $get_cart_data);

      if(!empty($product_idss))
      {
          $check_product_exists = $this->Products->find()->select(['id','tag_no','count'=>'count(id)'])->where(['id IN' => $product_idss,'visible'=>1])->group('id')->hydrate(false)->toArray();

          if(count($check_product_exists)!=count($product_idss))
          {
              foreach($check_product_exists as $key=>$val)
              {
                $keys=array_search($val['id'],$product_idss);
                if($keys!==FALSE)
                {
                  unset($product_idss[$keys]);
                }
              }
              $result['pid']=$product_idss;
              $result['condition']=2;
              throw new Exception("At least one item is no longer available for sale. Please remove it to continue11.");
          }
    
      $check_cart_user_id = $this->Carts->find()->where(['user_id' => $this->Auth->user("id"),'user_type' => $this->Auth->user("type")])->count();

      if($check_cart_user_id == 0)
      {
          throw new Exception("Your cart is empty.");
      }

            
      $formatted = implode(' ',str_split($this->request->session()->read("code")));    
        
      $users_table = TableRegistry::get("users");
        
      $user_id = '';
      $filePath = !empty($this->Auth->user('id'))?'order-'.$this->Auth->user('id').'.xml':'order-0.xml';
    
      if(!empty($this->Auth->user('id')))
      {
          $this->loadModel("Users");
          $get_user_detail = $this->Users->find()->select(['username','name','company','phone','existing_user'])->where(['id' => $this->Auth->user("id")])->first();
            $check_return_data =  $this->Common->generateXml($get_cart_data, $filePath, $get_user_detail, $this->request->data);

            if($check_return_data['email_status']=="success" && $check_return_data['xml_status']=="xml_success")
            {               
                $customers_table= TableRegistry::get("customers");
                $this->request->data['user_id'] = $this->Auth->user("id");
                $this->request->data['need_by_date'] = $this->request->data['need_by_date'];
                $this->request->data['comment'] = $this->request->data['comment'];
                $this->request->data['deliver_order'] = $this->request->data['comment'];
                $this->request->data['address_one'] = !empty($this->request->data['address_one'])?$this->request->data['address_one']:"";
                $this->request->data['address_two'] = !empty($this->request->data['address_two'])?$this->request->data['address_two']:"";
                $this->request->data['city'] = !empty($this->request->data['city'])?$this->request->data['city']:"";
                $this->request->data['state'] = !empty($this->request->data['state'])?$this->request->data['state']:"";
                $this->request->data['zip'] = !empty($this->request->data['zip'])?$this->request->data['zip']:"";
                $this->request->data['payment_method'] = $this->request->data['payment_method'];
                $this->request->data['created'] = date("Y-m-d H:i:s");
                $newEntity = $customers_table->newEntity($this->request->data());
                if($customers_table->save($newEntity))
                {                        
                    $get_cart = $this->Carts->find()->select(['product_id'])->where(['user_id' => $this->Auth->user("id"),'user_type' => $this->Auth->user("type")])->hydrate(false)->toArray();

                    $product_ids = array_map(function ($entry) {
                        return $entry['product_id'];
                      }, $get_cart);
        
                    if(!empty($get_cart)){
                        $purchases_table = TableRegistry::get("purchases");
                        foreach ($get_cart as $key => $value) {
                            $save_purchases = $purchases_table->newEntity();
                            $save_purchases->customer_id = $newEntity->id;
                            $save_purchases->user_id = $this->Auth->user("id");
                            $save_purchases->user_type = $this->Auth->user("type");
                            $save_purchases->product_id = $value['product_id'];
                            $save_purchases->purchase_date = date("Y-m-d H:i:s");
                            $purchases_table->save($save_purchases);
                        }
                    }

          $this->Carts->deleteAll(['user_id' => $this->Auth->user("id"),'user_type' => $this->Auth->user("type")]);

          $result['status'] = "success";
          $result["msg"] = "Your request has been submitted. We will contact you swiftly." ;
          echo json_encode($result);
          exit();   
        }else{
          throw new Exception("Something went wrong. Order not added.");
        }

      }else{
        throw new Exception("Something went wrong. Please try again later.");
      }

    }
    else{
      throw new Exception("You are already regitsred with NSSCO. Please do login to continue");
    }
    }
    else
        {
          throw new Exception("Your cart is empty.");
        }

  }catch (Exception $ex) {
    $result['status'] = "error";
    $result["msg"] = $ex->getMessage();
    echo json_encode($result);
  }
  exit();

}

public function deleteCartitem(){
  $this->loadModel('Carts');
  if(!empty($this->request->data['id'])){
    $id = $this->request->data['id'];
    $check_product_exists = $this->Carts->find()->where(['md5(id)' => $id])->first();

    if(count($check_product_exists) == 1){

      if($this->Carts->deleteAll(['md5(id)' => $this->request->data['id']])){
        $this->loadModel("Products");
       $product_count = $this->Products->find()->where(['id'=>$check_product_exists['product_id']])->count();
        if($product_count)
        {
        $product = $this->Products->get($check_product_exists['product_id']);
        $product->sold = 0;
        $this->Products->save($product);
        }
        if(empty($this->Auth->user("id"))){
          $check_cart = $this->Carts->find()->select(['user_session_id','price'])->where(['user_session_id' =>  $this->Cookie->read("cart_user_id"), 'user_id IS' => NULl])->hydrate(false)->toArray();
        }else{
          $check_cart = $this->Carts->find()->select(['user_session_id','price'])->where(['user_id' => $this->Auth->user("id"),'user_type' => $this->Auth->user("type")])->hydrate(false)->toArray();
        }
       // $cart_data=$this->request->session()->read("cart_data");
       // print_r($cart_data);
        //echo $check_product_exists['product_id'];
        //die();
        /*for($j=0;$j<count($cart_data);$j++)
        {
          if(isset($cart_data[$j]))
          {
          if($cart_data[$j]['pro_id']==$check_product_exists['product_id'])
          {
            unset($cart_data[$j]);
            array_values($cart_data);
            break;

 
          }
        }
        }*/
       // $this->request->session()->write("cart_data",array_values($cart_data));
        $grand_total = 0;
        if(count($check_cart) > 0){
          foreach ($check_cart as $key => $value) {
            $grand_total += $value['price'];
          }
        } 


        $result['status'] = 'success';
        $result['msg'] = 'Product deleted from cart';
        $result['grand_total'] = number_format($grand_total,2);
        $result['item_in_cart'] = count($check_cart);
      }else{
        $result['status'] = 'error';
        $result['msg'] = 'Product not deleted from cart. Please try again later';
      }       
      echo json_encode($result);
      exit(); 

    }else{
     $result['status'] = 'error';
     $result['msg'] = 'Invalid Product';
     echo json_encode($result);
     exit();
   }


 }else{
   $result['status'] = 'error';
   $result['msg'] = 'Invalid Product key passed';
   echo json_encode($result);
   exit(); 
 }

}

public function deleteFromCart(){
  $this->autoRender = false;
   $this->loadModel('Carts');
   $id = $this->request->data['id'];
    
         if(empty($this->Auth->user('id'))){
         $delete_cartitem = $this->Carts->DeleteAll(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id IS' => null, 'md5(product_id)' => $this->request->data['id']]);
         $count_item = $this->Carts->find()->where(['user_session_id' => $this->Cookie->read("cart_user_id"),'user_id IS' => null])->count();
     }else{
      $delete_cartitem = $this->Carts->DeleteAll(['user_id' => $this->Auth->user("id"), 'md5(product_id)' => $this->request->data['id']]);

        $count_item = $this->Carts->find()->where(['user_id' => $this->Auth->user("id"),'user_type' => $this->Auth->user("type")])->count();
     }

     /*$cart_data=$this->request->session()->read("cart_data");
        for($j=0;$j<count($cart_data);$j++)
        {
          if(isset($cart_data[$j]))
          {
          if(md5($cart_data[$j]['pro_id'])==$id)
          {
            unset($cart_data[$j]);
            array_values($cart_data);
            break;

 
          }
        }
        }
      $this->request->session()->write("cart_data",array_values($cart_data));
      */
      $result['item_count'] = $count_item;
      echo json_encode($result);

}

public function getProductBygroup(){
  $this->loadModel('Products');
  $this->loadModel('Groups');
  $this->loadModel('Mtr');

  $paginationCountChange = $this->pagelimit;
      if ($this->request->query('paginationCountChange')) {
            $paginationCountChange = $this->request->query('paginationCountChange');
        }

      
        // pr($products); die;

         

  // $where = "";
  // echo $this->request->data['id'];
     // pr($this->request->data); 

  if($this->request->data['id']==0){  
  echo "0 Id "; 
    $group_name = "View All";
    $options = array(['Products.visible' => 1,'Products.sold' => 0]);
  }else if(isset($this->request->data)){
    echo "some_id";
    $id = $this->request->data['id']; 
    $options = array(['md5(group_id)' => $id, 'Products.visible' => 1, 'Products.sold' => 0]);
    
    $group_name = $this->Groups->find()->select(['name'])->where(['md5(id)' => $id])->hydrate(false)->first();
    // $where = ['md5(group_id)' => $id, 'Products.visible' => 1, 'Products.sold' => 0];
  }
  
  $this->paginate = [
            'limit' => $paginationCountChange,
            'conditions' => $options,
            'order' => ['Products.id' => 'desc'],
            'contain' => ['Groups'],
                //'group'=>['ClientPackages.usr_id']
        ];     

        $products = $this->paginate($this->Products);

        $products_count = $this->Products->find()->contain(['Groups'])->where($options)->count();
        echo $products_count;

  // $products = $this->Products->find()->contain(['Groups'])->where($where)->hydrate(false)->toArray();

  $get_mtr = $this->Mtr->find()->select(['Mtr.item_ser_no','Mtr.mtr_tag_sql_recid','Mtr.mtr','Mtr.notes'])->hydrate(false)->toArray();

  $this->set(compact('products','group_name','get_mtr','paginationCountChange','products_count'));
}

public function refreshCaptcha(){
  $this->autoRender = false;
  session_start();
  $captcha_num = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz';
  $captcha_num = substr(str_shuffle($captcha_num), 0, 6);
  $_SESSION["code"] = $captcha_num;
  $formatted = implode(' ',str_split($_SESSION["code"])); 
  $result['code'] = $formatted;
  echo json_encode($result);
}
  
  public function showShippingAddress(){
  $states = TableRegistry::get("states");       
        $state_list = $states->find("list", ['keyField' => 'state_abbrivation','valueField' => 'state_abbrivation'])->order('name')->toArray();

        $this->set(compact('state_list'));
}

public function phpinfo(){
  phpinfo(); die;
}
public function filter(){
  $this->loadModel('Products');
  $this->loadModel('Carts');
  $this->loadModel('Groups');
  $this->loadModel('Mtr');
  $title = "Product Listing";
  $get_active_thick_id=0;
  $get_active_group_id=0;
  if($this->request->is('get'))
  {
    if(($this->request->query('check')!='' || $this->request->query('check')!=0) && ($this->request->query('thick')!='' || $this->request->query('thick')!=0))
    {
      $options = array(['Products.visible' => 1,'Products.sold' => 0,'Products.group_id IN'=>array_filter(explode('_',$this->request->query('check'))),'Products.thickness in'=>array_filter(explode('_',$this->request->query('thick')))]);
      $get_active_group_id=array_values(array_filter(explode('_',$this->request->query('check'))));
      $get_active_thick_id=array_values(array_filter(explode('_',$this->request->query('thick'))));
      $arr=array_count_values ($get_active_thick_id);
      $arr1=array_count_values ($get_active_group_id);
      foreach($arr as$r=>$v)
      {
          if($v>1)
          {
             $key=array_search($r,$get_active_thick_id);
            unset($get_active_thick_id[$key]);
            $key=array_search($r,$get_active_thick_id);
            unset($get_active_thick_id[$key]);
            return $this->redirect([
    'controller' => 'Pages',
    'action' => 'filter',
    '?' => [
        'filter' => 'true',
        'check' => implode('_',$get_active_group_id),
        'thick' => implode('_',$get_active_thick_id)
    ],
]);
            break;
          }
      }
      foreach($arr1 as$r=>$v)
      {
          if($v>1)
          {
             $key=array_search($r,$get_active_group_id);
            unset($get_active_group_id[$key]);
            $key=array_search($r,$get_active_group_id);
            unset($get_active_group_id[$key]);
            return $this->redirect([
    'controller' => 'Pages',
    'action' => 'filter',
    '?' => [
        'filter' => 'true',
        'check' => implode('_',$get_active_group_id),
        'thick' => implode('_',$get_active_thick_id)
    ],
]);
            break;
          }
      }
    }
    else if(($this->request->query('check')!='' || $this->request->query('check')!=0) && $this->request->query('thick')=='')
    {
      $options = array(['Products.visible' => 1,'Products.sold' => 0,'Products.group_id IN'=>array_filter(explode('_',$this->request->query('check')))]);
      $get_active_group_id=array_values(array_filter(explode('_',$this->request->query('check'))));
      //print_r($get_active_group_id);
      $arr=array_count_values ($get_active_group_id);
      foreach($arr as$r=>$v)
      {
          if($v>1)
          {
            $key=array_search($r,$get_active_group_id);
            unset($get_active_group_id[$key]);
            $key=array_search($r,$get_active_group_id);
            unset($get_active_group_id[$key]);
            return $this->redirect([
    'controller' => 'Pages',
    'action' => 'filter',
    '?' => [
        'filter' => 'true',
        'check' => implode('_',$get_active_group_id),
    ],
]);
            break;
          }
      }
      //print_r($get_active_group_id);
      
    }
    else if($this->request->query('check')=='' && ($this->request->query('thick')!='' || $this->request->query('thick')!=0))
    {
      $options = array(['Products.visible' => 1,'Products.sold' => 0,'Products.thickness in'=>array_filter(explode('_',$this->request->query('thick')))]);
      $get_active_thick_id=array_values(array_filter(explode('_',$this->request->query('thick'))));
       $arr=array_count_values ($get_active_thick_id);
      foreach($arr as$r=>$v)
      {
          if($v>1)
          {
            $key=array_search($r,$get_active_thick_id);
            unset($get_active_thick_id[$key]);
            $key=array_search($r,$get_active_thick_id);
            unset($get_active_thick_id[$key]);
            return $this->redirect([
    'controller' => 'Pages',
    'action' => 'filter',
    '?' => [
        'filter' => 'true',
        'thick' => implode('_',$get_active_thick_id),
    ],
]);
            break;
          }
      }
    }
    else
    {
      $options = array(['Products.visible' => 1,'Products.sold' => 0]);
    }
    
  }
  else
  {
    $options = array(['Products.visible' => 1,'Products.sold' => 0]);
  }
  //print_r($options);die();
  $paginationCountChange = $this->pagelimit;
  if ($this->request->query('paginationCountChange')) {
            $paginationCountChange = $this->request->query('paginationCountChange');
        }
        
        $this->paginate = [
            'limit' => $paginationCountChange,
            'conditions' => $options,
            'order' => ['CAST(Products.grade AS UNSIGNED),Products.grade asc','CAST(Products.thickness AS DECIMAL(10,5)),Products.thickness asc','CAST(Products.lbs AS DECIMAL(10,5)),Products.lbs desc'],
            'contain' => ['Groups'],            
        ];     
        $products = $this->paginate($this->Products)->toArray();
        $products_count = $this->Products->find()->contain(['Groups'])->where($options)->count();
        //debug($products_count);
         $thickness = $this->Products->find()->select(['Products.id','Products.group_id','Products.thickness','Products.grade'])->distinct(['Products.thickness'])->where($options)->order('Products.thickness')->toArray();
      $groups = $this->Groups->find()->select(['id', 'name'])->where(['status' => 1 ])->order('name')->hydrate(false)->toArray();
       $this->set(compact('products','groups','title','paginationCountChange','products_count','thickness','get_active_group_id','get_active_thick_id'));
     
 
}

}
