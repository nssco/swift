<table id="ifta_table" width="100%" border="1" class="font-size" cellpadding="5" cellspacing="5">

  <thead>
    <tr>
                <td width="7%" valign="top" bgcolor="#f0f0f0"><strong>Tag #</strong></td>
                <td width="5%" align="center" valign="top" bgcolor="#f0f0f0"><strong>Quantity</strong></td>
                <td width="9%" valign="top" bgcolor="#f0f0f0"><strong>Grade</strong></td>
                <td width="6%" valign="top" bgcolor="#f0f0f0"><strong>Thickness<br>
                  (in)</strong></td>
                <td width="6%" valign="top" bgcolor="#f0f0f0"><strong>Dimensions<br>
                  (in)</strong></td>
                <td width="5%" valign="top" bgcolor="#f0f0f0"><strong>Weight<br>
                  (lb)</strong></td>
                <td width="11%" valign="top" bgcolor="#f0f0f0"><strong>Country of Origin</strong></td>
                <td width="7%" valign="top" bgcolor="#f0f0f0"><strong>Mill Name</strong></td>
                <td width="8%" align="right" valign="top" bgcolor="#f0f0f0"><strong>Price/CWT</strong></td>
                <td width="7%" align="right" valign="top" bgcolor="#f0f0f0"><strong>Total Price</strong></td>
                <td width="14%" valign="top" bgcolor="#f0f0f0"><strong></strong></td>
                <td width="15%" valign="top" bgcolor="#f0f0f0">&nbsp;</td>
              </tr>
  </thead>
  <tbody class="model-ajax-data">
    <?php
    if(isset($products) && count($products) > 0) {
     $extension = array("jpg", "jpeg", "png", "gif", "pdf");
     foreach ($products as $pro):?>
     <tr>
      <!-- <td><?= ($pro['item_ser_no'] != '' ? $pro['item_ser_no'] : '--'); ?></td>                
      <td>
        <?php 
            if($pro['mtr'] != ''){
                
                $get_extension = explode(".", $pro['mtr']);
                $get_extension = end($get_extension);
                // echo $get_extension; die;
                if (in_array($get_extension, $extension)){
                    if(file_exists(WEBROOT_MTR_PATH.$pro['mtr'])){ ?>
                    <a class="download_pdf" download href="<?php echo $SITEURL;?>uploads/mtr/<?php echo $pro['mtr'];?>">Download Mtr</a>
                   <?php } else { echo "--";}
                } 
            }else{
                echo "--";
            }
                ?>
      </td> -->
      <td><?= ($pro['tag_no'] != '' ? $pro['tag_no'] : '--'); ?></td>
      <!-- <td><?= ($pro['group']['name'] != '' ? $pro['group']['name'] : '--'); ?></td> -->
      <td align="center">1</td>
      <td><?= ($pro['grade'] != '' ? $pro['grade'] : '--'); ?></td>
      <td><?= ($pro['thickness'] != '' ? $pro['thickness'] : '--'); ?></td>
      <td><?= ($pro['dimensions'] != '' ? $pro['dimensions'] : '--'); ?></td>
      <td><?= ($pro['lbs'] != '' ? $pro['lbs'] : '--'); ?></td>
      <!-- <td><?= ($pro['sf'] != '' ? $pro['sf'] : '--'); ?></td>
      <td><?= ($pro['heat'] != '' ? $pro['heat'] : '--'); ?></td>
      <td><?= ($pro['slab'] != '' ? $pro['slab'] : '--'); ?></td> -->
      <td><?= ($pro['country_of_origin'] != '' ? ucwords(strtolower($pro['country_of_origin'])) : '--'); ?></td>
      <td><?= ($pro['mill_name'] != '' ? ucwords(strtolower($pro['mill_name'])) : '--'); ?></td>
      <td align="right"><?= ($pro['price_cwt'] != '' ? '$'.number_format($pro['price_cwt'],2) : '--'); ?></td>
      <td align="right"><?= ($pro['total_price'] != '' ? '$'.number_format($pro['total_price'],2) : '--'); ?></td>
      <td align="center">
        <?php if($pro['mtr'] != '' && file_exists(WEBROOT_MTR_PATH.$pro['mtr'])){ ?>
         <a href="<?php echo $SITEURL;?>uploads/mtr/<?php echo $pro['mtr'];?>" target="_blank" title="Test Reports"><div class="bluee-btn"><span class="glyphicon glyphicon-list-alt blk-bg" aria-hidden="true"></span> Test Reports</div></a>
        <?php  }else{echo "--";} ?>
        
      </td>
      <td><center>
        <a href="javascript:void(0)" class="add_to_cart" id="<?php echo md5($pro['id']);?>" title="Add to Cart"><div class="bluee-btn">
                    <span class="glyphicon glyphicon-shopping-cart blk-bg" aria-hidden="true"></span> Add to Cart
                    </div></a>
       <!-- <div class="tooltip" style="color:#000;"><a href="javascript:void(0)" class="add_to_cart" id="<?php echo md5($pro['id']);?>" title="Add to Cart"><?php echo $this->Html->image("cart-icon.png");?><span class="tooltiptext tooltip-top">Add to Cart</span></a></div> -->
     </center></td> 
   </tr>
 <?php  endforeach;
}else { ?>
<tr>
  <td class="text-center" colspan="16">
    No Products to Show
  </td>
</tr>
<?php } ?>
</tbody>
</table>
<?= $this->Html->css('jquery.dataTables.min'); ?>
<?= $this->Html->script(['jquery.min'], ['fullBase' => true]) ?>
<?= $this->Html->script(['jquery.dataTables.min'], ['fullBase' => true]) ?>
<script>
  $(document).ready(function(){
   $('#ifta_table').dataTable({           
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "bAutoWidth": false,
    "dom":' <"search-box"f><"top"l>rt<"bottom"ip><"clear">',
    "searching": false
  }); 
 });
</script>