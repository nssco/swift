<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
      <p class="yellow">* This is a request-for-order system for a selected group of the current North Shore Steel plate inventory. <br> We have additional plate, structural steel, and fittings that are not currently available for online sales.<br>
        Call us to inquire.</p>
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bhoechie-tab-menu left-content">
        <h1>Products</h1>
        <?php echo $this->element("group_list");?>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 bhoechie-tab right-content">
        <div class="bhoechie-tab-content active table-box">
          <h2 class="group_name"></h2>
          <h6 class="none">See More Table scroll to right <span class="glyphicon glyphicon-chevron-right color" aria-hidden="true"></span></h6>
          <div class="table-responsive table-div product_table">
            <table id="ifta_table"  width="100%" border="1" class="font-size" cellpadding="5" cellspacing="5">
              <thead>
              <tr>
                <td width="7%" valign="top" bgcolor="#f0f0f0"><strong>Tag #</strong></td>
                <td width="5%" align="center" valign="top" bgcolor="#f0f0f0"><strong>Quantity</strong></td>
                <td width="9%" valign="top" bgcolor="#f0f0f0"><strong>Grade</strong></td>
                <td width="6%" valign="top" bgcolor="#f0f0f0"><strong>Thickness<br>
                  (in)</strong></td>
                <td width="6%" valign="top" bgcolor="#f0f0f0"><strong>Dimensions<br>
                  (in)</strong></td>
                <td width="5%" valign="top" bgcolor="#f0f0f0"><strong>Weight<br>
                  (lb)</strong></td>
                <td width="11%" valign="top" bgcolor="#f0f0f0"><strong>Country of Origin</strong></td>
                <td width="7%" valign="top" bgcolor="#f0f0f0"><strong>Mill Name</strong></td>
                <td width="8%" align="right" valign="top" bgcolor="#f0f0f0"><strong>Price/CWT</strong></td>
                <td width="7%" align="right" valign="top" bgcolor="#f0f0f0"><strong>Total Price</strong></td>
                <td width="14%" valign="top" bgcolor="#f0f0f0"><strong></strong></td>
                <td width="15%" valign="top" bgcolor="#f0f0f0">&nbsp;</td>
              </tr>
            </thead>
            <?php echo $this->element("index");?>
            
            </table>
          </div>
          <h6 class="none">See More Table scroll to right <span class="glyphicon glyphicon-chevron-right color" aria-hidden="true"></span></h6>
         <!--  <section>
            <nav role="navigation">
              <ul class="cd-pagination">
                <li class="button"><a class="white" href="#0"><span class="glyphicon glyphicon-chevron-left color2" aria-hidden="true"></span><span class="txt-none">Previous</span></a></li>
                <li><a class="current" href="#0">1</a></li>
                <li><a class="gry" href="#0">2</a></li>
                <li class="button"><a class="blk" href="#0"><span class="glyphicon glyphicon-chevron-right blk" aria-hidden="true"></span></a></li>
              </ul>
            </nav>
            <!-- cd-pagination-wrapper 
          </section> -->
        </div>
        
        

      </div>
    </div>