<!DOCTYPE html>
<html>
  <head>
     <script type="text/javascript">
    var site_url = '<?= $SITEURL; ?>';
  </script>  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  
  <title> <?php if(!empty($title)) { 
            echo $project_name.": ".$title;
        }else{
            echo 'Web Services';
        } ?></title>
 <?php 
    echo $this->Html->css("jquery-ui");
    echo $this->Html->css("all");
    echo $this->Html->css("global");
    echo $this->Html->css("new");
    echo $this->Html->css("settings");
    echo $this->Html->css("pagination");
    echo $this->Html->css("pagination");
     echo $this->Html->css('layers') ;
    echo $this->Html->css('custom');
    echo $this->Html->css('font-awesome.min');
    echo $this->Html->css('bootstrap.min');
    echo $this->Html->css('jquery.dataTables.min');
    echo $this->Html->script('jquery.min');
    echo $this->Html->script("bootstrap.min");
    echo $this->Html->script("jquery.dataTables.min");
    echo $this->Html->script("jquery.ui.min");
    echo $this->Html->script('sweetalert.min');
    echo $this->Html->css('sweetalert');
    echo $this->Html->script('custom');
    echo $this->Html->script("jquery.validate.min");
    echo $this->Html->script("validation");    
 
    ?>
 
  </head>

   <body>
<div id="headerWrap" style="background:#000; position:relative;">
    <div id="headerContainer">
    <div id="logo"><a href="http://www.nssco.com/"><?php echo $this->html->image("logo.png");?></a></div>
    <div class="right-box"> <span class="back"><a href="http://www.nssco.com/assets/pdf/NSSCO_BROCHURE.pdf" target="_blank">Download Our Brochure</a></span> <a href="<?php echo $SITEURL;?>pages/view_cart" class="view_cart"><span class="cart right-spce"> <span class="img"><span class="number cart_total"><?php echo $get_cartitem;?></span><?php echo $this->html->image("arrow.png");?></span> </span></a>
     <?php if(empty($this->request->session()->read('Auth.User.id')) && $this->request->params['action'] !="signin" && $this->request->params['action'] !="login"){


      ?>
       <a href="<?php echo $SITEURL;?>users/signin" class="singin"> Sign In</a>
       <?php if($this->request->params['action'] !='register'){ ?>
        <a href="<?php echo $SITEURL;?>users/register" class="singup"> Create Login</a>
        <?php  } ?> 
     <?php } else if(!empty($this->request->session()->read('Auth.User.id'))){ 
      echo $this->element("menu");
   }
     ?>


     </div>
  </div>
  </div>
<div class="blankSpacer"></div>

<div id="pageContent">

<div id="pageWrap" class="container top-mr">

    <div class="row">