<div class="dropdown down postonn">
        <button class="btn btn-default dropdown-toggle bluecolor" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> Welcome <?php echo $this->request->session()->read('Auth.User.name');?> <span class="caret"></span> </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="<?php echo $SITEURL;?>users/profile" class=""><span class="glyphicon glyphicon-user float-icon" aria-hidden="true"></span> My Account </a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo $SITEURL;?>users/change_password"><span class="glyphicon glyphicon-lock float-icon" aria-hidden="true"></span> Change Password </a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo $SITEURL;?>users/logout"><span class="glyphicon glyphicon-off float-icon" aria-hidden="true"></span> Logout </a></li>
          </ul>
      </div>
