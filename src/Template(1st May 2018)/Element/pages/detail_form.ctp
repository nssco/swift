<div class="container">
        <div class="row">
            <div class="col-md-11 padg-div left-box">
             <?php echo $this->Form->create("/", ['method' => 'post',"id" => "odrer_product"]);?>
                <h1>Optional details</h1>
                <div class="col-md-7 col-sm-7 col-xs-12 form-group leftpd">
                 <?php echo $this->Form->input("comment",['class' => 'form-control comments-box common',"label" => false, 'placeholder' => 'Comments', 'id' => 'comment', 'rows' => '8' , 'type' => 'textarea']);?>
              </div>
                <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                <?php echo $this->Form->input("po",['class' => 'form-control6 common',"label" => false, 'placeholder' => 'PO']);?>
              </div>
                <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                  <?php echo $this->Form->input("need_by_date",['class' => 'form-control3 calendar common',"label" => false, 'placeholder' => 'Need by date', 'id' => 'datepicker']);?>
               
              </div>
                <div class="col-md-4 col-sm-4 ">
                <button class="submit-btn btn_1 pull-right" type="submit">Submit</button>
              </div>
              </form>
          </div>
          </div>
      </div>