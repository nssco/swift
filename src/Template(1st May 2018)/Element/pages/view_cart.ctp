<?php
if(isset($get_cart_data) && count($get_cart_data) > 0) {
 $extension = array("jpg", "jpeg", "png", "gif", "pdf");
 $grand_total = 0;
 $i=1; foreach ($get_cart_data as $pro):
 $grand_total += $pro['price'];

 ?>
 <tr class="list_cart_product show_product_incart">
    <td class="td_order" bgcolor="#0078ae"><?=$i;?></td>
    <!-- <td align="center"><?= ($pro['products']['mtr'] != '' && file_exists(WEBROOT_MTR_PATH.$pro['products']['mtr']) ? $this->Html->image('../uploads/mtr/'.$pro['products']['mtr'],["width" => "100", "height" => "100"]) : '--'); ?></td> -->
   <!--  <td>
        <?php if($pro['products']['mtr'] != ''){
                
                $get_extension = explode(".", $pro['products']['mtr']);
                $get_extension = end($get_extension);
                // echo $get_extension; die;
                if (in_array($get_extension, $extension)){
                    if(file_exists(WEBROOT_MTR_PATH.$pro['products']['mtr'])){ ?>
                    <a class="download_pdf" download href="<?php echo $SITEURL;?>uploads/mtr/<?php echo $pro['products']['mtr'];?>">Download Mtr</a>
                   <?php } else { echo "--";}
                } 
            }else{
                echo "--";
            }
        ?>
    </td> -->
    <td><?= ($pro['products']['tag_no'] != '' ? $pro['products']['tag_no'] : '--'); ?></td>
    <td align="center"><?=$pro['quantity'];?></td>
    <td><?= ($pro['products']['grade'] != '' ? $pro['products']['grade'] : '--'); ?></td>
    <td><?= ($pro['products']['thickness'] != '' ? $pro['products']['thickness'] : '--'); ?></td>
    <td><?= ($pro['products']['dimensions'] != '' ? $pro['products']['dimensions'] : '--'); ?></td>
    <td><?= ($pro['products']['lbs'] != '' ? $pro['products']['lbs'] : '--'); ?></td>
    <!-- <td><?= ($pro['products']['sf'] != '' ? $pro['products']['sf'] : '--'); ?></td>
    <td><?= ($pro['products']['heat'] != '' ? $pro['products']['heat'] : '--'); ?></td>
    <td><?= ($pro['products']['slab'] != '' ? $pro['products']['slab'] : '--'); ?></td> -->
    <td><?= ($pro['products']['country_of_origin'] != '' ? ucwords(strtolower($pro['products']['country_of_origin'])) : '--'); ?></td>
    <td><?= ($pro['products']['mill_name'] != '' ? ucwords(strtolower($pro['products']['mill_name'])) : '--'); ?></td>
    <td align="right"><?= ($pro['products']['price_cwt'] != '' ? '$'.number_format($pro['products']['price_cwt'],2) : '--'); ?></td>
    <td align="right"><?= ($pro['price'] != '' ? '$'.number_format($pro['price'],2) : '--'); ?></td>
    <td align="center"><center>
            <?php if($pro['products']['mtr'] != '' && file_exists(WEBROOT_MTR_PATH.$pro['products']['mtr'])){ ?>
            <a href="<?php echo $SITEURL;?>uploads/mtr/<?php echo $pro['products']['mtr'];?>" target="_blank" title="Test Reports"><div class="bluee-btn"><span class="glyphicon glyphicon-list-alt blk-bg" aria-hidden="true"></span> Test Reports</div></a>
           <!--  <div class="tooltip" style="color:#000;"><a href="<?php echo $SITEURL;?>uploads/mtr/<?php echo $pro->mtr;?>" target="_blank" title="View MTR"><?php echo $this->Html->image("privew.png");?><span class="tooltiptext tooltip-top">View MTR</span></a>  </div> -->
           <!--  <div class="tooltip" style="color:#000;"><a href="<?php echo $SITEURL;?>uploads/mtr/<?php echo $pro->mtr;?>" download title="Download MTR"><?php echo $this->Html->image("download.png");?><span class="tooltiptext tooltip-top">Download MTR</span></a></div> -->
        <?php  }else{echo "--";} ?> 
</center></td>
    <td><center>
        <a href="javascript:void(0)" class="delete_cartitem" id="<?php echo md5($pro['id']);?>" title="Delete"><div class="bluee-btn">
                    <span class="glyphicon glyphicon-trash blk-bg" aria-hidden="true"></span> Delete
                    </div></a>
       <!--  <div class="tooltip" style="color:#000;"><a href="javascript:void(0)" class="add_to_cart" id="<?php echo md5($pro->id);?>" title="Add to Cart"><?php echo $this->Html->image("cart-icon.png");?><span class="tooltiptext tooltip-top">Add to Cart</span></a></div> -->
    </center>
        </td>
</tr>
<?php  $i++; endforeach;?>
<tr class="list_cart_product">
    <td style="text-align: right;" scope="col" colspan="10"><strong>Total</strong></td>
    <td align="right"><strong class="calculate_grand_total">$<?php echo number_format($grand_total,2);?></strong></td>
    <td></td>
    <td></td>
</tr>
<tr class="no_products" style="display: none;">
    <td class="text-center" colspan="16">
        No Products to Show
    </td>
</tr>
<?php }else { ?>
<tr>
    <td class="text-center" colspan="16">
        No Products to Show
    </td>
</tr>
<?php } ?>


