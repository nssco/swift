    <?php echo $this->element("header");    
    echo $this->fetch('content');
    if($this->request->params['action'] != "documentation"){
      echo $this->element("footer");
    }
    ?>
    <div class="overlay">
      <div class="imageHol">  
        <?php echo $this->Html->image('pageloader.gif'); ?>
      </div>
    </div>
    
    <script>
      $(document).ready(function(){
        $(".success_message,.error_message").delay(5000).hide("slow");
       $('#ifta_table').dataTable({           
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bAutoWidth": false,
        "dom":' <"search-box"f><"top"l>rt<"bottom"ip><"clear">',
        "searching": false
      }); 

       $("#datepicker").datepicker(
        { minDate: 0,dateFormat: 'dd-mm-yy'}
        );

     });

   </script>




