<header class="row">
    <div class="header-title">
        <h1>NSSCO [REST] Web Services</h1>
    </div>
</header>

<div class="row">
    <div class="columns large-12">
        API REQUEST URL :- <a href="http://securedstaging.com/nssco/api" target="_blank">http://securedstaging.com/nssco/api</a>
        <br>
        <!-----add product method--------------------->
        <hr/>
        
        <u>Add Product API Method</u>
        <br/>
        <b>Request Parameters:(Method: item_update)</b>
        <ul>
            <li> => method (Mandatory)</li>
            <li> => responseType (Optional)</li>
            <li> => tokenID (Mandatory)</li>
            <li> => group_name (Mandatory should be string)</li>
            <li> => item_ser_no (Mandatory should be number. This is unique field for product.)</li>
            <li> => tag_no (Mandatory should be string)</li>
            <li> => grade (Mandatory should be string)</li>
            <li> => price_cwt (Mandatory should be floating value)</li>
            <li> => total_price (Mandatory should be floating value)</li>
            <li> => visible (Mandatory should be binary value of 0 or 1)</li>
            <li> => thickness (Optional should be floating value)</li>
            <li> => dimensions (Optional should be string)</li>
            <li> => lbs (Optional should be floating value)</li>
            <li> => sf (Optional should be floating value)</li>
            <li> => heat (Optional should be string)</li>
            <li> => slab (Optional should be string)</li>
            <li> => country_of_origin (Optional should be string)</li>
            <li> => mill_name (Optional should be string)</li>
        </ul>
        <b>Response  Parameters</b>
        <ul>
            <li>=> status ( value : success/error )</li>
            <li>=> message ( Description of message or error )</li>
            <li>=> data ( NULL  )</li>
        </ul>
        <b>Sample Request:</b>

        {<br/>
            method : item_update,<br/>
            responseType : json <br/>
            tokenID : token <br/>
        }
        
        <br/>
        <b>Sample Response:</b> 

        {<br/>
          "status": "success",<br/>
          "message": "Product added successfully.",<br/>
            "data": {<br/>
            <font style="padding-left:50px;float:left;">"item_ser_no": "123"</font> <br/>
            <font style="padding-left:30px;float:left;">}</font><br/>
        }<br/>
        
        <hr/>
        <!---------hide product api method------------>
        
        <u>Hide All Products API Method</u>
        <br/>
        <b>Request Parameters(Method: hide_all)</b>
        <ul>
            <li> => method (Mandatory)</li>
            <li> => responseType (Optional)</li>
            <li> => tokenID (Mandatory)</li>
        </ul>
        <b>Response  Parameters</b>
        <ul>
            <li> => status ( value : success/error )</li>
            <li> => message ( Description of message or error )</li>
            <li> => data ( NULL  )</li>
        </ul>
        <b>Sample Request:</b>

        {<br/>
         method : hide_all,<br/>
         responseType : json <br/>
         tokenID : token <br/>
        }
        
        <br/>
        <b>Sample Response:</b> 

        {<br/>
          "status": "success",<br/>
          "message": "All products visiblity is set to hidden.",<br/>
          "data": null<br/>
        }<br/>
        
        <hr/>

              <!---------MTR Update api method------------>
        
        <u>MTR Update API Method</u>
        <br/>
        <b>Request Parameters(Method: mtr_update)</b>
        <ul>
            <li> => method (Mandatory)</li>
            <li> => responseType (Optional)</li>
            <li> => tokenID (Mandatory)</li>
            <li> => item_ser_no (Mandatory)</li>
            <li> => mtr (Mandatory)</li>
            <!-- <li> => mtr_tag_sql_recid (Mandatory)</li>
            <li> => notes (Optional)</li> -->
        </ul>
        <b>Response  Parameters</b>
        <ul>
            <li> => status ( value : success/error )</li>
            <li> => message ( Description of message or error )</li>
            <li> => data ( NULL/ On success: Url of the uploaded MTR  )</li>
        </ul>
        <b>Sample Request:</b>

        {<br/>
         method : mtr_update,<br/>
         responseType : json, <br/>
         tokenID : token, <br/>
         item_ser_no : 1234, <br/>
         mtr : image with base64 script. <b>(Refer Url:</b> <a target="_blank" href="https://www.browserling.com/tools/image-to-base64"><u>Click here to convert image to base64 script</u></a><b>)</b> <br/>
         <!-- mtr_tag_sql_recid : 7890, <br/>
         notes : This is testing notes, <br/> -->
        }
        
        <br/>
        <b>Sample Response:</b> 

        {<br/>
          "status": "success",<br/>
          "message": "MTR updated successfully.",<br/>
          "data": {<br/>
            <font style="padding-left:50px;float:left;">"mtr": "http://securedstaging.com/nssco/uploads/mtr/abc.jpg"</font> <br/>
            <font style="padding-left:30px;float:left;">}</font><br/>
        
        }<br/>

        <hr/>

         <!---------MTR delete api method------------>
        
        <u>MTR delete API Method</u>
        <br/>
        <b>Request Parameters(Method: mtr_delete)</b>
        <ul>
            <li> => method (Mandatory)</li>
            <li> => responseType (Optional)</li>
            <li> => tokenID (Mandatory)</li>
            <li> => item_ser_no (Mandatory)</li>           
            <!-- <li> => mtr_tag_sql_recid (Mandatory)</li> -->
        </ul>
        <b>Response  Parameters</b>
        <ul>
            <li> => status ( value : success/error )</li>
            <li> => message ( Description of message or error )</li>
            <li> => data ( NULL/ On success: Url of the uploaded MTR  )</li>
        </ul>
        <b>Sample Request:</b>

        {<br/>
         method : mtr_delete,<br/>
         responseType : json, <br/>
         tokenID : token, <br/>
         item_ser_no : 1234, <br/>         
         <!-- mtr_tag_sql_recid : 7890, <br/> -->
        }
        
        <br/>
        <b>Sample Response:</b> 

        {<br/>
          "status": "success",<br/>
          "message": "MTR deleted successfully.",<br/>
          "data": null<br/>
      }<br/>

        <hr/>

        <b>Note:</b> Method, Response type and Token can also be send from header using variables method, responseType and tokenID respectively. Allowed request types: POST,GET,PUT,PATCH
    </div>
</div>

<br />