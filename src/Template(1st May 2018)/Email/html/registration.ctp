<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>NSSCO: New order</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet"> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	
</head>


<body style="margin: 0; padding: 0;">

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#fff" >
	<tbody>
	<tr>
		<td width="100%" valign="top" align="center">
		
		
<table width="680" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#f2f2f2" style="">
							<tbody>
							
							<tr>
							
							<td>							
						<!-- Wrapper -->
							<table width="620" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border-radius:8px; margin-top: 30px;">
							<tbody>
							
							<tr>
								<td height="" width="100%" style="padding:20px;"> <a href="<?php echo $url;?>"><img width="87" border="0" alt="" src="<?php echo $url;?>img/mail_logo.png"></a> </td>
							</tr>
							
							<tr>						

											<td valign="middle" width="100%" style="text-align: left; font-family: 'Open Sans', sans-serif; font-size:17px; color: #60605e; line-height:28px; font-weight:normal;  padding:0 20px;">
											<h1 width="100%" style="text-align: left; font-family: 'Open Sans', sans-serif; font-size:30px; color: #60605e; line-height:38px; font-weight: 600; margin: 0 0 20px; padding: 0;">
															<?php echo $heading;?>
														</h1>
							<p><p>Hi <?php echo $name;?>,</P>
                        
                <p>Thank you for your registering at NSSCO</P>
                <p>These are the login credentials.</p>

                <p>Username: <b><?php echo $email;?></b></p>
                <p>Password: <b><?php echo $password;?></b></p>

                  <p>If you have any questions, please email us <?php echo $to_email;?>.</p> 
               <p>Many thanks, <br/> NSSCO Team</p>           
                        
                      </td>                  
              </tr>
              
                <tr>
                
                
                  <td align="center">   <a href='<?php echo $url;?>' style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Visit NSSCO </a> </td>
                  
                
                  </tr>
							
			<tr>
		
									</tr>

						<tr>							
							<td align="center" height="30">	 </td>
								
									</tr>							
							
						</tbody></table><!-- End Wrapper 2 -->
						
						
						
						
								<table width="620" border="0" cellpadding="0" cellspacing="0" align="center" style="border-radius:8px; margin-top: 30px;">
							<tbody>
		
		                     <tr>
							
							
							</tr>
		
		</tbody></table>
		
		</td>
		
						 </td>
							</tr>
		
		</tbody></table>	
	
	</tr>
	
</tbody></table>


</body>

</html>






