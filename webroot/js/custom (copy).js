    $(document).ready(function(){
        // add product to cart
        $(document).on("click", ".add_to_cart", function(){
            var id = $(this).attr("id");
            var curr = $(this);
            var group_name = $(".group_name").text();
            // alert(id)
            $.ajax({
                type: "POST",
                url: site_url+"pages/add_cart",
                data: {id: id,group_name:group_name},
                dataType: 'json',
                success: function (data) {
                    if (data.status != "" && data.status == "success") {
                        // $(".remove_cart_item_td,.remove_cart_item").show();
                        // var in_cart_html = '<div class="bluee-btn">';
                        // in_cart_html += '<span class="glyphicon glyphicon-shopping-cart green-icon blk-bg" aria-hidden="true"></span>Item in Cart';
                        // in_cart_html += '</div>';
                        
                        // var delete_from_cart = '<center>';
                        //  delete_from_cart += '<a href="javascript:void(0)" class="delete_from_cart" id="'+id+'" title="Delete">';
                        //  delete_from_cart += '<div class="bluee-btn">';
                        //  delete_from_cart += '<span class="glyphicon glyphicon-trash blk-bg" aria-hidden="true"></span> Delete';
                        //  delete_from_cart += '</div></a></center>';      

                        var delete_from_cart = '<center>';
                         delete_from_cart += '<a href="javascript:void(0)" class="delete_from_cart" id="'+id+'" title="Delete">';
                         delete_from_cart += '<div class="bluee-btn green-icon">';
                         // delete_from_cart += '<span class="glyphicon glyphicon-shopping-cart green-icon blk-bg" aria-hidden="true"></span> Delete';
                         delete_from_cart += '<span class="glyphicon glyphicon-shopping-cart blk-bg" aria-hidden="true"></span> Delete';
                         delete_from_cart += '</div></a></center>';      
                         $(".item_in_cart"+id+"").html(delete_from_cart); 
                        
                        // curr.html(in_cart_html); 
                        // curr.removeClass("add_to_cart");
                        // curr.addClass("item_in_cart");                                              
                        // $(".remove_cart_item"+id+"").html(delete_from_cart);                        
                        $(".cart_total").html(data.item_count);
                    } else {
                        // swal("", data.msg, "error");
                    }
                    
                }
            });
        });


         $(document).on("click", ".delete_from_cart", function(){
           
            var id = $(this).attr("id");
            var curr = $(this);
            var group_name = $(".group_name").text();
            // alert(id)
            $.ajax({
                type: "POST",
                url: site_url+"pages/delete_from_cart",
                data: {id: id,group_name:group_name},
                dataType: 'json',
                success: function (data) {                    
                    
                    $(".cart_total").html(data.item_count);
                    $(".remove_cart_item"+id+"").html('');
                     var add_cart = '<center>';
                         add_cart += '<a href="javascript:void(0)" class="add_to_cart" id="'+id+'" title="Add to Cart">';
                         add_cart += '<div class="bluee-btn">';
                         add_cart += '<span class="glyphicon glyphicon-shopping-cart blk-bg" aria-hidden="true"></span> Add to Cart';
                         add_cart += '</div></a></center>';  

                         $(".item_in_cart"+id+"").html(add_cart); 
                         var count_delete_length = $(document).find(".delete_from_cart").length;
                        if(count_delete_length == 0 || data.item_count == 0){
                             $('.err').remove();
                           $(".remove_cart_item_td,.remove_cart_item").hide(); 
                        }
                    // if (data.status != "" && data.status == "success") {
                    //     $(".remove_cart_item_td,.remove_cart_item").show();
                    //     var in_cart_html = '<div class="bluee-btn">';
                    //     in_cart_html += '<span class="fa fa-cart-plus blk-bg" aria-hidden="true"></span>Item in Cart';
                    //     in_cart_html += '</div>';
                        
                    //     var delete_from_cart = '<center>';
                    //      delete_from_cart += '<a href="javascript:void(0)" class="delete_from_cart" id="'+id+'" title="Delete">';
                    //      delete_from_cart += '<div class="bluee-btn">';
                    //      delete_from_cart += '<span class="glyphicon glyphicon-trash blk-bg" aria-hidden="true"></span> Delete';
                    //      delete_from_cart += '</div></a></center>';        
                        
                        
                    //     curr.html(in_cart_html);
                    //     curr.css("cursor", 'default');
                    //     alert(".remove_cart_item"+id+"");
                    //     $(".remove_cart_item"+id+"").html(delete_from_cart);
                    //     swal("", data.msg, "success");
                    //     $(".cart_total").html(data.item_count);
                    // } else {
                    //     swal("", data.msg, "error");
                    // }
                    
                }
            });
        });

        // // list product by respectcive group
        // $(document).on("click", ".get_product_bygroup", function(){
        //     var id = $(this).attr("id");
        //     var group_name = $(this).text();
        //     $('.get_product_bygroup.active').removeClass('active');
        //     $(this).addClass('active');
        //     $(".group_name").show();
        //     // alert(text);
        //     // alert(id)
        //     $.ajax({
        //         type: "POST",
        //         url: site_url+"pages/get_product_bygroup",
        //         data: {id: id},                
        //         success: function (data) {
        //             $(".group_name").html(group_name);
        //             $(".product_table").html(data);
                    
        //         }
        //     });
        // });

        
    //     $(document).on("click", ".view_all_product", function(){
    //         var id = $(this).attr("id");
    //         var group_name = $(this).text();
    //         $('.get_product_bygroup.active').removeClass('active');
    //         $(this).addClass('active');
    //         $(".group_name").hide();
    //         // alert(text);
    //         // alert(id)
    //         $.ajax({
    //             type: "GET",
    //             url: site_url+"pages/index",
    //             data: {group_id: id},                               
    //             success: function (data) {
    //                 $("#divUpdate").html(data);
    //                 $('#ifta_table').dataTable({   
    //                     "searching": false,
    //                     "paging":   false,
    //                     "info":false,
    //                     aoColumnDefs: [
    //                     {
    //                        'bSortable': false,
    //                        'aTargets': ['tag_number',1,4,6,7,8,9,10,11 ]
    //                    }
    //                    ]

    //      // "ordering":false     
    //                   });
                    
    //             }
    //         });
    //     });
    });


    // Delete product from cart
    $(document).ready(function(){
        // alert(site_url);
        $(document).on("click", ".delete_cartitem", function(){
            var id = $(this).attr("id");
            var curr = $(this);
            
            // swal({
            //     title: "Are you sure?",
            //     text: "Item will be deleted from the cart!",
            //     type: "warning",
            //     showCancelButton: true,
            //     confirmButtonColor: "#DD6B55",
            //     confirmButtonText: "Yes, delete it!",
            //     cancelButtonText: "No, cancel plz!",
            //     closeOnConfirm: false,
            //     closeOnCancel: false,
            // },
            // function (isConfirm) {
                // if (isConfirm) {
                    $.ajax({
                        url: site_url+"pages/delete_cartitem", //this is the submit URL
                        type: 'POST', //or POST  
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {

                            if (data.status != "" && data.status == "success") {
                                curr.closest("tr").next('.err').remove();
                                curr.closest("tr").remove();
                                $(".cart_total").html(data.item_in_cart);
                                // swal("Deleted!", data.msg, "success");
                                $('td.td_order').text(function (i) {                                    
                                  return i + 1;
                              }); 
                                if(data.grand_total!=0){
                              $(".calculate_grand_total").html('$'+data.grand_total); 
                              }else{
                                $(".list_cart_product").remove();
                                $(".no_products").show();
                              }                                                           
                                //$('.err').remove(); 
                            } else {
                                swal("", data.msg, "error");
                            }
                        }
                    });
                // } else {
                //     swal("Cancelled", "Your data is safe :)", "error");
                // }
            // });
        });


        $(document).on("click",".refresh_captcha", function(){
            $.ajax({
                        url: site_url+"pages/refresh_captcha", //this is the submit URL
                        type: 'POST',//or POST                       
                        dataType: 'json',
                        success: function (data) {
                            $(".captcha").val(data.code);

                        }
                    });
        });
    });

    $(document).on("click",'.del',function()
    {
        var id = $(this).closest('td').children('center').children('a').attr("id");
        console.log($(this).closest('td').children('center').children('a'));
            var curr = $(this);
            
          
                    $.ajax({
                        url: site_url+"pages/delete_cartitem", //this is the submit URL
                        type: 'POST', //or POST  
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {

                            if (data.status != "" && data.status == "success") {
                                curr.closest("tr").next('.err').remove();
                                curr.closest("tr").remove();
                                $(".cart_total").html(data.item_in_cart);
                                // swal("Deleted!", data.msg, "success");
                                $('td.td_order').text(function (i) {                                    
                                  return i + 1;
                              }); 
                                if(data.grand_total!=0){
                              $(".calculate_grand_total").html('$'+data.grand_total); 
                              }else{
                                $(".list_cart_product").remove();
                                $(".no_products").show();
                              }                                                           
                                //$('.err').remove(); 
                            } else {
                                swal("", data.msg, "error");
                            }
                        }
                    });
       
    });
    // Submit user info and generate XML
    $(document).on("submit","#odrer_product", function (e) {
        e.preventDefault();
        if ($("#odrer_product").valid())
            {   $(".overlay").show();
        $.ajax({
            cache: false,  
        url: site_url + "pages/viewCart", //this is the submit URL
        type: 'POST', //or POST
        dataType: 'json',
        data: new FormData(this),
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.status != "" && data.status == "success") {                                
                swal("", data.msg, "success");
               // $('.rb').text('Submit Request For Order');
                $('.err').remove();
                $(".cart_total").html('0');
                $(".list_cart_product").remove();
                $(".no_products").show();
                $("#odrer_product").find(".common").val('');
            } else {
                if (data.msg.indexOf("Tag number") >= 0)
                   {
                    
                    
                    $.each(data.pid, function(n, elem) {
                    var txt=$('#'+elem+' td:nth-child(2)').text();
                    console.log($('#'+elem).next());
                    if($('#'+elem).next().hasClass('err'))
                    {
                        console.log($('#'+elem).next());
                        $('#'+elem).children('td.err').remove();
                    }
                    $('#'+elem).addClass('errmain');
                    $('#'+elem).append('<td colspan="11" style="color:red;" class="err">Tag Number '+txt+' is no longer available for sale.<span class="glyphicon glyphicon-trash blk-bg del" aria-hidden="true"></span></td>');
                     //$('.rb').text('Sumit Order Request Anyway.');
                      swal("", "Some item(s) no longer for sale in your cart, please remove highlighted item(s) to continue.", "error");
                    });
                   }
                   else
                   {
                        swal("", data.msg, "error");
                   }
                
            }
            $(".overlay").hide();
            
        }
    });
    }
    
});

    // Submit user info and generate XML
    $(document).on("submit","#guest_login", function (e) {
        e.preventDefault();
        if ($("#guest_login").valid())
            {   $(".overlay").show();
        $.ajax({
            cache: false,  
        url: site_url + "users/guest_login", //this is the submit URL
        type: 'POST', //or POST
        dataType: 'json',
        data: new FormData(this),
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.status != "" && data.status == "success") {                                
                window.location.href = site_url+"pages/view_cart";
            } else {
                swal("Oops!", data.msg, "error");
            }
            $(".overlay").hide();
            
        }
    });
    }
    
});

// Submit user info and generate XML
    $(document).on("submit","#register", function (e) {
        e.preventDefault();
        if ($("#register").valid())
            {   $(".overlay").show();
        $.ajax({
            cache: false,  
        url: site_url + "users/register", //this is the submit URL
        type: 'POST', //or POST
        dataType: 'json',
        data: new FormData(this),
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.status != "" && data.status == "success") { 

                setTimeout(function(){ window.location.href = site_url+"pages/view_cart"; }, 2000);                               
                
            } else {
                $(".overlay").hide();
                swal("Oops!", data.msg, "error");
            }
            
            
        }
    });
    }
    
});

    $(document).on("submit","#forgot_password", function (e) {
        // alert(1);
    // alert(path); return false;
    e.preventDefault();

    if ($("#forgot_password").valid())
    {
       $(".overlay").show();

       $.ajax({
        cache: false,
            url: site_url + "users/forgot_password", //this is the submit URL
            type: 'POST', //or POST
            data: new FormData(this),
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (data) {


                if (data != "" && data.status == "success") {

                    $("#forgot_password").find("input[type=email]").val('');
                    swal("", data.response, "success");                 

                } else if (data.status == "error") {
                    swal("", data.response, "error");
                }

                $(".overlay").hide();

            }
        });
   }
});
