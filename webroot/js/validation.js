jQuery(document).ready(function () {
    $('.phone_masking').mask('000-000-0000');
     $.validator.addMethod(
            "lettersOnly",
            function (value, element) {
                return value.match(/^[A-Za-z ]+$/);
            },
            "Enter only letters"
            );

    $.validator.addMethod(
            "validateEmail",
            function (value, element) {
                return value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
            },
            "Invalid Email"
            );

    
     $.validator.addMethod(
            "validatePhone",
            function (value, element) {
                // return value.match(/^[+-]?\d+$/);
                return value.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/);
            },
            "Invalid Phone Number"
            );
    
    /******Add User form validation**********/
    jQuery("#guest_login").validate({
        // Specify the validation rules
        rules: {
	    "name": {
                required: true,
                lettersOnly: true,
     
            },
            "company": {
                required: true,
                // lettersOnly: true,
           
            },
            "username": {
                required: true,
                validateEmail: true,
                //remote: site_url+"Users/checkemailuniqueness"

            },  
             "phone": {
                required: true,
                validatePhone: true
            
            },            
            "comment": {
                required: true,
            
            },  
            "captcha_text": {
                required: true,
            
            },        
        },
        // Specify the validation error messages
        messages: {
            "name": {
                required: "Please enter name"
            },
            "company": {
                required: "Please enter company name"

            },
            "username": {
                required: "Please enter email id",
              // remote: "Email already exists"
               
            },  
             "phone": {
                required: "Please enter phone number",
            
            },  
            "comment": {
                required: "Please enter comment",
            
            },  
            "captcha_text": {
                required: "Please enter security code shown below",
            
            },         
        }
    });

    jQuery("#login_form").validate({
        // Specify the validation rules
        rules: {
        
            "username": {
                required: true,
                validateEmail: true,              

            },  
             "password": {
                required: true,
            
            },        
        },
        // Specify the validation error messages
        messages: {
           
            "username": {
                required: "Please enter username",           
               
            },  
             "password": {
                required: "Please enter password",
            
            },         
        }
    });
    
    /******Add User form validation**********/
    jQuery("#register").validate({
        // Specify the validation rules
        rules: {
        "name": {
                required: true,
                lettersOnly: true,
     
            },
            "company": {
                required: true,
                // lettersOnly: true,
           
            },
             "email": {
                required: true,
                validateEmail: true,
                remote: site_url+"Users/checkeguestemail"

            }, 
            "phone": {
                required: true,
                validatePhone: true
            
            },


            "username": {
                required: true,
                validateEmail: true,
                remote: site_url+"Users/checkemailuniqueness"

            },
            "password": {
                required: true,
            
            },  
            "cpassword": {
                required: true,
                equalTo: "#password"
            }         
        },
        // Specify the validation error messages
        messages: {
            "name": {
                required: "Please enter name"
            },
            "company": {
                required: "Please enter company name"

            },
            "email": {
                required: "Please enter email",
                remote: "Email already exists"
            },
            
             "phone": {
                required: "Please enter phone number",
            
            },  
            "username": {
                required: "Please enter email",
                remote: "Email already exists"
            },
            "password": {
                required: "Please enter password"

            },
            "cpassword": {
                required: "Please re-enter your password",
                equalTo: "Passwords do not match"
            
            }      
        }
    });



    /******Add User form validation**********/
    jQuery("#profile").validate({
        // Specify the validation rules
        rules: {
        "name": {
                required: true,
                lettersOnly: true,
     
            },
            "company": {
                required: true,
                // lettersOnly: true,
           
            },
             "email": {
                required: true,
                validateEmail: true,
                remote: site_url+"Users/checkemailuniqueness_onedit"

            }, 
            "phone": {
                required: true,
                validatePhone: true
            
            },


            "username": {
                required: true,
                validateEmail: true,
                remote: site_url+"Users/checkemailuniqueness_onedit"

            },
              
        },
        // Specify the validation error messages
        messages: {
            "name": {
                required: "Please enter name"
            },
            "company": {
                required: "Please enter company name"

            },
            "email": {
                required: "Please enter email",
                remote: "Email already exists"
            },
            
             "phone": {
                required: "Please enter phone number",
            
            },  
            "username": {
                required: "Please enter email",
                remote: "Email already exists"
            },
             
        }
    });
    
     
    jQuery("#forgot_password").validate({
        // Specify the validation rules
        rules: {
        
            "username": {
                required: true,
                validateEmail: true,              

            },  
                  
        },
        // Specify the validation error messages
        messages: {
           
            "username": {
                required: "Please enter email id",           
               
            },  
                  
        }
    });


   jQuery("#reset_password").validate({
        // Specify the validation rules
        rules: {
       
            "password": {
                required: true,
            
            },  
            "password_confirm": {
                required: true,
                equalTo: "#password"
            }   
        },
        // Specify the validation error messages
        messages: {
           
            "password": {
                required: "Please enter password"

            },
            "password_confirm": {
                required: "Please re-enter your password",
                equalTo: "Passwords do not match"
            
            }  
        }
    });


   jQuery("#change_password").validate({
        // Specify the validation rules
        rules: {

            "old_password": {
                required: true,            
            },       
            "password": {
                required: true,            
            },  
            "confirm_password": {
                required: true,
                equalTo: "#password"
            }   
        },
        // Specify the validation error messages
        messages: {

            "old_password": {
                required: "Please enter old password"
            },           
            "password": {
                required: "Please enter password"
            },
            "confirm_password": {
                required: "Please re-enter your password",
                equalTo: "Passwords do not match"
            
            }  
        }
    });
    

  
});


