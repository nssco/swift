<?php
    define('METHOD_INVALID', 'Invalid {var} Supplied.');
    define('SUCCESS', '{entity} {method} successfully.');
    define('ERROR', 'Unable to {method} {entity}.');
?>